package core;

import com.google.gson.Gson;

public class CommandRequest {
	
    public String Command;
    public String Channel;
    public String Option;
    public String Setting;
    public String BaseID;
    public String callid;
    public int pbxID;
    
    public CommandRequest() {    	
    }
    
    public CommandRequest(String json) {
    	try {
    		Gson gson = new Gson();
    		CommandRequest obj = gson.fromJson(json, CommandRequest.class);
    		
        	this.Command = obj.Command;
        	this.Channel = obj.Channel;
        	this.Option = obj.Option;
        	this.Setting = obj.Setting;
        	this.BaseID = obj.BaseID;
        	this.pbxID = obj.pbxID;
        	this.callid = obj.callid;
    	} catch(Exception e) {
    		LogWriter.instance.appendException(e);
    	}
    }
}
