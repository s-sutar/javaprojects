package core;

import java.util.LinkedList;
import java.util.Queue;

import commands.Command;
import pbx.Pbx;

public class QueueManagerForPred {
	private static QueueManagerForPred _instance = new QueueManagerForPred();
	private Queue<Command> commandQueue = new LinkedList<Command>();
	
	private QueueManagerForPred() {
	}
	
	public static QueueManagerForPred GetInstance() {
		if(_instance == null) {
			synchronized(QueueManager.class) {
				if(_instance == null) {
					_instance = new QueueManagerForPred();
				}
			}//end synchronized
		}
		
		return _instance;
	}
	
	public Boolean RemoveCommand(String targetID, Pbx pbx) {
		Boolean bFound = false;

		synchronized(this.commandQueue) {
			Queue<Command> commandQueueTemp = new LinkedList<Command>();
			
			for(Command cmd: this.commandQueue) {
				String interID = cmd.getInternalCallID();
				if(cmd.pbx != pbx || !interID.equals(targetID)) 
					commandQueueTemp.add(cmd);
				else
					bFound = true;
			}
			this.commandQueue = commandQueueTemp;
		}
		
		return bFound;
	}

	public void AddCommand(Command command) {
		if(command == null) {
			return;
		}
		
		synchronized(this.commandQueue) {
			this.commandQueue.add(command);
		}
	}
	
	public Command GetCommand() {
		synchronized(this.commandQueue) {
			return this.commandQueue.poll();
		}
	}
	
	public Command PeekCommand() {
		synchronized(this.commandQueue) {
			return this.commandQueue.peek();
		}
	}
	
	public int size() {
		return this.commandQueue.size();
	}

}
