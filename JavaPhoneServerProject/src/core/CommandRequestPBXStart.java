package core;

import com.google.gson.Gson;

public class CommandRequestPBXStart {
    public String Command;
    public String pbx_address;
    public String pbx_port;
    public String pbx_username;
    public String pbx_password;
    public String stasis_app;
    public String trunk_name;
    public String aesservicename;
    public String pbxType;
    public String pbx_id;
    
    public CommandRequestPBXStart() {    	
    }
    
    public CommandRequestPBXStart(String json) {
    	try {
    		Gson gson = new Gson();
    		CommandRequestPBXStart obj = gson.fromJson(json, CommandRequestPBXStart.class);
    		
        	this.Command = obj.Command;
        	this.pbx_address = obj.pbx_address;
        	this.pbx_port = obj.pbx_port;
        	this.pbx_username = obj.pbx_username;
        	this.pbx_password = obj.pbx_password;
        	this.stasis_app = obj.stasis_app;
        	this.trunk_name = obj.trunk_name;
        	this.aesservicename = obj.aesservicename;
        	this.pbxType = obj.pbxType;
        	this.pbx_id = obj.pbx_id;
    	} catch(Exception e) {
    		LogWriter.instance.appendException(e);
    	}
    }}
