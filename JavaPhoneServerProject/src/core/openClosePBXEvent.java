package core;

import com.google.gson.Gson;

public class openClosePBXEvent {
	public String EventName = null;
	public String pbxType = null;
	public String pbxid = null;
	public String result = null;
	public String errorCode = null;
	public String errorDescription = null;

	// to json
	public String toJson() {
		Gson gson = new Gson();
		return(gson.toJson(this));
	}

    public openClosePBXEvent() {    	
    }
    
    public openClosePBXEvent(String EventName, String pbxType, String pbxid, String result, String errorCode, String errorDescription) {
    	this.EventName = EventName;
    	this.pbxType = pbxType;
    	this.pbxid = pbxid;
    	this.result = result;
    	this.errorCode = errorCode;
    	this.errorDescription = errorDescription;
    }
    
    public openClosePBXEvent(String json) {
	try {
		Gson gson = new Gson();
		openClosePBXEvent obj = gson.fromJson(json, openClosePBXEvent.class);
		
    	this.EventName = obj.EventName;
    	this.pbxType = obj.pbxType;
    	this.pbxid = obj.pbxid;
    	this.result = obj.result;
    	this.errorCode = obj.errorCode;
    	this.errorDescription = obj.errorDescription;
	} catch(Exception e) {
		LogWriter.instance.appendException(e);
	}
}
}
