package core;


import com.google.gson.Gson;

public class RecordInfo {

	public int id = 0;
	public int type = 0;
	public int length = 0;
	public int LogCallID = 0;
	public int parentid = 0;
	public int pbxID = 0;
	public String EventName = null;
	public String callid = null;
	public String CustomerID = null;
	public String fromphoneNumber = null;
	public String toPhoneNumber = null;
	public String fileextension = null;
	public String filename = null;
	public String starttime = null;
	public String endtime = null;
	public String toAgentID = null;
	public String starttalkingtime = null;

    public String ToJson() {
    	try {
    		Gson gson = new Gson();
    		String json = gson.toJson(this);
    		
    		return json;
    	} catch(Exception e) {
    		LogWriter.instance.appendException(e);
    	}
    	
    	return null;
    }
    
    public RecordInfo() {
    }
}
