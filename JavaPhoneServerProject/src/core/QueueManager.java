package core;

import java.util.LinkedList;
import java.util.Queue;

import commands.Command;
import pbx.Pbx;

public class QueueManager {
	private static QueueManager _instance = new QueueManager();
	private Queue<Command> commandQueue = new LinkedList<Command>();
	private boolean IsRunning = false;
	
	private QueueManager() {
	}
	
	public static QueueManager GetInstance() {
		if(_instance == null) {
			synchronized(QueueManager.class) {
				if(_instance == null) {
					_instance = new QueueManager();
				}
			}//end synchronized
		}
		
		return _instance;
	}
	
	public Boolean RemoveCommand(String targetID, Pbx pbx) {
		Boolean bFound = false;

		synchronized(this.commandQueue) {
			Queue<Command> commandQueueTemp = new LinkedList<Command>();
			
			for(Command cmd: this.commandQueue) {
				String interID = cmd.getInternalCallID();
				if(cmd.pbx != pbx || !interID.equals(targetID)) 
					commandQueueTemp.add(cmd);
				else
					bFound = true;
			}
			this.commandQueue = commandQueueTemp;
		}
		
		return bFound;
	}

	public void AddCommand(Command command) {
		if(command == null) {
			return;
		}
		
		synchronized(this.commandQueue) {
			this.commandQueue.add(command);
		}
		
		Process();
	}
	
	public Command GetCommand() {
		synchronized(this.commandQueue) {
			return this.commandQueue.poll();
		}
	}
	
	public Command PeekCommand() {
		synchronized(this.commandQueue) {
			return this.commandQueue.peek();
		}
	}
	
	public void Process() {
		if(this.IsRunning) {
			return;
		}
		
		synchronized(QueueManager.class) {
			if(this.IsRunning) {
				return;
			}
			
			this.IsRunning = true;
		}

		while(this.PeekCommand() != null && PhoneManager.isRunning) {
			Command command = this.GetCommand();
			
			try {
				command.Execute();
			} catch(Exception e) {
				LogWriter.instance.appendError("Failed to execute: " + command.toString());
				LogWriter.instance.appendException(e);
			}		
		}
		
		LogWriter.instance.appendInfo("Terminated - QueueManager loop");
		
		this.IsRunning = false;
	}

}

