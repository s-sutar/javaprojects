package core;

import com.google.gson.Gson;

public class AsteriskChannelPeer {
	public String id;
	public String name;
	public String state;
	public AsteriskDialPlan dialplan;

    public AsteriskChannelPeer(String json) {
    	try {
    		Gson gson = new Gson();
    		AsteriskChannelPeer obj = gson.fromJson(json, AsteriskChannelPeer.class);
    		
        	this.id = obj.id;
        	this.name = obj.name;
        	this.state = obj.state;
        	this.dialplan = obj.dialplan;

    	} catch(Exception e) {
    		LogWriter.instance.appendException(e);
    	}
    }
    
    public String ToString() {
    	return "id=" + this.id + " name=" + this.name + " state=" + this.state;
    }
}
