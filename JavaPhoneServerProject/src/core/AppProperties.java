package core;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.Properties;


public class AppProperties {

	public String pbxtype = "";
	public String serviceName = "";
	public String login = "";
	public String password = "";
	public String serverIP = "";
	public String portNumber = "";
	public String webserver = "";
	public String hostname = "";
	public int mpbx_reconnect_waittime = 3;
	public int avaya_reconnect_waittime = 3;
	public String db_name = "";
	public String db_address = "";
	public String db_username = "";
	public String db_password = "";
	public boolean isSimulation = false;
	public boolean isOutgoingSimulation = false;
	public boolean isNewUUI = false;
	public boolean bPredictiveCall = false;
	
	public int asteriskReconAttemptCount = 3;

	public String pbxtemp_address = "";

	public boolean pbxEnableSSL = false;
	
	public boolean recording_enabled = false;
	public String record_db_name = "";
	public String record_db_address = "";
	public String record_db_username = "";
	public String record_db_password = "";
	public String record_table = "";
	
	public int listenPort;
	public int listenPortSSL;
	public int listenPortWS;
	public int listenPortWSS;
	
	public int pingInterval;
	
	public int maxthread_avaya_dialing = 20;
	
	public String stasisApp = "";
	
	public static AppProperties instance = new AppProperties();
	Properties prop = null;

	public void load() {
		this.prop = new Properties();
		InputStream input = null;
	 
		try {	 
			input = new FileInputStream("config.properties");
	 
			// load a properties file
			prop.load(input);

			pbxtype = GetConfigString("pbx_type", pbxtype);			

			if(pbxtype != null && pbxtype.equals("mpbx")) {

				login = GetConfigString("pbx_username", login);			
				password = GetConfigString("pbx_password", password);			
				serverIP = GetConfigString("pbx_address", serverIP);			
				portNumber = GetConfigString("pbx_port", portNumber);			
				
				pbxtemp_address = GetConfigString("pbxtemp_address", pbxtemp_address);			
			}
			else {
				// get the property value and print it out
				serviceName = GetConfigString("aes_serviceName", serviceName);			
				login = GetConfigString("aes_login", login);			
				password = GetConfigString("aes_password", password);			
				serverIP = GetConfigString("aes_serverIP", serverIP);			
				portNumber = GetConfigString("aes_portNumber", portNumber);			
			}
			
			bPredictiveCall = GetConfigBoolean("predictive_call", bPredictiveCall);			
			pbxEnableSSL = GetConfigBoolean("pbx_enableSSL", pbxEnableSSL);			
			
			mpbx_reconnect_waittime = GetConfigInt("mPBX_reconnect_waittime", mpbx_reconnect_waittime);			
			
			avaya_reconnect_waittime = GetConfigInt("avaya_reconnect_waittime", avaya_reconnect_waittime);			

			asteriskReconAttemptCount = GetConfigInt("reconnect_maxretries", asteriskReconAttemptCount);			

			listenPort = GetConfigInt("listenPort", listenPort);			
			
			listenPortSSL = GetConfigInt("listenPortSSL", listenPortSSL);			
			
			listenPortWS = GetConfigInt("listenPortWS", listenPortWS);			
			
			listenPortWSS = GetConfigInt("listenPortWSS", listenPortWSS);			

			pingInterval = 60;	//default
			pingInterval = GetConfigInt("pingInterval", pingInterval);			
			
			if (pingInterval < 5) {
				pingInterval = 5;
			}
			else if (pingInterval > 300) {
				pingInterval = 300;
			}
			pingInterval *= 1000;
			
		    InetAddress addr;
		    addr = InetAddress.getLocalHost();
		    hostname = addr.getHostName();
		    db_name = GetConfigString("db_name", db_name);			
		    db_address = GetConfigString("db_address", db_address);			
		    db_username = GetConfigString("db_username", db_username);			
		    db_password = GetConfigString("db_password", db_password);			
		    
		    stasisApp = GetConfigString("stasis_app", stasisApp);			
		    
		    maxthread_avaya_dialing = GetConfigInt("maxthread_avaya_dialing", maxthread_avaya_dialing);			
			
		    //simulation
			isSimulation = GetConfigBoolean("simulation", isSimulation);			

		    isOutgoingSimulation = GetConfigBoolean("outgoing_simulation", isOutgoingSimulation);			
		    
		    isNewUUI = GetConfigBoolean("new_uui", isNewUUI);			

		    record_db_name = GetConfigString("record_db_name", record_db_name);			
			record_db_address = GetConfigString("record_db_address", record_db_address);			
			record_db_username = GetConfigString("record_db_username", record_db_username);			
			record_db_password = GetConfigString("record_db_password", record_db_password);			
			record_table = GetConfigString("record_table", record_table);			
			
			if(record_db_name != null && record_db_name.isEmpty() == false
					&& record_db_address != null && record_db_address.isEmpty() == false
					&& record_db_username != null && record_db_username.isEmpty() == false
					&& record_db_password != null && record_db_password.isEmpty() == false
					&& record_table != null && record_table.isEmpty() == false) {
				recording_enabled = true;
			}
		} catch (Exception ex) {
			LogWriter.instance.appendException(ex);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					LogWriter.instance.appendException(e);
				}
			}
		}//end try catch		
	}
	
	private String GetConfigString(String key, String defaultValue) {
		if (this.prop == null || key == null || key.isEmpty()) {
			return defaultValue;
		}

		String value = defaultValue;

		try {
			value = this.prop.getProperty(key);
			if(value == null) {
				throw new Exception("failed to get string");
			}
			value = value.replace(" ", "");

			LogWriter.instance.appendInfo(String.format("---%s=%s", key, value));
		} catch (Exception ex) {
			value = defaultValue;
			
			//LogWriter.instance.appendInfo(String.format("---failed to get %s, defaultValue=%s, error=%s", key, defaultValue, ex.getMessage()));
			LogWriter.instance.appendInfo(String.format("---failed to get %s, defaultValue=%s", key, defaultValue));
		}

		return value;
	}

	private int GetConfigInt(String key, int defaultValue) {
		if (this.prop == null || key == null || key.isEmpty()) {
			return defaultValue;
		}
		
		int value = defaultValue;

		try {
			value = Integer.parseInt(this.prop.getProperty(key));
			LogWriter.instance.appendInfo(String.format("---%s=%d", key, value));
		} catch (Exception ex) {
			value = defaultValue;
			
			LogWriter.instance.appendInfo(String.format("---failed to get %s, defaultValue=%d", key, defaultValue));
		}

		return value;
	}	

	private boolean GetConfigBoolean(String key, boolean defaultValue) {
		if (this.prop == null || key == null || key.isEmpty()) {
			return defaultValue;
		}
		
		boolean value = defaultValue;

		try {
			value = Boolean.parseBoolean(this.prop.getProperty(key));
			LogWriter.instance.appendInfo(String.format("---%s=%d", key, value));
		} catch (Exception ex) {
			value = defaultValue;
			
			LogWriter.instance.appendInfo(String.format("---failed to get %s, defaultValue=%b", key, defaultValue));
		}

		return value;
	}	
}
