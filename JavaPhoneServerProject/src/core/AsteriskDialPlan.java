package core;

import com.google.gson.Gson;

public class AsteriskDialPlan {
	public String context;
	public String exten;
	public String priority;
	
    public AsteriskDialPlan(String json) {
    	try {
    		Gson gson = new Gson();
    		AsteriskDialPlan obj = gson.fromJson(json, AsteriskDialPlan.class);
    		
        	this.context = obj.context;
        	this.exten = obj.exten;
        	this.priority = obj.priority;
    	} catch(Exception e) {
    		LogWriter.instance.appendException(e);
    	}
    }
    
}
