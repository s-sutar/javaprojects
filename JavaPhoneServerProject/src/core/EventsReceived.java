package core;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;

public class EventsReceived {
	
	private static DateFormat df = new SimpleDateFormat("HH:mm:ss.SSS");
	
	public String EventName;
	public String Channel;
	public String Message;
	public String RequestID;
	public String Response;
	public String Timestamp;
	public String SessionID;
	public String Exten;
	public String ChannelState;
	public String Context;
	public String Reason;
	public String CustomerID;
	public String CustomerName;

	public int CallIndex;
	public String cause;
	public String SystemCallID;
	public String BaseID;
	public int pbxid;
	
    public EventsReceived() {    	
    }
    
    public EventsReceived(String eventName, String channel) {
    	this.EventName = eventName;
    	this.Channel = channel;
    	this.Timestamp = df.format(new Date());
    }
    
    public EventsReceived(String json) {
    	try {
    		Gson gson = new Gson();
    		EventsReceived obj = gson.fromJson(json, EventsReceived.class);
    		
        	this.EventName = obj.EventName;
    	} catch(Exception e) {
    		LogWriter.instance.appendException(e);
    	}
    }
    
    public String ToJson() {
    	try {
    		Gson gson = new Gson();
    		String json = gson.toJson(this);
    		
    		return json;
    	} catch(Exception e) {
    		LogWriter.instance.appendException(e);
    	}
    	
    	return null;
    }

}
