package core;

import java.text.NumberFormat;

public class ResourceMonitor {
	private static Runtime runtime = Runtime.getRuntime();
	private static NumberFormat format = NumberFormat.getInstance();
	
	public static void run() {
		StringBuilder sb = new StringBuilder();
		long maxMemory = runtime.maxMemory();
		long allocatedMemory = runtime.totalMemory();
		long freeMemory = runtime.freeMemory();
		int availProcessors = runtime.availableProcessors();

		sb.append("avail processors: " + availProcessors + "\t");
		sb.append("free memory: " + format.format(freeMemory / 1024) + "\t");
		sb.append("allocated memory: " + format.format(allocatedMemory / 1024) + "\t");
		sb.append("max memory: " + format.format(maxMemory / 1024) + "\t");
		sb.append("total free memory: " + format.format((freeMemory + (maxMemory - allocatedMemory)) / 1024));
		
		LogWriter.instance.appendInfo(sb.toString());
	}

}
