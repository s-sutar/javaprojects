package core;

import com.google.gson.Gson;

public class AsteriskEvent {
	public String type;
	public String cause;
	public String cause_txt;
	public String dialstatus;
	public String variable;
	public String value;
	public AsteriskChannelPeer channel;
	
    public AsteriskEvent(String json) {
    	try {
    		Gson gson = new Gson();
    		AsteriskEvent obj = gson.fromJson(json, this.getClass());
    		
        	this.cause = obj.cause;
        	this.type = obj.type;
        	this.channel = obj.channel;
        	this.dialstatus = obj.dialstatus;
        	this.cause_txt = obj.cause_txt;
        	this.value = obj.value;
        	this.variable  = obj.variable;
        	
    	} catch(Exception e) {
    		LogWriter.instance.appendException(e);
    	}
    }
    
    public String ToString() {
    	String ret = "type=" + this.type;
    	if(channel != null)
    		ret += " channel=" + channel.ToString();
    	
    	return ret;
    }
}
