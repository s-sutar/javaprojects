package core;

import network.WebClient;
import commands.CommandFactory;
import commands.Command;
import pbx.CallOriginator;
import pbx.CallReceiver;
import pbx.Pbx;
import pbx.PbxPool;
import pbx.astPbx;
import pbx.Pbx.PbxType;

public class PhoneManager {
	
	public static boolean isRunning = true;
	private static PhoneManager _instance = new PhoneManager();
	
	public static PhoneManager GetInstance() {
		if(_instance == null) {
			synchronized(PhoneManager.class) {
				if(_instance == null) {
					_instance = new PhoneManager();
				}
			}//end synchronized
		}
		
		return _instance;
	}
	
	public boolean Process(PbxType pbxType, String commandName,	String fromNumber, String option, String setting, String BaseID, int pbxID) {
		if(commandName == null || commandName.isEmpty()
				|| fromNumber == null || fromNumber.isEmpty()) {
			
			String failedReason = "";
			
			if(commandName == null || commandName.isEmpty()) {
				failedReason = "invalid commandName";
			} else if(fromNumber == null || fromNumber.isEmpty()) {
				failedReason = "invalid fromNumber";
			}
			
			LogWriter.instance.appendError("Failed to process a request: reason=" + failedReason);
			
			return false;
		}
		
		Pbx pbx = PbxPool.GetInstance().GetPbx(pbxType, pbxID);
		CallOriginator originator = null;
		CallReceiver receiver = null;
		
		if(pbx == null) {
			LogWriter.instance.appendError("Failed to process a request: no pbx Command: " + commandName);
			
			EventsReceived evt = new EventsReceived(commandName, fromNumber);
			evt.Message = "failed";
			String msg = evt.ToJson();
			WebClient.GetInstance().SendMessage(msg);
			network.SocketHandler.notifyAllDailers(msg);

			return false;
		}
		
		if(pbxType == PbxType.MPBX) {
			astPbx aPBX = (astPbx) pbx;

			if(commandName.equals("conferenceaddchannel"))
			{
				String[] split = option.split(";");
				if(split != null && split.length > 1) {					
					aPBX.ConferenceAddChannel(split[0], split[1], false);
				}
				
				return true;
			}
			else if(commandName.equals("conferenceremovechannel"))
			{
				String[] split = option.split(";");
				if(split != null && split.length > 1) {					
					aPBX.ConferenceRemoveChannel(split[0], split[1], false);
				}
				
				return true;
			}
		}
		
		if(commandName.equals("callistend")) {
			Boolean removeResult = QueueManager.GetInstance().RemoveCommand(option, pbx);
	
			//If it wasn't on the command queue then check the predictive dial queue
			if(!removeResult)
				removeResult = QueueManagerForPred.GetInstance().RemoveCommand(option, pbx);

			EventsReceived evt = new EventsReceived(commandName, fromNumber);
			evt.Message = (removeResult) ? "success" : "failed";
			evt.Exten = option;
			
			if(pbx != null)
				evt.pbxid = pbx.pbxID;
			
			String msg = evt.ToJson();
			network.SocketHandler.notifyAllDailers(msg);
		}
		else {
			//if(pbxType == PbxType.AVAYA || pbxType == PbxType.SIMULATION) {
				originator = pbx.GetCallOriginator(fromNumber, option);
				receiver = pbx.GetCallReceiver(option);
				
				if(receiver != null) {
					receiver.setting = setting;
					receiver.BaseID = BaseID;
				}
			//}
			
			Command command = CommandFactory.GetInstance().GetCommand(commandName, pbx, originator, receiver);
			
			if(command == null) {
				return false;
			}
			
			QueueManager.GetInstance().AddCommand(command);
		}
		return true;
	}

}
