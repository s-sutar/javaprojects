package core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class LogWriter {

	public static LogWriter instance = new LogWriter();

	public static String VERSION = "1.2.0.44";
	public static String logDir = "log/";
	public static String buildDir = logDir + "build_" + VERSION + "/";
	public static String logPath = buildDir + "log.txt";

	private LogWriter() {
	}

	public void initializeFile() {
		try {
			// check if logDir exists or not
			File dir = new File(logDir);
			if (dir.exists() == false) {
				dir.mkdir();
			}
						
			// check if buildDir exists or not
			dir = new File(buildDir);
			if (dir.exists() == false) {
				dir.mkdir();
			}

			// To make sure that it creates a new file for every start up.
			File oldFile = new File(logPath);

			if (oldFile.exists()) {
				DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HHmmss");
				Date date = new Date();
				String newFileName = "log-" + dateFormat.format(date) + "_end.txt";
				File newFile = new File(buildDir + newFileName);

				boolean isSuccess = oldFile.renameTo(newFile);

				if (isSuccess) {
					new Thread(() -> zipFile(newFileName)).start();
				} else {
					System.out.println("Failed initializeFile: " + newFileName);
				}
			}
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
	}

	public void Append(String text, String level) {

		try {
			File f = new File(logPath);
			PrintWriter writer = null;

			if (f.exists() == false) {
				f.createNewFile();				
			} else {
				double bytes = f.length();
				double kilobytes = (bytes / 1024);
				double megabytes = (kilobytes / 1024); // 1048576
				int limitSize = 20;

				if (megabytes > limitSize) {
					// backup and reset
					DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HHmmss");
					Date date = new Date();
					String newFileName = "log-" + dateFormat.format(date) + ".txt";
					File newFile = new File(buildDir + newFileName);
					copyFile(f, newFile);

					new Thread(() -> zipFile(newFileName)).start();

					writer = new PrintWriter(new FileWriter(logPath));
				}
			}

			writer = (writer == null) ? new PrintWriter(new FileWriter(logPath, true)) : writer;

			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
			Date date = new Date();
			String now = dateFormat.format(date);
			String log_msg = String.format("[%s] %S %s%n", now, level, text);

			writer.append(log_msg);
			writer.close();

			System.out.print(log_msg);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private static void copyFile(File source, File dest) throws IOException {
		InputStream input = null;
		OutputStream output = null;
		try {
			input = new FileInputStream(source);
			output = new FileOutputStream(dest);
			byte[] buf = new byte[1024];
			int bytesRead;
			while ((bytesRead = input.read(buf)) > 0) {
				output.write(buf, 0, bytesRead);
			}
		} catch (Exception e) {
			System.out.print(e.getMessage());
		} finally {
			input.close();
			output.close();
		}
	}

	private static void zipFile(String sourceFile) {
		if (sourceFile == null) {
			return;
		}

		byte[] buffer = new byte[1024];

		try {
			String outputFile = sourceFile.replace(".txt", ".zip");
			FileOutputStream fos = new FileOutputStream(buildDir + outputFile);
			ZipOutputStream zos = new ZipOutputStream(fos);
			ZipEntry ze = new ZipEntry(sourceFile);
			zos.putNextEntry(ze);
			FileInputStream in = new FileInputStream(buildDir + sourceFile);

			int len = 0;
			while ((len = in.read(buffer)) > 0) {
				zos.write(buffer, 0, len);
			}

			in.close();
			zos.closeEntry();
			zos.close();

			// delete source file
			File file = new File(buildDir + sourceFile);
			file.delete();
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
	}

	public void appendException(Exception e) {
		if (e == null) {
			return;
		}

		String errorType = e.toString();
		String errorMsg = e.getMessage();
		StackTraceElement[] traces = e.getStackTrace();
		String stackTrace = "";

		if (traces != null && traces.length > 0) {
			for (StackTraceElement trace : traces) {
				stackTrace = stackTrace.concat(String.format("line=%d, class=%s, method=%s%n", trace.getLineNumber(),
						trace.getClassName(), trace.getMethodName()));
			}
		}

		Append(String.format("%s%n%s%n%s", errorType, errorMsg, stackTrace), "ERROR");
	}

	public void appendWarning(String text) {
		Append(text, "WARNING");
	}

	public void appendError(String text) {
		Append(text, "ERROR");
	}

	public void appendInfo(String text) {
		Append(text, "INFO");
	}
	
	public void appendInfo(String text, int pbxID) {
		Append("PBX: " + pbxID + " " + text, "INFO");
	}
	
	public void appendQuery(String text) {
		Append(text, "QUERY");
	}
}
