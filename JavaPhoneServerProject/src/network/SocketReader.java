package network;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;

import pbx.Pbx.PbxType;
import core.AppProperties;
import core.CommandRequest;
import core.LogWriter;
import core.PhoneManager;
import mit.mitAvayaController;

public class SocketReader implements Runnable {
	private Socket mySocket = null;

	public SocketReader(Socket socket) {
		this.mySocket = socket;
	}
	
    public void onMessage(String msg) {
    	LogWriter.instance.appendInfo("SocketReader: in: " + msg);
        
		new Thread(msg) {
		    public void run() {
		    	 HandleCommand(msg);
		    }
		}.start();
    }
    
	public void HandleCommand(String json) {
		if(json == null || json.isEmpty()) {
			return;
		}
		
		boolean isSuccess = false;
		
		if(json.contains("\"EventName\":\"connected\"")) {
/*			EventsReceived evt = new EventsReceived(json);
			
			if(evt != null) {
				// initialize PBX
				PbxInfo pbxInfo = new PbxInfo(
						AppProperties.instance.serverIP,
						AppProperties.instance.portNumber,
						AppProperties.instance.login,
						AppProperties.instance.password,
						AppProperties.instance.serviceName);
				Pbx pbx = new AvayaPbx(pbxInfo);
				pbx.Login();
				PbxPool.GetInstance().AddPbx(pbx);
			}*/
		} else if(json.contains("\"Command\"")) {
			CommandRequest request = new CommandRequest(json);
			
			if(request != null && request.Command != null) {
				String cmd = request.Command.toLowerCase();
				String fromNumber = request.Channel;
				String option = request.Option;
				
				PbxType type = (AppProperties.instance.isSimulation) ? PbxType.SIMULATION : PbxType.AVAYA;
				if (AppProperties.instance.pbxtype.equals("mpbx")) {
					type = PbxType.MPBX;
				}

				isSuccess = PhoneManager.GetInstance().Process(type, cmd, fromNumber, option, request.Setting, request.BaseID, request.pbxID);
			}
		}	
		else {
			if (mitAvayaController.instance != null) {
				mitAvayaController.instance.sendMessageToListener("UNEXPECTED: " + json);
			}	
		} 
	}
	
	public void run() {
		while (mySocket != null) {
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(mySocket.getInputStream()));
		        String requestMsg = br.readLine();
		        
				if (requestMsg == null || requestMsg.isEmpty()) {
					continue;
				}

				requestMsg = requestMsg.trim();

				LogWriter.instance.appendInfo(
						String.format("Request: from=%s, msg=%s", mySocket.getRemoteSocketAddress(), requestMsg));
				
				String reply = "Response: pong\r\n\r\n";
				if (requestMsg.toLowerCase().startsWith("action:ping")) {
					if (requestMsg.contains("ActionID:")) {
						int actionIDIndex = requestMsg.indexOf("ActionID:") + "ActionID:".length();
						String actionID = requestMsg.substring(actionIDIndex);
						actionID = actionID.substring(0, actionID.length() - 1);
						reply = "Response: pong\r\nActionID: " + actionID + "\r\n\r\n";
					}
				}
				else {
					reply = "Response: Success\r\n\r\n";
					if (requestMsg.contains("ActionID:")) {
						int actionIDIndex = requestMsg.indexOf("ActionID:") + "ActionID:".length();
						String actionID = requestMsg.substring(actionIDIndex);
						actionID = actionID.substring(0, actionID.length() - 1);
						reply = "Response: Success\r\nActionID: " + actionID + "\r\n\r\n";
					}
				}

				if (mitAvayaController.instance != null) {
					mitAvayaController.instance.sendMessageToListener(reply);
				}
				else {
					DataOutputStream out = null;
					
					try {
						synchronized(mySocket) {
							out = new DataOutputStream(mySocket.getOutputStream());
							out.writeBytes(reply);
							out.flush();
							
							LogWriter.instance.appendInfo("sent: " + reply );
						}
					} catch (Exception ex) {
						LogWriter.instance.appendInfo("Socket abort when out.writeBytes");
						return;
					}
				}

				// Convert request message to Action				
				onMessage( requestMsg);				
				
			} catch (SocketException se) {
				if (se.getMessage() == "Connection reset") {
					// Stop monitor all virtual numbers
					//this.requestHandler.stopAllMonitoring(mySocket.getRemoteSocketAddress());
					LogWriter.instance.appendInfo("Client disconnected: "
							+ ((mySocket != null) ? mySocket.getRemoteSocketAddress() : "null"));
				} 				
				else if (se.getMessage() == "Socket is closed") {
					// Stop monitor all virtual numbers
					//this.requestHandler.stopAllMonitoring(mySocket.getRemoteSocketAddress());
					LogWriter.instance.appendInfo("Client socket closed: "
							+ ((mySocket != null) ? mySocket.getRemoteSocketAddress() : "null"));
				} else {
					LogWriter.instance.appendException(se);
				}

				if (mySocket != null && mySocket.isClosed() == false) {
					try {
						mySocket.close();
					} catch (Exception e2) {
						LogWriter.instance.appendException(e2);
					}
				}

				break;
			} catch (Exception e) {
				LogWriter.instance.appendException(e);

				/*
				 * if(mySocket != null && mySocket.isClosed() == false) { try {
				 * mySocket.close(); } catch (IOException e2) {
				 * LogWriter.instance.appendException(e2); } }
				 */

				break;
			}
		} // end while
		
		LogWriter.instance.appendInfo("Dialer socket disconnected");
		SocketHandler.removeSocket(mySocket);
	}// end run()
}// end inner class
