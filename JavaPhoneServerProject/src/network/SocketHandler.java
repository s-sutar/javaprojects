package network;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLSocket;

import core.LogWriter;
import mit.mitAvayaController;

public class SocketHandler extends Thread {
	private ServerSocket serverSocket = null;
	private SSLServerSocket SSLserverSocket = null;
	private boolean isClosing = false;
	private static ArrayList<Socket> socketList = new ArrayList<>();
	private static ArrayList<JettyWebSocketHandler> wsocketList = new ArrayList<>();

	public SocketHandler(SSLServerSocket serverSocket) throws IOException {
		this.SSLserverSocket = serverSocket;
		setName("SSLserversocket");
	}
	
	public SocketHandler(ServerSocket serverSocket) throws IOException {
		this.serverSocket = serverSocket;
		setName("serversocket");
	}
	
	public void close() {

		if( this.serverSocket == null) {
			return;
		}
		
		this.isClosing = true;
		try {
			this.serverSocket.close();
		} catch (IOException e) {
			LogWriter.instance.appendException(e);
		}
		this.serverSocket = null;		
	}

	public void run() {
		while (this.serverSocket != null || this.SSLserverSocket != null) {
			Socket mySocket = null;
			SSLSocket mySSLSocket = null;

			try {
				String log = String.format("Waiting for client at %s...", InetAddress.getLocalHost().getHostAddress());
				LogWriter.instance.appendInfo(log);
				if(this.SSLserverSocket != null) {
					mySSLSocket = (SSLSocket) this.SSLserverSocket.accept();

					if (mySSLSocket != null && this.SSLserverSocket != null && this.isClosing == false) {
						LogWriter.instance.appendInfo(
								"**********************************************************************************************************");
						LogWriter.instance.appendInfo(String.format("Connected: client=%s",
								mySSLSocket.getRemoteSocketAddress()));

						LogWriter.instance.appendInfo("SSL accept waiting for lock");
						synchronized(socketList) {
							socketList.add(mySSLSocket);
							LogWriter.instance.appendInfo("SSL releasing lock");
						}
						new Thread(new SocketReader(mySSLSocket), "SocketReader").start();
					}				
				}
				else {
					mySocket = this.serverSocket.accept();

					if (mySocket != null && this.serverSocket != null && this.isClosing == false) {
						LogWriter.instance.appendInfo(
								"**********************************************************************************************************");
						LogWriter.instance.appendInfo(String.format("Connected: client=%s",
								mySocket.getRemoteSocketAddress()));
	
						LogWriter.instance.appendInfo("socket waiting for lock");
						synchronized(socketList) {
							socketList.add(mySocket);
							LogWriter.instance.appendInfo("socket releasing lock");
						}
						new Thread(new SocketReader(mySocket), "SocketReader").start();
					}
				}
			} catch (Exception e) {
				if(this.isClosing == false) {
					LogWriter.instance.appendException(e);
				}

				if (mySocket != null && mySocket.isClosed() == false) {
					try {
						mySocket.close();
					} catch (IOException e2) {
						LogWriter.instance.appendException(e2);
					}
				}

				break;
			}

		} // end while

		synchronized(socketList) {
			if (socketList != null && socketList.size() > 0) {
				LogWriter.instance.appendInfo("SocketHandler - to close, socketList=" + socketList.size());
	
				for (Socket socket : socketList) {
					try {
						socket.close();
					} catch (IOException e) {
					}
	
					LogWriter.instance.appendInfo("SocketHandler - closed, client=" + socket.getLocalAddress().toString());
					socket = null;
				}
			} else {
				LogWriter.instance.appendInfo("SocketHandler - no socket to close");
			}
	
			socketList = null;
		}

		LogWriter.instance.appendInfo("*** terminated SocketHandler");
	}// end run()
	
	public static void removeSocket(Socket sock) {
		LogWriter.instance.appendInfo("removeSocket waiting for lock");
		synchronized(socketList) {
			socketList.remove(sock);
			LogWriter.instance.appendInfo("removeSocket releasing lock");
		}
	}
	
	public static void addwSocket(JettyWebSocketHandler ws) {
		synchronized(wsocketList) {
			wsocketList.add(ws);
		}		
	}
	
	public static void removewSocket(JettyWebSocketHandler ws) {
		synchronized(wsocketList) {
			wsocketList.remove(ws);
		}		
	}
	
	public static void notifyAllDailers(String reply) {
		int sockCount=0;
	
		LogWriter.instance.appendInfo("Send: " + reply);

		if (mitAvayaController.instance != null) {
			mitAvayaController.instance.sendMessageToListener(reply);
			return;
		}

		LogWriter.instance.appendInfo("notifyAllDailers - waiting for lock");
		synchronized(socketList) {
			LogWriter.instance.appendInfo("notifyAllDailers - has the lock");
			if (wsocketList.size() == 0 && socketList.size() == 0) {
				LogWriter.instance.appendInfo("notifyAllDailers - socketList is empty");
				return;
			}		
			
			//String framedreply = "Event:JSONEVENT\r\nMessage:" + reply + "\r\n\r\n";
			String framedreply = reply + "\r\n\r\n";
	
			Socket brokenSocket = null;
			for (Socket mySocket : socketList) {				
				DataOutputStream out = null;
			
				LogWriter.instance.appendInfo("notifyAllDailers - waiting for mySocket lock");
				synchronized(mySocket) {
					LogWriter.instance.appendInfo("notifyAllDailers - has mySocket lock");
					try {
						out = new DataOutputStream(mySocket.getOutputStream());
						out.writeBytes(framedreply);
						out.flush();
		
						LogWriter.instance.appendInfo("SentA ("+sockCount+"):" + framedreply);
						sockCount++;
					} catch (Exception ex) {
						brokenSocket = mySocket;
						LogWriter.instance.appendException(ex);
					}
				}
				LogWriter.instance.appendInfo("notifyAllDailers - released mySocket lock");
			}//end foreach
			
			if (brokenSocket != null) {
				LogWriter.instance.appendInfo("notifyAllDailers - cleanup broken socket");
				removeSocket(brokenSocket);
			}
		}
		LogWriter.instance.appendInfo("notifyAllDailers - released lock");
		
		synchronized(wsocketList) {
			//now the websockets
			JettyWebSocketHandler brokenwSocket = null;
			for (JettyWebSocketHandler mywSocket : wsocketList) {				
				
				try {
					mywSocket.sendMessage(reply);
	
					LogWriter.instance.appendInfo("SentB ("+sockCount+"):" + reply);
					sockCount++;
				} catch (Exception ex) {
					brokenwSocket = mywSocket;
					LogWriter.instance.appendException(ex);
				}
			}//end foreach
			
			if (brokenwSocket != null) {
				removewSocket(brokenwSocket);
			}
		}
	}
}
