package network;

import java.net.URI;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;

import core.AppProperties;
import core.EventsReceived;
import core.LogWriter;
import core.PhoneManager;

public class WebClient {
	public static boolean isShutdown = false;
	public static boolean isConnectingNow = false;
	private static WebClient _instance = new WebClient();
	private WebSocketClient client = null;
	private ClientSocket socket = null;
	private String savedURL;
	private boolean isAsterisk = false;
	private int retryCount = 0;
	
	public int pbxID = 0;
	public WebClient() {
	}
	
	public static WebClient GetInstance() {
		if(_instance == null) {
			synchronized(WebClient.class) {
				if(_instance == null) {
					_instance = new WebClient();
				}
			}
		}
		return _instance;
	}
	
	public void SendMessage(String msg) {
		if(this.socket == null || msg == null || msg.isEmpty()) {
			return;
		}
		
		this.socket.sendMessage(msg);
	}
	
	public String Connect(String url, boolean isAsterisk) {
		if(url == null)
			url = savedURL;
		else
			savedURL = url;		

		this.isAsterisk = isAsterisk;
		
		String errorStr = "";
		
		if(PhoneManager.isRunning == false) {
			LogWriter.instance.appendInfo("Terminated WebClient connecting - PhoneManager.isRunning=false");
			
			return errorStr;
		}
		
		synchronized(this) {
			if( isConnectingNow == true ){
				return errorStr;
			}
			isConnectingNow = true;
		}
		
		Close();
		
		if(url.startsWith("wss")) {
			SslContextFactory sslf = new SslContextFactory();
			sslf.setTrustAll(true);
			this.client = new WebSocketClient(sslf);		
		}
		else {
			this.client = new WebSocketClient();		
		}
		
		this.socket = new ClientSocket(isAsterisk, pbxID, this);
		
		try {
			URI toUri = new URI(url);
			if(this.client != null) {
				this.client.start();
				this.client.connect(this.socket, toUri, new ClientUpgradeRequest());
			}
			
			LogWriter.instance.appendInfo("WebClient: connecting to " + toUri);
			
			if(this.socket != null) {
				this.socket.awaitClose(5, TimeUnit.SECONDS);
			}
			
			if(this.socket != null && this.socket.isConnected && this.client.isRunning()) {
				LogWriter.instance.appendInfo("WebClient: connected to " + toUri);
				final ClientSocket mySocket = this.socket;
				retryCount = 0;

				sendAsteriskConnectEvent();
				
				String machineName = AppProperties.instance.hostname + "-debug:" + java.util.UUID.randomUUID();
				String loginRequest = "{\"Secret\":\"sakura123\",\"Sender\":\"" + machineName + "\",\"UserName\":\"predial\"}";
				SendMessage(loginRequest);				
				
	            new Thread("webClient") {	            	
	            	int pingTime = 30;
	            	
					public void run() {
						LogWriter.instance.appendInfo("Created - Pinging loop");
						
	                    while(PhoneManager.isRunning && mySocket != null && mySocket.isConnected) {
	                    	try {
	                    		mySocket.sendMessage("{\"ping\":\"true\"}");
	                    		
	                    		int count = 0;
	                    		//while(count < pingTime && PhoneManager.isRunning) {
	                    		while(count < pingTime && PhoneManager.isRunning && mySocket != null && mySocket.isConnected) {
	                    			sleep(1000);
	                    			count++;
	                    		}
							} catch (InterruptedException e) {
								LogWriter.instance.appendException(e);
							}
	                    }
	                    
	                    LogWriter.instance.appendInfo("Terminated - Pinging loop");
	                }
	            }.start();				
			} else {
				String details = "socket is not null";
				//this.socket != null && this.socket.isConnected && this.client.isRunning()
				if (this.socket == null) {
					details = "socket is null";
				} else if(this.socket.isConnected) {
					details = "socket is not connected";
				}

				if (this.client != null && this.client.isRunning()) {
					details += " & client is not running";
				}
				
				errorStr = "WebClient: failed to connect to=" + toUri;

				LogWriter.instance.appendError("WebClient: failed to connect to="+ toUri + ", details=" + details);
				retryCount++;
				//jack
				//this.Close();
				
				if(retryCount > AppProperties.instance.asteriskReconAttemptCount) {
					sendAsteriskDownEvent();								
				}
				else if(!WebClient.isShutdown) {
					sendAsteriskDiscEvent();
		            new Thread() {		            	
						public void run() {
							int waitTime = AppProperties.instance.mpbx_reconnect_waittime;
							LogWriter.instance.appendInfo("Try to connect to web server again in " + waitTime + " seconds, url=" + toUri);
							
							try {
								for(int i = 0; i < waitTime; i++) {
									if(WebClient.isShutdown) { 
										isConnectingNow = false;
										return;
									}
									
									Thread.sleep(1000);
								}
							} catch (InterruptedException e1) {
								LogWriter.instance.appendException(e1);
							}
							
							if(!WebClient.isShutdown) {
								isConnectingNow = false;
								Connect(savedURL, isAsterisk);
							}
		                }
		            }.start();
				}
			}
		} catch(Exception e) {
			LogWriter.instance.appendException(e);
			errorStr = "WebClient exception: " + e.getMessage();
		}//end try-catch
		
		isConnectingNow = false;
		
		return errorStr;
	}
	
	public void onCloseCallback(Integer reasonCode) {
		LogWriter.instance.appendError("WebClient: disconnected from "+ savedURL);		
		retryCount++;
		
		if(retryCount > AppProperties.instance.asteriskReconAttemptCount) {
			sendAsteriskDownEvent();			
		}
		else {
			sendAsteriskDiscEvent();
			new Thread("webReconnectClient") {	            	      	
				public void run() {
					int waitTime = AppProperties.instance.mpbx_reconnect_waittime;
					LogWriter.instance.appendInfo("Waiting to reconnect to server again in " + waitTime + " seconds, url=" + savedURL);
					
					try {
						for(int i = 0; i < waitTime; i++) {
							if(WebClient.isShutdown) { 
								isConnectingNow = false;
								return;
							}
							
							Thread.sleep(1000);
						}
					} catch (InterruptedException e1) {
						LogWriter.instance.appendException(e1);
					}
					
					if(!WebClient.isShutdown) {
						isConnectingNow = false;
						Connect(savedURL, isAsterisk);
					}
	            }
	        }.start();
		}
	}
	
	public void sendAsteriskDownEvent() {
		EventsReceived evt = new EventsReceived("asteriskdown", "");
		evt.pbxid = pbxID;
		String outmsg = evt.ToJson();
		// LogWriter.instance.appendInfo(String.format("Response: %s", outmsg));
		network.SocketHandler.notifyAllDailers(outmsg);		
	}	
	
	public void sendAsteriskDiscEvent() {
		EventsReceived evt = new EventsReceived("asteriskdisconnect", "");
		evt.pbxid = pbxID;
		String outmsg = evt.ToJson();
		// LogWriter.instance.appendInfo(String.format("Response: %s", outmsg));
		network.SocketHandler.notifyAllDailers(outmsg);		
	}	
	
	public void sendAsteriskConnectEvent() {
		EventsReceived evt = new EventsReceived("asteriskconnect", "");
		evt.pbxid = pbxID;
		String outmsg = evt.ToJson();
		// LogWriter.instance.appendInfo(String.format("Response: %s", outmsg));
		network.SocketHandler.notifyAllDailers(outmsg);		
	}	

	public void Close() {		
		try {
			try {
				if(this.socket != null) {
					this.socket.close();
					this.socket = null;
					
					LogWriter.instance.appendInfo("WebClient: stop ClientSocket succeeded");
				}
			} catch (Exception e) {
				LogWriter.instance.appendInfo("WebClient: stop ClientSocket failed, err=" + e.getMessage());
			}
			
			try {
				if (this.client != null && this.client.isRunning()) {
					this.client.stop();
					this.client = null;
					
					LogWriter.instance.appendInfo("WebClient: stop WebSocketClient succeeded");
				}
			} catch (Exception e) {
				LogWriter.instance.appendInfo("WebClient: stop WebSocketClient failed, err=" + e.getMessage());
			}
		} catch (Exception e) {
			LogWriter.instance.appendException(e);
		}
		
		LogWriter.instance.appendInfo("WebClient: clean up");
	}

}
