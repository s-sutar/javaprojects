package network;

import java.io.IOException;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.jetty.websocket.api.RemoteEndpoint;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import pbx.Pbx.PbxType;
import core.AppProperties;
import core.CommandRequest;
import core.LogWriter;
import core.PhoneManager;

@WebSocket
public class JettyWebSocketHandler {
	RemoteEndpoint endpoint = null;
	private PingTimerTask timerTask = null;
	private Timer pingtimer = null;
	private Date lastActive = new Date();

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
    	if (pingtimer != null) {
    		pingtimer.cancel();
    		pingtimer = null;
    	}
    	
		LogWriter.instance.appendInfo("Close: statusCode=" + statusCode + ", reason=" + reason);
		network.SocketHandler.removewSocket(this);
    }

    @OnWebSocketError
    public void onError(Throwable t) {
    	if (pingtimer != null) {
    		pingtimer.cancel();
    		pingtimer = null;
    	}
    	
		LogWriter.instance.appendInfo("Error: " + t.getMessage());
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
		LogWriter.instance.appendInfo("Connect: " + session.getRemoteAddress().getAddress());
        try {
        	reschedulePing();
            
            session.getRemote().sendString("{\"EventName\":\"connected\"}");
            
            endpoint = session.getRemote();
            
    		network.SocketHandler.addwSocket(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnWebSocketMessage
    public void onMessage(String message) {
    	LogWriter.instance.appendInfo("JettyWebSocketHandler: in: " + message);
        
    	this.lastActive = new Date();
    	
		new Thread(message) {
		    public void run() {
		    	 HandleCommand(message);
		    }
		}.start();
    }
    
    public void sendMessage(String json) {
        try {
        	if( endpoint != null) {
        		synchronized(endpoint) {
        	    	this.lastActive = new Date();
        			endpoint.sendString(json);
        		}
        	}
        } catch (IOException e) {
            e.printStackTrace();
        }    	
    }
    
    public long timeIdle() {
    	Date now = new Date();
    	
    	return (now.getTime() - this.lastActive.getTime());
    }
    
    public void reschedulePing() {
    	long nextTime = AppProperties.instance.pingInterval - timeIdle();

    	if (nextTime < 1000) {
    		nextTime = 1000;
    	}
    	
    	timerTask = new PingTimerTask(this);
    	pingtimer = new Timer(true);
    	pingtimer.schedule(timerTask, nextTime);
    }
    
    public void processPing()
    {
		if( timeIdle() >= AppProperties.instance.pingInterval - 1000) {
			sendMessage("PING");
		}
		reschedulePing();
    }
    
	public void HandleCommand(String json) {
		if(json == null || json.isEmpty()) {
			return;
		}
		
		boolean isSuccess = false;
		
		if(json.contains("\"Command\"")) {
			CommandRequest request = new CommandRequest(json);
			
			if(request != null && request.Command != null) {
				String cmd = request.Command.toLowerCase();
				String fromNumber = request.Channel;
				String option = request.Option;
				
				PbxType type = (AppProperties.instance.isSimulation) ? PbxType.SIMULATION : PbxType.AVAYA;
				isSuccess = PhoneManager.GetInstance().Process(type, cmd, fromNumber, option, request.Setting, request.BaseID, request.pbxID);
			}
		}
		
		if(isSuccess) {
			
		} else {
			
		}
	}
    
}
