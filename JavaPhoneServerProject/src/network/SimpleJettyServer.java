package network;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.websocket.server.WebSocketHandler;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;


public class SimpleJettyServer {
	private Server server;
	
	public SimpleJettyServer(int port) {
        try {
	        server = new Server(port);
	        WebSocketHandler wsHandler = new WebSocketHandler() {
	            @Override
	            public void configure(WebSocketServletFactory factory) {
	                factory.register(JettyWebSocketHandler.class);
	            }
	        };
        	server.setHandler(wsHandler);
			server.start();
			//server.join();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
    public void stop() throws Exception {
    	if( server != null ) {
    		server.stop();
    		server.join();
    	}
    }
	
}

