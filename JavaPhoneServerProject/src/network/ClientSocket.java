package network;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map.Entry;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.StatusCode;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import core.AppProperties;
import core.AsteriskEvent;
import core.CommandRequest;
import core.CommandRequestPBXStart;
import core.EventsReceived;
import core.LogWriter;
import core.PhoneManager;
import core.RecordInfo;
import core.openClosePBXEvent;
import mit.mitAvayaController;
import pbx.Pbx;
import pbx.PbxPool;
import pbx.astCallInfo;
import pbx.astCallOriginator;
import pbx.astCallReceiver;
import pbx.astPbx;
import pbx.Pbx.PbxType;

@WebSocket(maxTextMessageSize = 64 * 1024, maxIdleTime = 120000)
public class ClientSocket {

	private final CountDownLatch closeLatch;
	public boolean isConnected = false;
	public boolean isAsterisk = false;
	public int retryCount = 0;
	public int mpbxID = 0;
	
	private WebClient myParent;

	// @SuppressWarnings("unused")
	private Session session;

	public ClientSocket(boolean isAsterisk, int pbxID, WebClient parent) {
		this.isAsterisk = isAsterisk;
		this.mpbxID = pbxID;
		this.myParent = parent;
		this.closeLatch = new CountDownLatch(1);
	}

	public boolean awaitClose(int duration, TimeUnit unit) throws InterruptedException {
		return this.closeLatch.await(duration, unit);
	}

	@OnWebSocketClose
	public void onClose(int statusCode, String reason) {
		isConnected = false;
		LogWriter.instance.appendInfo(String.format("ClientSocket onClose: session:%d %d - %s", this.session.hashCode(), statusCode, reason));
		this.session = null;
		this.closeLatch.countDown();

		if (PhoneManager.isRunning) {
			if( myParent != null)
			{
				myParent.onCloseCallback(statusCode);
			} 
		}
	}

	@OnWebSocketConnect
	public void onConnect(Session session) {
		isConnected = true;
		this.session = session;

		LogWriter.instance.appendInfo(
				String.format("ClientSocket: session:%d %s, %s", this.session.hashCode(), this.session.getLocalAddress(), this.session.getRemoteAddress()));
	}

	public void sendMessage(String msg) {
		if (msg == null || msg.isEmpty()) {
			return;
		}

		try {
			if (this.session != null) {
				Future<Void> fut = this.session.getRemote().sendStringByFuture(msg);
				fut.get(2, TimeUnit.SECONDS);
			}

			LogWriter.instance.appendInfo(String.format("ClientSocket: out: %s", msg));
		} catch (Exception e) {
			LogWriter.instance.appendError("Failed to send: " + msg);
			LogWriter.instance.appendException(e);
		}
	}

	public void close() {
		try {
			if (this.session != null) {
				this.session.close(StatusCode.NORMAL, "close from command");
			}
		} catch (Exception e) {
			LogWriter.instance.appendException(e);
		}
	}

	@OnWebSocketMessage
	public void onMessage(String msg) {

		if (msg.contains("\"Command\"")) {
			LogWriter.instance.appendInfo("ClientSocket: in: " + msg);
			new Thread("ClientSocket") {
				public void run() {
					HandleCommand(msg);
				}
			}.start();
		} else {
			AsteriskEvent ast = null;
			try {
				LogWriter.instance.appendInfo("Event from Asterisk: " + msg);
				ast = new AsteriskEvent(msg);
			} catch (Exception ex) {
				LogWriter.instance.appendInfo(String.format("ClientSocket: onMessage: %s", msg));
				LogWriter.instance.appendException(ex);
			}

			astPbx pbx = (astPbx) PbxPool.GetInstance().GetPbx(Pbx.PbxType.MPBX, mpbxID);

			if (pbx != null) {
				boolean isonList = false;
				String CUID = "";
				String SystemCallID = "";
				String AgentChannel = "0";
				String Exten = "0";
				boolean isAgent = false;
				String delayedDialNumber = "";
				String delayedMonitorDial = "";

				astCallInfo callInfo = null;

				if (ast.channel != null) {
					 callInfo = astPbx.channelList.get(ast.channel.id);

					// int callIndex = astPbx.channelList.get(ast.channel.id);
					if (astPbx.channelList.containsKey(ast.channel.id)) {
						isonList = true;

						callInfo = astPbx.channelList.get(ast.channel.id);
						CUID = callInfo.ToCUNumber;
						SystemCallID = ast.channel.id;
						isAgent = callInfo.isAgent;
						AgentChannel = callInfo.AgentExtension;
						Exten = callInfo.ToNumber;
						delayedDialNumber = callInfo.delayedDial;
					    delayedMonitorDial = callInfo.delayedMonitorDial;
					}
					
					else {
						
						if(ast.variable!= null && ast.variable.equals("MIXMONITOR_FILENAME")) {
							for (Entry<String, astCallInfo> p : astPbx.channelList.entrySet()) {
									if(p.getValue().ToNumber.equals(ast.channel.dialplan.exten)) {
										
										 EventsReceived evt = new EventsReceived("MIXMONITOR_FILENAME", CUID); 
										 evt.SystemCallID = p.getKey();
										 evt.Exten = p.getValue().ToNumber;
										 evt.pbxid = mpbxID; 
										 evt.Message = ast.value;
										 String outmsg = evt.ToJson(); //
										 LogWriter.instance.appendInfo(String.format("Response: %s",
										 outmsg)); 
										 network.SocketHandler.notifyAllDailers(outmsg);
									}
										
								
							}
							
							
						}
					}
				}

				if (isonList) {
					LogWriter.instance.appendInfo("AsteriskEvent: " + ast.ToString());

					if (isAgent) { // It is an agent event
						if (ast.type.equals("ChannelStateChange")) {
							if (ast.channel.state.equals("Up")) { // agent answered the ready call
								
								if(delayedMonitorDial != null && delayedMonitorDial.isEmpty() == false) {

									
									 EventsReceived evt = new EventsReceived("SVEvent", CUID); 
									 evt.SystemCallID = SystemCallID;
									 evt.Exten = Exten;
									 evt.pbxid = mpbxID; 
									 String outmsg = evt.ToJson(); //
									 LogWriter.instance.appendInfo(String.format("Response: %s",
									 outmsg)); 
									 network.SocketHandler.notifyAllDailers(outmsg);
									 
									 
								}else

								if (delayedDialNumber != null && delayedDialNumber.isEmpty() == false) {
									// Agent answered, now dial the number
									astCallOriginator originator = new astCallOriginator();
									originator.FromNumber = CUID; // for a call to agent the CUID is the agent's
																	// extension
									astCallReceiver receiver = new astCallReceiver();
									receiver.ToNumber = delayedDialNumber;

									/*
									 * EventsReceived evt = new EventsReceived("AgentEvent", CUID); evt.SystemCallID
									 * = SystemCallID; evt.Exten = Exten; evt.pbxid = mpbxID; String outmsg =
									 * evt.ToJson(); // LogWriter.instance.appendInfo(String.format("Response: %s",
									 * outmsg)); network.SocketHandler.notifyAllDailers(outmsg);
									 */
									
								} else {
									EventsReceived evt = new EventsReceived("ReadyEvent", CUID);
									evt.SystemCallID = SystemCallID;
									evt.Exten = Exten;
									evt.pbxid = mpbxID;
									String outmsg = evt.ToJson();
									// LogWriter.instance.appendInfo(String.format("Response: %s", outmsg));
									network.SocketHandler.notifyAllDailers(outmsg);
								}
							}
						} else if (ast.type.equals("ChannelDestroyed")) { // agent hung up their extension
							if(callInfo != null)
								callInfo.isInConfRoom = false;

							EventsReceived evt = new EventsReceived("ReadyConfHangup", CUID);
							evt.SystemCallID = SystemCallID;
							evt.Exten = Exten;
							evt.cause = ast.cause;
							evt.pbxid = mpbxID;
							String outmsg = evt.ToJson();
							// LogWriter.instance.appendInfo(String.format("Response: %s", outmsg));
							network.SocketHandler.notifyAllDailers(outmsg);

							astPbx.channelList.remove(ast.channel.id);
						}
					} else {
						EventsReceived evt = null;
						String outmsg = "";
						switch (ast.type) {
						case "ChannelDestroyed":
							if ("Ring".equals(ast.channel.state)) {
							//	pbx.cleanupConferences(SystemCallID);

								evt = new EventsReceived("Cancel", AgentChannel);
								evt.CustomerID = CUID;
								evt.SystemCallID = SystemCallID;
								evt.Exten = Exten;
								evt.pbxid = mpbxID;
								outmsg = evt.ToJson();
								network.SocketHandler.notifyAllDailers(outmsg);

								astPbx.channelList.remove(ast.channel.id);
							}else if("Ringing".equals(ast.channel.state)) {
							
								pbx.cleanupConferences(SystemCallID);

								evt = new EventsReceived("CustomerNoanswer", AgentChannel);
								evt.CustomerID = CUID;
								evt.SystemCallID = SystemCallID;
								evt.Exten = Exten;
								evt.pbxid = mpbxID;
								outmsg = evt.ToJson();
								network.SocketHandler.notifyAllDailers(outmsg);

								astPbx.channelList.remove(ast.channel.id);
							
							}else {
							//	if (!"Ring".equals(ast.channel.state) && !"Ringing".equals(ast.channel.state)) {
								pbx.cleanupConferences(SystemCallID);
	
								evt = new EventsReceived("CustomerHangup", AgentChannel);
								evt.CustomerID = CUID;
								evt.SystemCallID = SystemCallID;
								evt.Exten = Exten;
								evt.pbxid = mpbxID;
								evt.Reason = (ast.cause_txt == null? ast.cause + ":":ast.cause + ":" + ast.cause_txt);
								evt.Reason = "DIAL FAILED :" + evt.Reason;
								outmsg = evt.ToJson();
								
								network.SocketHandler.notifyAllDailers(outmsg);
								RecordInfo recordinfo = new RecordInfo();
								recordinfo.EventName = "recordingstop";
								recordinfo.CustomerID = CUID;
								recordinfo.toPhoneNumber = CUID;
								recordinfo.endtime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
								recordinfo.pbxID = mpbxID;
								
								outmsg = recordinfo.ToJson();

								network.SocketHandler.notifyAllDailers(outmsg);
								
								if(callInfo.SnoopChannel != null) {
									LogWriter.instance.appendInfo("[CustomerHangup@ClientSocket] Snoop channel deleted " + callInfo.SnoopChannel);
									astPbx.channelList.remove(callInfo.SnoopChannel);

								}
								astPbx.channelList.remove(ast.channel.id);
								
							}
							break;
						case "ChannelHangupRequest":
							if (ast.cause != null) {
								switch (ast.cause) {
								case "1":
									pbx.cleanupConferences(SystemCallID);

									evt = new EventsReceived("NumberMistake", AgentChannel);
									evt.CustomerID = CUID;
									evt.SystemCallID = SystemCallID;
									evt.Exten = Exten;
									evt.pbxid = mpbxID;
									outmsg = evt.ToJson();
									
									network.SocketHandler.notifyAllDailers(outmsg);

									astPbx.channelList.remove(ast.channel.id);
									break;
								case "2":
									pbx.cleanupConferences(SystemCallID);

									evt = new EventsReceived("NumberMistake", AgentChannel);
									evt.CustomerID = CUID;
									evt.SystemCallID = SystemCallID;
									evt.Exten = Exten;
									evt.pbxid = mpbxID;
									outmsg = evt.ToJson();
									
									network.SocketHandler.notifyAllDailers(outmsg);

									astPbx.channelList.remove(ast.channel.id);
									break;
								case "3":
									pbx.cleanupConferences(SystemCallID);

									evt = new EventsReceived("NumberMistake", AgentChannel);
									evt.CustomerID = CUID;
									evt.SystemCallID = SystemCallID;
									evt.Exten = Exten;
									evt.pbxid = mpbxID;
									outmsg = evt.ToJson();
									
									network.SocketHandler.notifyAllDailers(outmsg);

									astPbx.channelList.remove(ast.channel.id);
									break;
								case "17":
									pbx.cleanupConferences(SystemCallID);

									evt = new EventsReceived("CustomerBusy", AgentChannel);
									evt.CustomerID = CUID;
									evt.SystemCallID = SystemCallID;
									evt.Exten = Exten;
									evt.pbxid = mpbxID;
									outmsg = evt.ToJson();
									network.SocketHandler.notifyAllDailers(outmsg);

									astPbx.channelList.remove(ast.channel.id);
									break;
							
								case "18":
									pbx.cleanupConferences(SystemCallID);
									evt = new EventsReceived("Offline", AgentChannel);
									evt.CustomerID = CUID;
									evt.SystemCallID = SystemCallID;
									evt.Exten = Exten;
									evt.pbxid = mpbxID;
									outmsg = evt.ToJson();
									network.SocketHandler.notifyAllDailers(outmsg);

									astPbx.channelList.remove(ast.channel.id);
									break;
								case "19":
									pbx.cleanupConferences(SystemCallID);

									evt = new EventsReceived("Offline", AgentChannel);
									evt.CustomerID = CUID;
									evt.SystemCallID = SystemCallID;
									evt.Exten = Exten;
									evt.pbxid = mpbxID;
									outmsg = evt.ToJson();
									network.SocketHandler.notifyAllDailers(outmsg);

									astPbx.channelList.remove(ast.channel.id);
									break;
								case "20":
									pbx.cleanupConferences(SystemCallID);

									evt = new EventsReceived("NumberMistake", AgentChannel);
									evt.CustomerID = CUID;
									evt.SystemCallID = SystemCallID;
									evt.Exten = Exten;
									evt.pbxid = mpbxID;
									outmsg = evt.ToJson();
									network.SocketHandler.notifyAllDailers(outmsg);

									astPbx.channelList.remove(ast.channel.id);
									break;
								case "21":
									pbx.cleanupConferences(SystemCallID);

									evt = new EventsReceived("Cancel", AgentChannel);
									evt.CustomerID = CUID;
									evt.SystemCallID = SystemCallID;
									evt.Exten = Exten;
									evt.pbxid = mpbxID;
									outmsg = evt.ToJson();
									network.SocketHandler.notifyAllDailers(outmsg);

									astPbx.channelList.remove(ast.channel.id);
									break;
								case "22":
									pbx.cleanupConferences(SystemCallID);

									evt = new EventsReceived("NumberChanged", AgentChannel);
									evt.CustomerID = CUID;
									evt.SystemCallID = SystemCallID;
									evt.Exten = Exten;
									evt.pbxid = mpbxID;
									outmsg = evt.ToJson();
									network.SocketHandler.notifyAllDailers(outmsg);

									astPbx.channelList.remove(ast.channel.id);
									break;
							
								case "27"://27 - destination out of order

									pbx.cleanupConferences(SystemCallID);

									evt = new EventsReceived("Offline", AgentChannel);
									evt.CustomerID = CUID;
									evt.SystemCallID = SystemCallID;
									evt.Exten = Exten;
									evt.pbxid = mpbxID;
									outmsg = evt.ToJson();
									network.SocketHandler.notifyAllDailers(outmsg);

									astPbx.channelList.remove(ast.channel.id);
									break;
								case "28"://27 - destination out of order

									pbx.cleanupConferences(SystemCallID);

									evt = new EventsReceived("NumberMistake", AgentChannel);
									evt.CustomerID = CUID;
									evt.SystemCallID = SystemCallID;
									evt.Exten = Exten;
									evt.pbxid = mpbxID;
									outmsg = evt.ToJson();
									network.SocketHandler.notifyAllDailers(outmsg);

									astPbx.channelList.remove(ast.channel.id);
									break;
								
								default :
									if(ast.channel.name.contains("Snoop")) {
										astPbx.channelList.remove(ast.channel.id);
									}
									break;
								}
							} else if ("Ring".equals(ast.channel.state) || "Ringing".equals(ast.channel.state)) {
								pbx.cleanupConferences(SystemCallID);

								evt = new EventsReceived("CustomerNoanswer", AgentChannel);
								evt.CustomerID = CUID;
								evt.SystemCallID = SystemCallID;
								evt.Exten = Exten;
								evt.pbxid = mpbxID;
								outmsg = evt.ToJson();
								// LogWriter.instance.appendInfo(String.format("Response: %s", outmsg));
								network.SocketHandler.notifyAllDailers(outmsg);

								astPbx.channelList.remove(ast.channel.id);
							}
							break;
						case "ChannelStateChange":
							if (ast.channel.state.equals("Up")) { // customer answered
								evt = new EventsReceived("CustomerAnswer", AgentChannel);
								evt.CustomerID = CUID;
								evt.SystemCallID = SystemCallID;
								evt.Exten = Exten;
								evt.pbxid = mpbxID;
								outmsg = evt.ToJson();
								// LogWriter.instance.appendInfo(String.format("Response: %s", outmsg));
								network.SocketHandler.notifyAllDailers(outmsg);
							}
							break;
						case "ChannelLeftBridge":
							if(ast.channel.name.contains("Snoop")) {
								astPbx.channelList.remove(ast.channel.id);
							}
							break;
						}
					}
				}
			}
		}
	}
	
	public void HandleCommand(String json) {
		if (json == null || json.isEmpty()) {
			return;
		}

		boolean isSuccess = false;

		if (json.contains("\"EventName\":\"connected\"")) {
			/*
			 * EventsReceived evt = new EventsReceived(json);
			 * 
			 * if(evt != null) { // initialize PBX PbxInfo pbxInfo = new PbxInfo(
			 * AppProperties.instance.serverIP, AppProperties.instance.portNumber,
			 * AppProperties.instance.login, AppProperties.instance.password,
			 * AppProperties.instance.serviceName); Pbx pbx = new AvayaPbx(pbxInfo);
			 * pbx.Login(); PbxPool.GetInstance().AddPbx(pbx); }
			 */
		} else if (json.contains("\"Command\"")) {
			CommandRequest request = new CommandRequest(json);

			if (request != null && request.Command != null) {
				String cmd = request.Command.toLowerCase();
				if (cmd.equals("openpbx")) {
					CommandRequestPBXStart reqStart = new CommandRequestPBXStart(json);
					if (mitAvayaController.instance != null) {
						Pbx.PbxType pbxTypeInt = Pbx.PbxType.MPBX;
						if(reqStart.pbxType.equals("mpbx"))
							pbxTypeInt = Pbx.PbxType.MPBX;
						else if(reqStart.pbxType.equals("avaya"))
							pbxTypeInt = Pbx.PbxType.AVAYA;

						String errorStr = mitAvayaController.instance.configureAndOpenPBX(pbxTypeInt, reqStart.pbx_address, reqStart.pbx_port, reqStart.pbx_username, reqStart.pbx_password, reqStart.aesservicename, reqStart.stasis_app, reqStart.trunk_name,reqStart.pbx_id);
						
						openClosePBXEvent ocEvent = new openClosePBXEvent("openpbxResponse", reqStart.pbxType, reqStart.pbx_id, "success", null, null);
						if (!errorStr.isEmpty())
							ocEvent = new openClosePBXEvent("openpbxResponse", reqStart.pbxType, reqStart.pbx_id, "failed", "400", errorStr);
						
						if (mitAvayaController.instance != null) {
							mitAvayaController.instance.sendMessageToListener(ocEvent.toJson());
						}	
					}
				}
				else if (cmd.equals("closepbx")) {
					CommandRequestPBXStart reqStart = new CommandRequestPBXStart(json);
					if (mitAvayaController.instance != null) {
						Pbx.PbxType pbxTypeInt = Pbx.PbxType.MPBX;
						if(reqStart.pbxType.equals("mpbx"))
							pbxTypeInt = Pbx.PbxType.MPBX;
						else if(reqStart.pbxType.equals("avaya"))
							pbxTypeInt = Pbx.PbxType.AVAYA;
						
						String errorStr = mitAvayaController.instance.closePBX(pbxTypeInt, reqStart.pbx_id);
						
						openClosePBXEvent ocEvent = new openClosePBXEvent("closepbxResponse", reqStart.pbxType, reqStart.pbx_id, "success", null, null);
						if (!errorStr.isEmpty())
							ocEvent = new openClosePBXEvent("closepbxResponse", reqStart.pbxType, reqStart.pbx_id, "failed", "400", errorStr);
						
						if (mitAvayaController.instance != null) {
							mitAvayaController.instance.sendMessageToListener(ocEvent.toJson());
						}	
					}
				}
				else {
					String fromNumber = request.Channel;
					String option = request.Option;
					
					if(cmd.equals("callistend")) {
						option = request.callid;
						fromNumber = "dialer";
					}
	
					PbxType type = (AppProperties.instance.isSimulation) ? PbxType.SIMULATION : PbxType.AVAYA;
	
					if (AppProperties.instance.pbxtype.equals("mpbx")) {
						type = PbxType.MPBX;
					}
	
					isSuccess = PhoneManager.GetInstance().Process(type, cmd, fromNumber, option, request.Setting, request.BaseID, request.pbxID);
				}
			}
		}

		if (isSuccess) {

		} else {

		}

	}
}