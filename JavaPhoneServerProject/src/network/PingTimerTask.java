package network;

import java.util.TimerTask;

public class PingTimerTask extends TimerTask {
	private JettyWebSocketHandler parentSocket;
	
	PingTimerTask(JettyWebSocketHandler parent)
	{
		this.parentSocket = parent;
	}
	
	@Override
	public void run() {
		this.parentSocket.processPing();
	}

}
