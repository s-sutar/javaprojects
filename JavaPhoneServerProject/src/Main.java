import java.net.ServerSocket;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.Date;
import java.util.ArrayList;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;

import org.boris.winrun4j.AbstractService;
import org.boris.winrun4j.ServiceException;
import org.eclipse.jetty.util.resource.Resource;

import core.AppProperties;
import core.LogWriter;
import core.PhoneManager;
import db.DBManager;
import network.ClientSocket;
import network.SecureJettyServer;
import network.SimpleJettyServer;
import network.SocketHandler;
import network.WebClient;
import pbx.AvayaCallControlConnectionListener;
import pbx.AvayaPbx;
import pbx.Pbx;
import pbx.PbxInfo;
import pbx.PbxPool;
import pbx.SimCall;
import pbx.SimPbx;
import pbx.astPbx;

public class Main extends AbstractService {

	public static void main(String[] args) throws ServiceException {
		Main instance = new Main();
		instance.serviceMain(args);
	}

	@Override
	public int serviceMain(String[] arg0) throws ServiceException {
		boolean isConsole = (arg0 != null && arg0.length > 0) ? true : false;		
		
		LogWriter.instance.initializeFile();
		AppProperties.instance.load();
		DBManager.GetInstance().initialize();
		LogWriter.instance.appendInfo("Starting version " + LogWriter.VERSION);
		LogWriter.instance.appendInfo("PhoneManager.isRunning = true");
		PhoneManager.isRunning = true;
		
		//Test for calllistend command
/*		configurePBX(1);
		ClientSocket csock = new ClientSocket(true, 0, null);
		for( int i = 0; i < 30; i++)
		csock.HandleCommand("{\"Reason\":0,\"CallIndex\":0,\"Channel\":\"231915\",\"Command\":\"dialpredictive\",\"Option\":\"0136442296;77060;0*0418061*;425;12;桜井ユウ子\",\"Setting\":\"12,10,31\",\"pbxID\":1}");
		csock.HandleCommand("{\"Reason\":0,\"CallIndex\":0,\"Channel\":\"231915\",\"Command\":\"dialpredictive\",\"Option\":\"0136442296;77061;0*0418061*;425;12;桜井ユウ子\",\"Setting\":\"12,10,31\",\"pbxID\":1}");
		csock.HandleCommand("{\"Reason\":0,\"CallIndex\":0,\"Channel\":\"231915\",\"Command\":\"dialpredictive\",\"Option\":\"0136442296;77062;0*0418061*;425;12;桜井ユウ子\",\"Setting\":\"12,10,31\",\"pbxID\":1}");
		csock.HandleCommand("{\"Command\":\"callistend\",\"callid\":\"77061-0136442296\",\"pbxID\":1,\"Option\":\"\"}");
		csock.HandleCommand("{\"Command\":\"callistend\",\"callid\":\"1000-08012341234\",\"pbxID\":1,\"Option\":\"\"}");
*/
		
		if( AppProperties.instance.serverIP == null || AppProperties.instance.serverIP == "") {
			LogWriter.instance.appendInfo("AES Server Name is not configured");
			return 0;
		}
		
		if(AppProperties.instance.isOutgoingSimulation == true)
			LogWriter.instance.appendInfo("outgoing_simulation is on");

		try {
			//WebClient.GetInstance().Connect(AppProperties.instance.webserver);
			int port = AppProperties.instance.listenPort;
			int ssl_port = AppProperties.instance.listenPortSSL;
						
			SimpleJettyServer jettyserver = null;
			SecureJettyServer jettysecure = null;
			
			SocketHandler t = null;
			if( port > 0 ) {
				LogWriter.instance.appendInfo("Waiting for TCP/IP server connection at port: " + port);
			        
				ServerSocket serverSocket = new ServerSocket(port);
				t = new SocketHandler(serverSocket);

				t.start();
			}
			
			SocketHandler sslt = null;
			if( ssl_port > 0 ) {
				LogWriter.instance.appendInfo("Initializing TCP/IP server connection at ssl_port: " + ssl_port);

		        Resource kpr = Resource.newClassPathResource("/keystore.jks");
		        //FileInputStream keyFile = new FileInputStream("./keystore.jks"); 
		        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
		        keyStore.load(kpr.getInputStream(), "changeit".toCharArray());

		        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		        keyManagerFactory.init(keyStore, "changeit".toCharArray());
		        // init KeyManager
		        KeyManager keyManagers[] = keyManagerFactory.getKeyManagers();
		        // init the SSL context
		        SSLContext sslContext = SSLContext.getInstance("SSL");
		        sslContext.init(keyManagers, null, new SecureRandom());
		        // get the socket factory
		        SSLServerSocketFactory sslssf = sslContext.getServerSocketFactory();
		        				
		        SSLServerSocket sslServerSocket =
		            (SSLServerSocket) sslssf.createServerSocket(ssl_port);
		        
		        //sslServerSocket.setEnabledCipherSuites([]);
		        String[] suites = sslServerSocket.getSupportedCipherSuites();
		        sslServerSocket.setEnabledCipherSuites(suites);			        
		        sslServerSocket.setEnabledProtocols(sslServerSocket.getSupportedProtocols());
			        
				sslt = new SocketHandler(sslServerSocket);

				sslt.start();

				LogWriter.instance.appendInfo("Waiting for TCP/IP server connection at ssl_port: " + ssl_port);
			}


			if( AppProperties.instance.listenPortWS != 0 ) {
				LogWriter.instance.appendInfo("Waiting for localhost client connection at port: " + AppProperties.instance.listenPortWS);
				jettyserver = new SimpleJettyServer(AppProperties.instance.listenPortWS);
			}

			if( AppProperties.instance.listenPortWSS != 0 ) {
				LogWriter.instance.appendInfo("Waiting for ENCRYPTED localhost client connection at port: " + AppProperties.instance.listenPortWSS);
				jettysecure = new SecureJettyServer("localhost", AppProperties.instance.listenPortWSS);
			}
			
			if( AppProperties.instance.serverIP != null )
			
	        new Thread("AES Connect") {		            	
				public void run() {
					// initialize PBX
					Pbx pbx = configurePBX(1);

					if(AppProperties.instance.isOutgoingSimulation == false)
						pbx.Login();
				}
	        }.start();
			
			while (!shutdown || isConsole) {			
				//LogWriter.instance.appendInfo("shutdown: " + shutdown);
				
				Thread.sleep(1000);
			}
			
			WebClient.isShutdown = true;	//Prevents AES connect from retrying
	
			if(t != null) {
				t.close();	//will close serverSocket too
			}
			
			if(sslt != null) {
				sslt.close();
			}
			
			if(jettyserver != null ) {
				jettyserver.stop();
			}

			if(jettysecure != null ) {
				jettysecure.stop();
			}

			PhoneManager.isRunning = false;
			LogWriter.instance.appendInfo("Terminated - Main loop");
		} catch (Exception e) {
			LogWriter.instance.appendException(e);
		}
		
		PbxPool.GetInstance().RemovePbx();
		PhoneManager.isRunning = false;
		LogWriter.instance.appendInfo("PhoneManager.isRunning = false");
		return 0;
	}

	public Pbx configurePBX(int pbxNum) {
		PbxInfo pbxInfo = new PbxInfo(
				AppProperties.instance.serverIP,
				AppProperties.instance.portNumber,
				AppProperties.instance.login,
				AppProperties.instance.password,
				AppProperties.instance.serviceName,
				AppProperties.instance.stasisApp,
				1);
		
		if (pbxNum == 2) {
		    pbxInfo = new PbxInfo(
				AppProperties.instance.serverIP,
				AppProperties.instance.portNumber,
				AppProperties.instance.login,
				AppProperties.instance.password,
				AppProperties.instance.serviceName,
				AppProperties.instance.stasisApp,
				2);
		}
		
		Pbx pbx = null;
		
		if(AppProperties.instance.pbxtype.equals("mpbx")) {
			if (AppProperties.instance.stasisApp != null) {
				pbxInfo.StasisApp = AppProperties.instance.stasisApp;
				if (pbxNum == 2) {
					pbxInfo.StasisApp = AppProperties.instance.stasisApp + "2";
				}
			}
			pbx = new astPbx(pbxInfo);
		}
		else if(AppProperties.instance.isSimulation) {
			pbx = new SimPbx(pbxInfo);
		} else {
			pbx = new AvayaPbx(pbxInfo);
		}
		
		PbxPool.GetInstance().clearPbxList();
		PbxPool.GetInstance().AddPbx(pbx);
		
		return pbx;
	}
}
