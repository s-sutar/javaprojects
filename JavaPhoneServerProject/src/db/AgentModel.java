package db;

import pbx.CallOriginator;

public class AgentModel {

	public String extension;
	public String id;
	public String name;
	
	public AgentModel(CallOriginator originator) {		
		if(originator != null) {
			this.extension = originator.FromNumber;
			this.id = originator.AgentID;
			this.name = originator.Name;
		}
	}
	
}
