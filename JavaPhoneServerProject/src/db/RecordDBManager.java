package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import core.AppProperties;
import core.LogWriter;

public class RecordDBManager {
	
	private static RecordDBManager _instance;
	
	public static RecordDBManager GetInstance() {
		if(_instance == null) {
			synchronized(DBManager.class) {
				if(_instance == null) {
					_instance = new RecordDBManager();
				}
			}
		}
		
		return _instance;
	}
	
	private RecordDBManager() {
	}
	
	private Connection getConnection() {
		if(AppProperties.instance.recording_enabled == false) {
			return null;
		}
		
		return RecordDBManager.GetInstance().connectDB(
				AppProperties.instance.record_db_address,
				AppProperties.instance.record_db_name, 
				AppProperties.instance.record_db_username, 
				AppProperties.instance.record_db_password);
	}
	
	private Connection connectDB(String address, String dbName, String username, String password) {
		if (address == null || dbName == null) {
			return null;
		}

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			String finalAddress = address.replace(',', ':');
			String connStr = String.format("jdbc:sqlserver://%s;databaseName=%s", finalAddress, dbName);
			Connection connection = DriverManager.getConnection(connStr, username, password);

			return connection;
		} catch (Exception e) {
			LogWriter.instance.appendException(e);
		}

		return null;
	}
	
	public void updateRecordTable(String extension, String agentID, int isUseSoftphone) {
		if(AppProperties.instance.recording_enabled == false
				|| extension == null || extension.isEmpty()) {
        	return;
        }
		
        new Thread() {
            public void run() {            	
        		Connection connection = getConnection();
        		
        		String agentIDValue = (agentID == null || agentID.isEmpty()) ? "NULL" : "'" + agentID + "'";
        		
        		String query = String.format(
                        "UPDATE %s WITH (ROWLOCK) SET agentid=%s, UseSoftphone=%d WHERE extno = '%s';",
                        AppProperties.instance.record_table, agentIDValue, isUseSoftphone, extension);
        		
        		try {
					if(connection == null || connection.isClosed()) {
						// failed to connect
						LogWriter.instance.appendWarning("Failed to update: query=" + query);
						
						return;
					}
					
					execute(connection, query);
					
					connection.close();
				} catch (SQLException e) {
					LogWriter.instance.appendException(e);
				}
		    }
		}.start();
	}
	
	private boolean execute(Connection connection, String query) {
		if(query == null || query.isEmpty()) {
			return false;
		}
		
		boolean isSuccess = false;

    	try {
    		Statement stmt = connection.createStatement();
    		int i = stmt.executeUpdate(query);
    		isSuccess = (i > 0) ? true : false;
    	} catch(Exception e) {
    		LogWriter.instance.appendException(e);
    	}
        
        LogWriter.instance.appendQuery(((isSuccess) ? "" : "FAILED ") + query);
      
        return isSuccess;
	}

}
