package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import core.AppProperties;
import core.LogWriter;

public class DBManager {
	
	private static DBManager _instance;
	private Connection connection = null;
	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	public boolean isConnected = false;
	
	public static DBManager GetInstance() {
		if(_instance == null) {
			synchronized(DBManager.class) {
				if(_instance == null) {
					_instance = new DBManager();
				}
			}
		}
		
		return _instance;
	}
	
	private DBManager() {
	}
	
	public void initialize() {
		if(AppProperties.instance.db_name != null && AppProperties.instance.db_name.isEmpty() == false) {
			DBManager.GetInstance().connectDB(
					AppProperties.instance.db_address,
					AppProperties.instance.db_name, 
					AppProperties.instance.db_username, 
					AppProperties.instance.db_password);
		}
	}
	
	private boolean connectDB(String address, String dbName, String username, String password) {
		if (address == null || dbName == null) {
			return false;
		}

		try {			
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			String finalAddress = address.replace(',', ':');
			String connStr = String.format("jdbc:sqlserver://%s;databaseName=%s", finalAddress, dbName);
			this.connection = DriverManager.getConnection(connStr, username, password);

			if (this.connection != null && this.connection.isClosed() == false) {
				isConnected = true;
				return true;
			}
		} catch (Exception e) {
			LogWriter.instance.appendException(e);
		}

		return false;
	}	
	
	public boolean close() {
		if(this.connection == null) {
			return false;
		}
		
		try {			
			if(this.connection.isClosed() == false) {
				this.connection.close();
			}
			
			return true;
		} catch (Exception e) {
			LogWriter.instance.appendException(e);
		}
		
		return false;
	}
	
	public void insertLogCall(String unique_id, String callFrom, String callTo, String startTime, String endTime,
			int isOutbound, int dialerId, int mpbxId, int dbId, int lastAcdId, int lastOpId, int groupId, int jobId, int callListId,
			int predId, int isScheduledCall, int connection_result) {
		if(this.isConnected == false || this.connection == null) {
        	return;
        }
        
        new Thread() {
            public void run() {  
            	String machineName = "";
				String tableName = AppProperties.instance.db_name + ".dbo.t4e_log_call";
		        String values = String.format("'%s','%s','%s','%s','',%d,%d,%d,%d,%d,%d,%d,%d,%d",
		        		unique_id, callFrom, callTo, startTime, endTime, isOutbound, dialerId, mpbxId, dbId, lastAcdId, lastOpId, groupId, jobId, callListId,
		        		predId, isScheduledCall, connection_result, machineName);
		        String query = String.format("INSERT INTO %s (%s) VALUES(%s)",
		        		tableName,
		        		"unique_id, call_from, call_to, start_time, end_time, is_outbound, dialer_id, mpbx_id, db_id, last_acd_id, last_op_id, group_id, job_id, calllist_id,"
		        		+ " pred_id, is_scheduled_call, connection_result, created_by",
		        		values);
		        
		        execute(query);
            }
        }.start();
	}
	
	public void insertLogOp(String agentID, String agentName, int jobID, String channel, int status, int siteID) {
		if(this.isConnected == false || this.connection == null
				|| agentID == null || agentID.isEmpty()) {
        	return;
        }
		
		String agentNameTemp = (agentName == null) ? "" : agentName;

        new Thread() {
            public void run() {  
				String tableName = AppProperties.instance.db_name + ".dbo.T4E_OPStatus";			
				String now = dateFormat.format(new Date());	
				
				String query = String.format(
		                "IF EXISTS (SELECT 1 FROM %s(NOLOCK) WHERE UserID=%s)" +
		                "UPDATE %s SET OPStatus=%d, JOBID=%d, TimeStamp='%s',SiteID=%d, Extension='%s' WHERE UserID=%s;" +
		                "ELSE INSERT INTO %s(UserID,AgentName,JobID,Extension,OPStatus,TimeStamp,SiteID) VALUES(%s,'%s',%d,'%s',%d,'%s',%d);",
		                tableName, agentID,
		                tableName, status, jobID, now, siteID, channel, agentID,
		                tableName, agentID, agentNameTemp, jobID, channel, status, now, siteID);
		      
		        execute(query);
		    }
		}.start();
	}
	
	private boolean execute(String query) {
		if(query == null || query.isEmpty()) {
			return false;
		}
		
		boolean isSuccess = false;
		
        synchronized(this.connection) {
        	try {
        		Statement stmt = this.connection.createStatement();
        		int i = stmt.executeUpdate(query);
        		isSuccess = (i > 0) ? true : false;
        	} catch(Exception e) {
        		LogWriter.instance.appendException(e);
        	}
        }
        
        LogWriter.instance.appendQuery(((isSuccess) ? "" : "FAILED ") + query);
      
        return isSuccess;
	}
}
