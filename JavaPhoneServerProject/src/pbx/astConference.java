package pbx;

import java.util.ArrayList;

public class astConference {
	public String name;
	public boolean isRecording = false;
	public String liveRecordingName;
	public String filename;
	public String parentid;
	public String CUNumber;
	
	public ArrayList<String> participants = new ArrayList<String>();
}
