package pbx;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.telephony.Address;
import javax.telephony.Call;
import javax.telephony.CallEvent;
import javax.telephony.Connection;
import javax.telephony.ConnectionEvent;
import javax.telephony.MetaEvent;
import javax.telephony.Terminal;
import javax.telephony.TerminalConnection;
import javax.telephony.TerminalConnectionEvent;
import javax.telephony.callcenter.CallCenterAddress;
import javax.telephony.callcontrol.CallControlCallEvent;
import javax.telephony.callcontrol.CallControlConnectionEvent;
import javax.telephony.callcontrol.CallControlTerminalConnectionEvent;
import javax.telephony.callcontrol.CallControlTerminalConnectionListener;

import com.avaya.jtapi.tsapi.LucentV6Agent;
import com.avaya.jtapi.tsapi.LucentV7ACDManagerAddress;
import com.avaya.jtapi.tsapi.LucentV7CallInfo;
import com.avaya.jtapi.tsapi.impl.events.conn.LucentV5CallControlConnectionEvent;

import acd.ACDManager;
import core.EventsReceived;
import core.LogWriter;
import db.DBManager;

public class AvayaCallControlTerminalConnectionListener implements CallControlTerminalConnectionListener {

	private String currentVDN = "";
	
	/*private static final Map<Integer, Integer> agentStatusMap;
    static {
        Map<Integer, Integer> aMap = new HashMap<>();
        aMap.put(7, 4);
        aMap.put(3, 3);
        aMap.put(4, 2);
        aMap.put(5, 5);
        aMap.put(6, 5);
        agentStatusMap = Collections.unmodifiableMap(aMap);
    }*/
	
	private String getTerminalName(TerminalConnectionEvent event) {
		return "TerminalConnection to Terminal " + getTerminalNumber(event) + " is ";
	}

	private String getTerminalNumber(TerminalConnectionEvent event) {
		String name = null;

		try {
			TerminalConnection termConn = event.getTerminalConnection();
			Terminal term = termConn.getTerminal();
			name = term.getName();
		} catch (Exception e) {
			LogWriter.instance.appendException(e);
		}

		return name;
	}

	private String getCallingNumber(TerminalConnectionEvent event) {
		String callingNumber = "null";

		try {
			int id = event.getID();
			int cause = event.getCause();

			Call call = event.getCall();
			Connection[] conns = (call != null) ? call.getConnections() : null;

			int callState = call.getState();
			String additional = "";

			if (conns != null && conns.length >= 1 && conns[0] != null ) {
				callingNumber = conns[0].getAddress().getName();

				int callingConnState = conns[0].getState();

				additional = String.format(", callingConnState=%d", callingConnState);
			}

			LogWriter.instance.appendInfo(String.format("id=%d, cause=%d, calling=%s, called=%s, callState=%d %s", id,
					cause, callingNumber, "nocallednumber", callState, additional));
		} catch (Exception e) {
			LogWriter.instance.appendException(e);
		}

		return callingNumber;
	}

	@Override
	public void connectionAlerting(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection CallControlConnectionEvent connectionAlerting");
		
		try
        {
            Call call = arg0.getCall();

            if (AvayaPbx.channelInfo[0].isMineEvt(call)) 
            {
                LogWriter.instance.appendInfo("connectionAlertingEvt: call[0]-----------RING");
            }

        }
        catch (Exception e)
        { 
        	LogWriter.instance.appendInfo("connectionAlertingEvt Error: " + e.toString());
        }
		
		String calledNumber = arg0.getCalledAddress() == null ? "" 
				            : arg0.getCalledAddress().getName();
		String callingNumber = arg0.getCallingAddress()== null ? ""
				             : arg0.getCallingAddress().getName();
		LogWriter.instance.appendInfo(callingNumber + " Calling Terminal AlertingTest EXT/INT");
		EventsReceived evt = new EventsReceived("vdn_info", calledNumber);
		evt.Exten = callingNumber;
		//evt.Message = vdn;
		String msg = evt.ToJson();
		LogWriter.instance.appendInfo(msg);
		currentVDN = calledNumber;
	}

	@Override
	public void connectionDialing(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection connectionDialing");
	}

	@Override
	public void connectionDisconnected(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection connectionDisconnected");
		
		LucentV5CallControlConnectionEvent tempevt = (LucentV5CallControlConnectionEvent) arg0;
        int reasonCode = tempevt.getCSTACause();
        LogWriter.instance.appendInfo("connectionEstablishedEvt: ReasonCode = " + reasonCode);
		
		try
        {
            int connects = 0;

            if (arg0.getCall().getConnections() != null)
                connects = arg0.getCall().getConnections().length;

            if (connects != 0)
                return;

            Call call = arg0.getCall();

            if (AvayaPbx.channelInfo[0].isMineEvt(call))
            {
            	LogWriter.instance.appendInfo("connectionDisconnectedEvt: clear channel 0, callid : " + AvayaPbx.channelInfo[0].getCallID());
            	AvayaPbx.channelInfo[0].ClearCall();
                if (!AvayaPbx.channelInfo[1].isActive())
                {
                    LogWriter.instance.appendInfo("connectionDisconnectedEvt: -----------ONHOOK[0]");
                }
                return;
            }

            if (AvayaPbx.channelInfo[1].isMineEvt(call))
            {
            	LogWriter.instance.appendInfo("connectionDisconnectedEvt: clear channel 1, callid : " + AvayaPbx.channelInfo[1].getCallID());
            	AvayaPbx.channelInfo[1].ClearCall();
                if (!AvayaPbx.channelInfo[0].isActive())
                {
                    LogWriter.instance.appendInfo("connectionDisconnectedEvt: -----------ONHOOK[1]");
                }
                return;
            }
        }
        catch (Exception e)
        { 
        	LogWriter.instance.appendInfo("connectionDisconnectedEvt Error: "+e.toString());
        }
	}

	@Override
	public void connectionEstablished(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection connectionEstablished");
		
		try
        {
			LucentV5CallControlConnectionEvent tempevt = (LucentV5CallControlConnectionEvent) arg0;
            int reasonCode = tempevt.getCSTACause();
            LogWriter.instance.appendInfo("connectionEstablishedEvt: ReasonCode = " + reasonCode);
            
            if (reasonCode == 31){
            	LogWriter.instance.appendInfo("connectionEstablishedEvt: monitor event ignored");
            	return;
            }
			
			int i;
            Call call = arg0.getCall();
            
            for (i = 0; i < 2; i++)
            {
                if (AvayaPbx.channelInfo[i].isMineEvt(call))
                {
                    int estcount = AvayaPbx.channelInfo[i].GetEstCountUpValue();
                    if (estcount == AvayaPbx.ESTCOUNT_TALKING)
                    {
                    	AvayaPbx.channelInfo[i].setisTalking(true);
                        if(i==0)
                            LogWriter.instance.appendInfo("connectionEstablishedEvt: call[" + i + "]-----------Talking");

                    }

                    if (i == 1 && estcount == AvayaPbx.ESTCOUNT_TRANSFERCOMPLETE)//]ณ]ฎนใ(callidgpj
                    {
                    	LogWriter.instance.appendInfo("connectionEstablishedEvt: clear channel 1, callid : " + AvayaPbx.channelInfo[1].getCallID());
                    	AvayaPbx.channelInfo[1].ClearCall();
                    	LogWriter.instance.appendInfo("connectionEstablishedEvt: -----------HOOK");
                    }

                    if (i == 0 && estcount == AvayaPbx.ESTCOUNT_TRANSFERCOMPLETE) // ]ๆล]ฎน(callidgp)
                    {
                    	LogWriter.instance.appendInfo("connectionEstablishedEvt: ------------------ring,talking");
                    }

                    return; 

                }
            }

        }
        catch (Exception e)
        {
        	LogWriter.instance.appendInfo("connectionEstablishedEvt Error: " + e.toString());
        }
	}

	@Override
	public void connectionFailed(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection connectionFailed");
	}

	@Override
	public void connectionInitiated(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection connectionInitiated");
	}

	@Override
	public void connectionNetworkAlerting(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection connectionNetworkAlerting");
	}

	@Override
	public void connectionNetworkReached(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection connectionNetworkReached");
	}

	@Override
	public void connectionOffered(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection connectionOffered");
	}

	@Override
	public void connectionQueued(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection connectionQueued");
	}

	@Override
	public void connectionUnknown(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection connectionUnknown");
	}

	@Override
	public void callActive(CallEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection callActive");
		
		
		try
        {
            Call call = arg0.getCall();
            
            for (int i = 0; i < 2; i++)
            {
            	// remove if invalid
            	if (AvayaPbx.channelInfo[i].getLucentV7CallInfo() != null)
            	{
            		Call prevcall = (Call) AvayaPbx.channelInfo[i].getLucentV7CallInfo();
            		if (prevcall.getState() == Call.INVALID)
            		{
            			LogWriter.instance.appendInfo("Call invalid at Channel " + i + " with id: " + AvayaPbx.channelInfo[i].getCallID() + " got cleared" );
            			AvayaPbx.channelInfo[i].ClearCall();
            		}
            		else
            		{
            			LogWriter.instance.appendInfo("Call active at Channel " + i + " with id: " + AvayaPbx.channelInfo[i].getCallID());
            		}
            		
            	}
            	
                if (AvayaPbx.channelInfo[i].RegisterCall(call))
                {
                	LogWriter.instance.appendInfo("Registered. register at Channel " + i + " by " + AvayaPbx.channelInfo[i].getCallID() );
                    break;
                }
                else
                {
                	LogWriter.instance.appendInfo("Occupied. cannot register at Channel " + i + " by " + AvayaPbx.channelInfo[i].getCallID() );
                }
            }
        }
        catch (Exception e)
        {
            LogWriter.instance.appendInfo("CallActiveEvt Error: " + e.toString());
        } 
	}

	@Override
	public void callEventTransmissionEnded(CallEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection callEventTransmissionEnded");
	}

	@Override
	public void callInvalid(CallEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection callInvalid");
	}

	@Override
	public void multiCallMetaMergeEnded(MetaEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection multiCallMetaMergeEnded");
	}

	@Override
	public void multiCallMetaMergeStarted(MetaEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection multiCallMetaMergeStarted");
	}

	@Override
	public void multiCallMetaTransferEnded(MetaEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection multiCallMetaTransferEnded");
	}

	@Override
	public void multiCallMetaTransferStarted(MetaEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection multiCallMetaTransferStarted");
	}

	@Override
	public void singleCallMetaProgressEnded(MetaEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection singleCallMetaProgressEnded");
	}

	@Override
	public void singleCallMetaProgressStarted(MetaEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection singleCallMetaProgressStarted");
	}

	@Override
	public void singleCallMetaSnapshotEnded(MetaEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection singleCallMetaSnapshotEnded");
	}

	@Override
	public void singleCallMetaSnapshotStarted(MetaEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection singleCallMetaSnapshotStarted");
	}

	@Override
	public void connectionAlerting(ConnectionEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection ConnectionEvent connectionAlerting");

	}

	@Override
	public void connectionConnected(ConnectionEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection connectionConnected");
	}

	@Override
	public void connectionCreated(ConnectionEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection connectionCreated");
	}

	@Override
	public void connectionDisconnected(ConnectionEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection connectionDisconnected");
	}

	@Override
	public void connectionFailed(ConnectionEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection connectionFailed");
	}

	@Override
	public void connectionInProgress(ConnectionEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection connectionInProgress");
	}

	@Override
	public void connectionUnknown(ConnectionEvent arg0) {
		LogWriter.instance.appendInfo("TerminalConnection connectionUnknown");
	}

	@Override
	public void terminalConnectionActive(TerminalConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getTerminalName(arg0) + "ACTIVE");
	}

	@Override
	public void terminalConnectionCreated(TerminalConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getTerminalName(arg0) + "CREATED");
	}

	@Override
	public void terminalConnectionDropped(TerminalConnectionEvent arg0) {
		String calledNumber = getTerminalNumber(arg0);
		String callingNumber = this.getCallingNumber(arg0);
		LogWriter.instance.appendInfo(getTerminalName(arg0) + "DROPPED from " + callingNumber);
		LogWriter.instance.appendInfo(getTerminalName(arg0) + "DROPPED at " + calledNumber);
		
		LucentV6Agent agent = (LucentV6Agent) ACDManager.GetInstance().GetAgent(calledNumber);
		if (agent == null)
			agent = (LucentV6Agent) ACDManager.GetInstance().GetAgent(callingNumber);
		
		int agentstatusCurrent = 999;
		if (agent != null){
		  int workstatusCurrent = agent.getState();
		  switch (workstatusCurrent){
		    case 7:
		    	agentstatusCurrent = 4;
		    	break;
		    case 3:
		    	agentstatusCurrent = 3;
		    	break;
		    case 4:
		    	agentstatusCurrent = 2;
		    	break;
		    case 6:
		    	agentstatusCurrent = 5;
		    	break;
		    case 5:
		    	agentstatusCurrent = 5;
		    	break;
		    default:
		    	agentstatusCurrent = 999;
		  }
		  //agentstatusCurrent = agentStatusMap.get(agent.getState());
		  //the getState returns the work state which is 4 when it is ready
		  //if (agentstatusCurrent == 4)
		  //  agentstatusCurrent = 2;
		}
		// {"Channel":"610001","EventName":"Hangup","Exten":"fus00008043497192","Message":"success","Response":"call's
		// hung up , cause = 16, finalCause=0","Timestamp":"12:03:10.1846"}
		EventsReceived evt = new EventsReceived("Hangup", calledNumber);
		evt.Exten = callingNumber;
		evt.Response = "call's hung up , cause = 16, finalCause = 0 ";
		evt.Message = "success";
		evt.ChannelState = String.valueOf(agentstatusCurrent); 
		String msg = evt.ToJson();
		network.SocketHandler.notifyAllDailers(msg);
		//WebClient.GetInstance().SendMessage(msg);
	}

	@Override
	public void terminalConnectionPassive(TerminalConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getTerminalName(arg0) + "PASSIVE");
	}

	@Override
	public void terminalConnectionRinging(TerminalConnectionEvent arg0) {
		String calledNumber = getTerminalNumber(arg0);
		String callingNumber = this.getCallingNumber(arg0);
		LogWriter.instance.appendInfo(getTerminalName(arg0) + "RINGING from " + callingNumber);
		
		String vdn = currentVDN;
			
		EventsReceived evt = new EventsReceived("incoming_call", calledNumber);
		evt.Exten = callingNumber;
		evt.Message = vdn;
		String msg = evt.ToJson();
		LogWriter.instance.appendInfo(msg);
		network.SocketHandler.notifyAllDailers(msg);
		//WebClient.GetInstance().SendMessage(msg);
		
		currentVDN = "";

		String unique_id = "";
		String startTime = "";
		int lastAcdId = 0;
		int lastOpId = 0;
		int groupId = 0;
		int jobId = 0;
		int callListId = 0;
		int predId = 0;
		int isScheduledCall = 0;
		int connection_result = 0;
		DBManager.GetInstance().insertLogCall(unique_id, callingNumber, calledNumber, startTime, null, 0, 0, 0, 0,
				lastAcdId, lastOpId, groupId, jobId, callListId, predId, isScheduledCall, connection_result);
	}

	@Override
	public void terminalConnectionUnknown(TerminalConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getTerminalName(arg0) + "UNKNOWN");

	}

	@Override
	public void terminalConnectionBridged(CallControlTerminalConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getTerminalName(arg0) + "BRIDGED");
	}

	@Override
	public void terminalConnectionDropped(CallControlTerminalConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getTerminalName(arg0) + "DROPPED 2");
	}

	@Override
	public void terminalConnectionHeld(CallControlTerminalConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getTerminalName(arg0) + "HELD");
	}

	@Override
	public void terminalConnectionInUse(CallControlTerminalConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getTerminalName(arg0) + "IN USE");
	}

	@Override
	public void terminalConnectionRinging(CallControlTerminalConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getTerminalName(arg0) + "CallControlTerminalConnectionEvent RINGING");
	}

	@Override
	public void terminalConnectionTalking(CallControlTerminalConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getTerminalName(arg0) + "TALKING");
		
		String calledNumber = getTerminalNumber(arg0);
		String callingNumber = this.getCallingNumber(arg0);
		
		EventsReceived evt = new EventsReceived("Talking", calledNumber);
		evt.Exten = callingNumber;
		evt.Response = "Success";
		evt.Message = callingNumber;
		String msg = evt.ToJson();
		LogWriter.instance.appendInfo(msg);
		network.SocketHandler.notifyAllDailers(msg);
		
	}

	@Override
	public void terminalConnectionUnknown(CallControlTerminalConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getTerminalName(arg0) + "UNKNOWN 2");
	}

}
