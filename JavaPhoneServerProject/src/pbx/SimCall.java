package pbx;

import java.util.Date;


public class SimCall {
	public long startAlertTime;
	public long ansTime;
	public long endTime;
	public boolean alerted;
	public boolean answered;
	public AvayaCallControlConnectionListener listener;
	
	public SimCall(AvayaCallControlConnectionListener listen, int ringtime, int talktime) {
		this.listener = listen;
		
		int delay = 2;		
				
		Date now = new Date();
		
		if(ringtime > 0) {
			this.startAlertTime = now.getTime() + delay * 1000;
			delay += ringtime;
		}
		
		if( talktime > 0 ) {
			this.ansTime = now.getTime() + delay * 1000;
			delay += talktime;
		}
		
		this.endTime = now.getTime() + delay * 1000;	
	}

}
