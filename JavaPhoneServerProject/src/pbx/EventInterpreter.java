package pbx;

import javax.telephony.Call;
import javax.telephony.Connection;
import javax.telephony.Event;
import javax.telephony.TerminalConnection;
import javax.telephony.callcontrol.CallControlEvent;

import com.avaya.jtapi.tsapi.LucentEventCause;

import core.LogWriter;

public class EventInterpreter {
	
	public static int toIsupCauseCode(int lucentEventCauseCode) {
		switch(lucentEventCauseCode) {
			case LucentEventCause.EC_BUSY : //3
				return 17; //user busy
				
			case LucentEventCause.EC_CALL_CANCELLED : //5
				return 21; //call reject
				
			case LucentEventCause.EC_DEST_NOT_OBTAINABLE : //13
				return 31; //normal specified
				
			case LucentEventCause.EC_REORDER_TONE : //29
				return 22; //number changed
				
			case LucentEventCause.EC_ALERT_TIME_EXPIRED : //60
				return 18; //no user responding
				
			case LucentEventCause.EC_INCOMPATIBLE_DESTINATION : //15
				return -1; //??
				
			default:
				return 0; // undefined
		}
 	}
	
	public static String toIsupCauseText(int isupCode) {
		switch(isupCode) {
			case 17 :
				return "user busy";
				
			case 18 :
				return "no user responding";
				
			case 21 :
				return "call reject";
				
			case 22 :
				return "number changed";
				
			case 28 :
				return "number changed";
				
			case 31 :
				return "normal specified";			
				
			default:
				return "undefined";
		}
	}
	
	public static String toLucentEventCauseText(int isupCauseCode) {
		switch(isupCauseCode) {
			case LucentEventCause.EC_BUSY :
				return "EC_BUSY";
				
			case LucentEventCause.EC_NEW_CALL :
				return "EC_NEW_CALL";				
				
			case LucentEventCause.EC_CALL_CANCELLED :
				return "EC_CALL_CANCELLED";
				
			case LucentEventCause.EC_DEST_NOT_OBTAINABLE :
				return "EC_DEST_NOT_OBTAINABLE";
				
			case LucentEventCause.EC_REORDER_TONE :
				return "EC_REORDER_TONE";
				
			case LucentEventCause.EC_ALERT_TIME_EXPIRED :
				return "EC_ALERT_TIME_EXPIRED";				
				
			case LucentEventCause.EC_INCOMPATIBLE_DESTINATION :
				return "EC_INCOMPATIBLE_DESTINATION";
				
			case LucentEventCause.EC_REDIRECTED :
				return "EC_REDIRECTED";
				
			case LucentEventCause.EC_CALL_NOT_ANSWERED :
				return "EC_CALL_NOT_ANSWERED";
				
			case LucentEventCause.EC_UNASSIGNED_NUMBER :
				return "EC_UNASSIGNED_NUMBER";	
				
			default:
				return "undefined";
		}
	}	
	
	public static String getMethodName(int depth)
	{
		/*		
		0 is getStackTrace(),
		1 is getMethodName(int depth) and
		2 is invoking method.
		*/
		String result = "null";
		try {
			StackTraceElement[] ste = Thread.currentThread().getStackTrace();
			result = ste[depth].getMethodName();
		} catch(Exception e) {
			//System.err.println(ex.getMessage());
			LogWriter.instance.appendException(e);
		}
	  
	  return result; 
	}

	public static String parseCallControlEventCause(int code) {
/*		CAUSE_ALTERNATE 202 
		CAUSE_BUSY 203 
		CAUSE_CALL_BACK 204 
		CAUSE_CALL_NOT_ANSWERED 205 
		CAUSE_CALL_PICKUP 206 
		CAUSE_CONFERENCE 207 
		CAUSE_DO_NOT_DISTURB 208 
		CAUSE_PARK 209 
		CAUSE_REDIRECTED 210 
		CAUSE_REORDER_TONE 211 
		CAUSE_TRANSFER 212 
		CAUSE_TRUNKS_BUSY 213 
		CAUSE_UNHOLD 214 */
		switch(code) {
			case CallControlEvent.CAUSE_ALTERNATE: 
				return "CAUSE_CALL_CANCELLED";
				
			case CallControlEvent.CAUSE_BUSY: 
				return "CAUSE_BUSY";
				
			case CallControlEvent.CAUSE_CALL_BACK: 
				return "CAUSE_CALL_BACK";
				
			case CallControlEvent.CAUSE_CALL_NOT_ANSWERED: 
				return "CAUSE_CALL_NOT_ANSWERED";
				
			case CallControlEvent.CAUSE_CALL_PICKUP: 
				return "CAUSE_BUSY";
				
			case CallControlEvent.CAUSE_CONFERENCE: 
				return "CAUSE_CONFERENCE";
				
			case CallControlEvent.CAUSE_DO_NOT_DISTURB: 
				return "CAUSE_DO_NOT_DISTURB";
				
			case CallControlEvent.CAUSE_PARK: 
				return "CAUSE_PARK";
				
			case CallControlEvent.CAUSE_REDIRECTED: 
				return "CAUSE_REDIRECTED";
				
			case CallControlEvent.CAUSE_REORDER_TONE: 
				return "CAUSE_REORDER_TONE";
				
			case CallControlEvent.CAUSE_TRANSFER: 
				return "CAUSE_TRANSFER";
				
			case CallControlEvent.CAUSE_TRUNKS_BUSY: 
				return "CAUSE_TRUNKS_BUSY";
				
			case CallControlEvent.CAUSE_UNHOLD: 
				return "CAUSE_UNHOLD";	
				
			default:
				return "null";
		}//end switch
	}
	
	public static String parseCause(int code) {
		/*
		CAUSE_CALL_CANCELLED	102
		CAUSE_DEST_NOT_OBTAINABLE	103
		CAUSE_INCOMPATIBLE_DESTINATION	104
		CAUSE_LOCKOUT	105
		CAUSE_NETWORK_CONGESTION	108
		CAUSE_NETWORK_NOT_OBTAINABLE	109
		CAUSE_NEW_CALL	106
		CAUSE_NORMAL	100
		CAUSE_RESOURCES_NOT_AVAILABLE	107
		CAUSE_SNAPSHOT	110
		CAUSE_UNKNOWN	101
		*/
		switch(code) {
			case Event.CAUSE_CALL_CANCELLED: 
				return "CAUSE_CALL_CANCELLED";
				
			case Event.CAUSE_DEST_NOT_OBTAINABLE: 
				return "CAUSE_DEST_NOT_OBTAINABLE";
				
			case Event.CAUSE_INCOMPATIBLE_DESTINATION: 
				return "CAUSE_INCOMPATIBLE_DESTINATION";
				
			case Event.CAUSE_LOCKOUT: 
				return "CAUSE_LOCKOUT";

			case Event.CAUSE_NETWORK_CONGESTION: 
				return "CAUSE_NETWORK_CONGESTION";
				
			case Event.CAUSE_NETWORK_NOT_OBTAINABLE: 
				return "CAUSE_NETWORK_NOT_OBTAINABLE";
				
			case Event.CAUSE_NEW_CALL: 
				return "CAUSE_NEW_CALL";
				
			case Event.CAUSE_NORMAL: 
				return "CAUSE_NORMAL";
				
			case Event.CAUSE_RESOURCES_NOT_AVAILABLE: 
				return "CAUSE_RESOURCES_NOT_AVAILABLE";
				
			case Event.CAUSE_SNAPSHOT: 
				return "CAUSE_SNAPSHOT";
				
			case Event.CAUSE_UNKNOWN: 
				return "CAUSE_UNKNOWN";		
				
			default:
				return parseCallControlEventCause(code);
		}//end switch
	}//end function
	
	public static String parseCallState(int code) {
		/*
		ACTIVE	33
		IDLE	32
		INVALID	34
		*/
		switch(code) {
			case Call.ACTIVE:
				return "ACTIVE";
				
			case Call.IDLE: 
				return "IDLE";
				
			case Call.INVALID: 
				return "INVALID";
				
			default:
				return "null";
		}
	}
	
	public static String parseConnectionState(int code) {
		/*
		ALERTING		50
		CONNECTED		51
		DISCONNECTED	52
		FAILED			53
		IDLE			48
		INPROGRESS		49
		UNKNOWN			54
		*/
		switch(code) {
			case Connection.ALERTING:
				return "ALERTING";
				
			case Connection.CONNECTED:
				return "CONNECTED";
				
			case Connection.DISCONNECTED:
				return "DISCONNECTED";
				
			case Connection.FAILED:
				return "FAILED";
			
			case Connection.IDLE:
				return "IDLE";
				
			case Connection.INPROGRESS:
				return "INPROGRESS";
				
			case Connection.UNKNOWN:
				return "UNKNOWN";				
				
			default:
				return "null";
		}
	}

	public static String parseTerminalConnectionState(int code) {
		/*
		ALERTING		50
		CONNECTED		51
		DISCONNECTED	52
		FAILED			53
		IDLE			48
		INPROGRESS		49
		UNKNOWN			54
		*/
		/*	TERMINAL_CONNECTION_ACTIVE	115
		TERMINAL_CONNECTION_CREATED	116
		TERMINAL_CONNECTION_DROPPED	117
		TERMINAL_CONNECTION_PASSIVE	118
		TERMINAL_CONNECTION_RINGING	119
		TERMINAL_CONNECTION_UNKNOWN	120*/		
		switch(code) {
			case TerminalConnection.ACTIVE:
				return "ALERTING";
				
			case Connection.CONNECTED:
				return "CONNECTED";
				
			case Connection.DISCONNECTED:
				return "DISCONNECTED";
				
			case Connection.FAILED:
				return "FAILED";
			
			case Connection.IDLE:
				return "IDLE";
				
			case Connection.INPROGRESS:
				return "INPROGRESS";
				
			case Connection.UNKNOWN:
				return "UNKNOWN";				
				
			default:
				return "null";
		}
	}
}
