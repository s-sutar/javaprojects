package pbx;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;

import core.LogWriter;
import pbx.Pbx.PbxType;

public class PbxPool {
	
	private static PbxPool _instance = new PbxPool();
	private Dictionary<PbxType, ArrayList<Pbx>> pbxMapping = new Hashtable<PbxType, ArrayList<Pbx>>();
	
	private PbxPool() {
	}
	
	public static PbxPool GetInstance() {
		if(_instance == null) {
			synchronized(PbxPool.class) {
				if(_instance == null) {
					_instance = new PbxPool();
				}
			}//end synchronized
		}
		
		return _instance;
	}
	
	public Pbx GetPbx(PbxType type, int pbxid) {
		if(this.pbxMapping == null) {
			return null;
		}
		
		synchronized(this.pbxMapping) {
			ArrayList<Pbx> list = this.pbxMapping.get(type);
			
			if(list != null && list.size() > 0) {
				for( Pbx p : list) {
					if( p.pbxID == pbxid)
						return p;
				}
				
				//Didn't find that PBX, try to find an active one
				for(Pbx p : list) {
					if(p.pbxIsActive)
						return p;
				}
				
				return null;
			}
		}
		
		return null;
	}
	
	public void AddPbx(Pbx pbx) {
		if(pbx == null || pbx.Type == null) {
			return;
		}
		
		synchronized(this.pbxMapping) {			
			if(this.pbxMapping.isEmpty() || this.pbxMapping.get(pbx.Type) == null) {
				this.pbxMapping.put(pbx.Type, new ArrayList<Pbx>());
				
				LogWriter.instance.appendInfo("Created a new pbxMapping key: pbxType=" + pbx.Type);
			}
			
			ArrayList<Pbx> queue = this.pbxMapping.get(pbx.Type);
			
			if(queue != null) {
				queue.add(pbx);
				LogWriter.instance.appendInfo("Added pbxMapping: " + pbx.toString());
			}
		}//end synchronized
	}
	
	public void RemovePbx(Pbx pbx) {
		if(pbx == null || pbx.Type == null) {
			return;
		}
		
		synchronized(this.pbxMapping) {			
			if(this.pbxMapping.isEmpty() || this.pbxMapping.get(pbx.Type) == null) {
				return;
			}
			
			ArrayList<Pbx> queue = this.pbxMapping.get(pbx.Type);
			
			if(queue != null) {
				queue.remove(pbx);
				LogWriter.instance.appendInfo("Removed pbxMapping: " + pbx.toString());
			}
		}//end synchronized
	}	
	
	public void RemovePbx() {
		if(this.pbxMapping == null) {
			return;
		}
		
		synchronized(this.pbxMapping) {
	       Enumeration<PbxType> e = this.pbxMapping.keys();
	        while(e.hasMoreElements()) {
	        	PbxType key = e.nextElement();
	        	ArrayList<Pbx> list = this.pbxMapping.get(key);
				
				if(list != null) {
					for(Pbx pbx : list) {
						pbx.Logout();
					}
				}
	        }
	        
	        LogWriter.instance.appendInfo("Terminated - RemovePbx loop");
		}
		
		this.pbxMapping = null;
	}

	public void clearPbxList() {
		this.pbxMapping = new Hashtable<PbxType, ArrayList<Pbx>>();
	}
}
