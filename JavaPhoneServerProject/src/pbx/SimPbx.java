package pbx;

import java.util.LinkedList;

import javax.telephony.Address;
import javax.telephony.Terminal;

import core.EventsReceived;
import core.LogWriter;
import network.WebClient;
public class SimPbx extends Pbx {
	
	private LinkedList<String> monitoredChannelList = new LinkedList<>();

	public SimPbx(pbx.PbxInfo pbxInfo) {
		super(pbxInfo);
		this.Type = PbxType.SIMULATION;
	}

	public String finishInit() {
		return "";
	}
	
	public void removeCallObj(Object call) {
	}	
	
	@Override
	public boolean Login() {
		return true;
	}

	@Override
	public boolean Logout() {
		return true;
	}

	@Override
	public boolean Monitor(CallOriginator originator, CallReceiver receiver) {
		synchronized (monitoredChannelList) {
			monitoredChannelList.add(originator.FromNumber);				
		}
		
		new Thread("simpbx") {
            public void run() {
                while(true) {
                	try {
                		synchronized (monitoredChannelList) {
                			if(monitoredChannelList.contains(originator.FromNumber) == false) {                				
                				LogWriter.instance.appendInfo("Terminated incoming_call loop: ch=" + originator.FromNumber);
                				break;
                			}
                		}
                		
						sleep(10000);
						
						EventsReceived evt = new EventsReceived("incoming_call", originator.FromNumber);
						evt.Exten = "0366734940";
						String msg = evt.ToJson();
						WebClient.GetInstance().SendMessage(msg);
						
						sleep(20000);
						//{"EventName":"Hangup","Channel":"12058","Response":"null","Timestamp":"13:38:53.140","Exten":"null"}
						evt = new EventsReceived("Hangup", originator.FromNumber);
						msg = evt.ToJson();
						WebClient.GetInstance().SendMessage(msg);
					} catch (InterruptedException e) {
						LogWriter.instance.appendException(e);
					}
                }
                
                LogWriter.instance.appendInfo("Terminated - Simulation loop");
            }
        }.start();
		
		return true;
	}

	@Override
	public boolean Unmonitor(CallOriginator originator) {
		synchronized (monitoredChannelList) {
			if(monitoredChannelList.contains(originator.FromNumber)) {
				monitoredChannelList.remove(originator.FromNumber);
			}
		}
			
		return true;
	}

	@Override
	public boolean SetAgentReady(CallOriginator originator) {
		return true;
	}

	@Override
	public boolean SetAgentNotReady(CallOriginator originator) {
		return true;
	}

	@Override
	public boolean SetAgentPostwork(CallOriginator originator) {
		return true;
	}

	@Override
	public boolean Dial(CallOriginator originator, CallReceiver receiver) {
		return true;
	}
	
	@Override
	public boolean DialPredictive(CallOriginator originator, CallReceiver receiver) {
		return true;
	}

	@Override
	public boolean Drop(CallOriginator originator) {
		return true;
	}

	@Override
	public boolean Answer(CallOriginator originator) {
		return true;
	}

	@Override
	public boolean Hold(CallOriginator originator) {
		return true;
	}

	@Override
	public boolean Unhold(CallOriginator originator) {
		return true;
	}

	@Override
	public boolean TransferSetup(CallOriginator originator, CallReceiver receiver) {
		return true;
	}

	@Override
	public boolean TransferComplete(CallOriginator originator) {
		return true;
	}

	@Override
	public boolean TransferCancel(CallOriginator originator) {
		return true;
	}

	
	@Override
	public CallOriginator GetCallOriginator(String fromNumber, String option) {
		CallOriginator obj = null;

		Address address = null;
		Terminal terminal = null;
		
		obj = new AvayaCallOriginator(fromNumber, address, terminal, option);
		
		return obj;
	}

	@Override
	public CallReceiver GetCallReceiver(String toNumber) {		
		if(toNumber != null && toNumber.isEmpty() == false) {
			return new AvayaCallReceiver(toNumber, "", null, null, null, null);
		}
		
		return null;
	}
	
	public void closeConnection() {
		if (pbxWebClient != null)
		pbxWebClient.Close();
	}

}