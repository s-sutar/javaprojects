package pbx;

public abstract class CallOriginator {
	
	public String SystemCallID = null;
	public String FromNumber = null;
	public String Option = null;
	public String Name = null;
	public String AgentID = null;
	public String AgentName = null;
	public String AcdUsername = null;
	public String delayDial = null;
	public String delayedMonitorDial = null;
	public String Reason = "";
}
