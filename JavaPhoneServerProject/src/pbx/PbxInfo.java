package pbx;

public class PbxInfo {
	
	public String Url;
	public String Port;
	public String Username;
	public String Password;
	public String ServiceName;
	public String StasisApp;
	public String TrunkName;

	public int pbxID;
	
	public PbxInfo() {
	}
	
	public PbxInfo(String url, String port, String username, String password, String serviceName, String stasisApp, int pbxid) {
		this.Url = url;
		this.Port = port;
		this.Username = username;
		this.Password = password;
		this.ServiceName = serviceName;
		this.StasisApp = stasisApp;
		this.pbxID = pbxid;
	}
	
	public PbxInfo(PbxInfo src) {
		this.Url = src.Url;
		this.Port = src.Port;
		this.Username = src.Username;
		this.Password = src.Password;
		this.ServiceName = src.ServiceName;
		this.StasisApp = src.StasisApp;
		this.pbxID = src.pbxID;
	}
}
