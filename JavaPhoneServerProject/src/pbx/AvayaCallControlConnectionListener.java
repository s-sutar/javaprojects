package pbx;

import java.util.Date;

import javax.telephony.Address;
import javax.telephony.Call;
import javax.telephony.CallEvent;
import javax.telephony.Connection;
import javax.telephony.ConnectionEvent;
import javax.telephony.MetaEvent;
import javax.telephony.TerminalConnection;
import javax.telephony.callcontrol.CallControlCall;
import javax.telephony.callcontrol.CallControlConnectionEvent;
import javax.telephony.callcontrol.CallControlConnectionListener;

import pbx.EventInterpreter;
import acd.ACDManager;
import core.AppProperties;

import com.avaya.jtapi.tsapi.ITsapiCallIDPrivate;
import com.avaya.jtapi.tsapi.LucentCall;
import com.avaya.jtapi.tsapi.LucentV7Call;
import com.avaya.jtapi.tsapi.TsapiInvalidStateException;
import com.avaya.jtapi.tsapi.UserToUserInfo;
import com.avaya.jtapi.tsapi.impl.events.conn.LucentV5CallControlConnectionEvent;
import com.avaya.jtapi.tsapi.adapters.CallControlConnectionListenerAdapter;

import core.EventsReceived;
import core.LogWriter;

public class AvayaCallControlConnectionListener extends CallControlConnectionListenerAdapter {

	private String toNumber = null;
	private boolean hasNetworkReached = false;
	private Date establishTime;
	private Date disconnectTime;
	private boolean isAgent;
	private String uui;
	private String vdnNumber;
	private boolean hasCustomerAnswered = false;
	private Address myAddress;
	private boolean hasAlertingResponded = false;
	public int callIndex = 0;
	private boolean hasSentHangup = false;
	private int pbxID;
	private int _callid = 0;
	
	private int simResultCode = 0;

	public AvayaCallControlConnectionListener(int callIdx, String toNumber, Address address, String uui, String vdnNumber, int pbxID) {
		this.callIndex = callIdx;
		this.toNumber = toNumber;
		this.myAddress = address;
		this.uui = uui;
		this.vdnNumber = vdnNumber;
		this.isAgent = (uui == null) ? true : false;
		this.pbxID = pbxID;
	}
	
	public void setSimData(int resultCode) {
		this.simResultCode = resultCode;
	}
	
	public void setSystemCallID(Call call) {
        _callid= ((ITsapiCallIDPrivate)call).getTsapiCallID();
	}
	
    private String getAddressNameWithText(ConnectionEvent event) {
        String name = null;
        
        try {
          Connection connection = event.getConnection();
          Address addr = connection.getAddress();
          name = addr.getName();
        } catch (Exception e) {
        	LogWriter.instance.appendException(e);
		}
        
		return "Connection to Address " + name + " is ";
	}
    
	private String[] getPhoneNumbers(ConnectionEvent event) {
		String callingNumber = "null";
		String calledNumber = "null";

		try {
			int id = event.getID();
			int cause = event.getCause();

			Call call = event.getCall();
			Connection[] conns = (call != null) ? call.getConnections() : null;

			int callState = call.getState();
			String additional = "";

			if (conns != null && conns.length == 2 && conns[0] != null && conns[1] != null) {
				callingNumber = conns[0].getAddress().getName();
				calledNumber = conns[1].getAddress().getName();

				int callingConnState = conns[0].getState();
				int calledConnState = conns[1].getState();

				additional = String.format(", callingConnState=%d, calledConnState=%d", callingConnState,
						calledConnState);
			}

			LogWriter.instance.appendInfo(String.format("getPhoneNumbers: id=%d, cause=%d, calling=%s, called=%s, callState=%d %s", id,
					cause, callingNumber, calledNumber, callState, additional));
		} catch (Exception e) {
			LogWriter.instance.appendException(e);
		}
		
		return new String[] { callingNumber, calledNumber };
	}
    
	@Override
	public void callActive(CallEvent arg0) {
		LogWriter.instance.appendInfo("Connection callActive");
	}

	@Override
	public void callEventTransmissionEnded(CallEvent arg0) {
		LogWriter.instance.appendInfo("Connection callEventTransmissionEnded");
		
	}

	@Override
	public void callInvalid(CallEvent arg0) {
		LogWriter.instance.appendInfo("Connection callInvalid");
		
		LucentV7Call call = (LucentV7Call)arg0.getCall();
	
		if(call != null) {
			LogWriter.instance.appendInfo("Connection callInvalid: Call: " + call.getUserToUserInfo());

			Pbx pbx = PbxPool.GetInstance().GetPbx(Pbx.PbxType.AVAYA, pbxID);
			if( pbx != null) {
				pbx.removeCallObj(call);
			}
		}

		try {
			int lucentEventCauseCode = arg0.getCause();
			LogWriter.instance.appendInfo("Connection callInvalid: Cause: " + lucentEventCauseCode);
		} catch(Exception e) {
			LogWriter.instance.appendException(e);
		}
	}

	@Override
	public void multiCallMetaMergeEnded(MetaEvent arg0) {
		//LogWriter.instance.appendInfo("Connection multiCallMetaMergeEnded");
		interpret(arg0);
	}

	@Override
	public void multiCallMetaMergeStarted(MetaEvent arg0) {
		//LogWriter.instance.appendInfo("Connection multiCallMetaMergeStarted");
		interpret(arg0);
	}

	@Override
	public void multiCallMetaTransferEnded(MetaEvent arg0) {
		//LogWriter.instance.appendInfo("Connection multiCallMetaTransferEnded");
		interpret(arg0);
	}

	@Override
	public void multiCallMetaTransferStarted(MetaEvent arg0) {
		//LogWriter.instance.appendInfo("Connection multiCallMetaTransferStarted");
		interpret(arg0);
	}

	@Override
	public void singleCallMetaProgressEnded(MetaEvent arg0) {
		//LogWriter.instance.appendInfo("Connection singleCallMetaProgressEnded");
		interpret(arg0);
		
		int cause = arg0.getCause();
		String evtname = "NOTDEFINED";
		switch (cause) {
			case 203 :
				evtname = "CustomerBusy";
				break;
			case 102 :
				evtname = "Cancel";
				break;
			case 103 : 
				evtname = "Offline";
				break;
			case 104 : 
				evtname = "NumberMistake";
				break;
			case 205 : 
				evtname = "CustomerNoanswer";
				break;
			default :
				return;
		
		}
		
		if(hasSentHangup == true) {
	    	LogWriter.instance.appendInfo("Skipping singleCallMetaProgressEnded because already send hangup event" );        	
			return;
		}
		hasSentHangup = true;

		
        
	        	EventsReceived evt = new EventsReceived(evtname, this.vdnNumber);
	        	evt.CustomerID = extractCustomerID(this.uui);
	        	evt.SystemCallID = Integer.toString(this.callIndex);
	        	evt.Exten = extractCustomerPhoneNumber(this.uui);
	        	evt.pbxid = pbxID;
	        	String responseMsg = evt.ToJson();
			
	        	// LogWriter.instance.appendInfo(String.format("Response: %s", responseMsg));
	        	network.SocketHandler.notifyAllDailers(responseMsg);

        //} else {
    	//	LogWriter.instance.appendInfo("Skipping singleCallMetaProgressEnded because ID: " + arg0.getID() + " expecting: " + _callid );        	
        //}
        
	}

	@Override
	public void singleCallMetaProgressStarted(MetaEvent arg0) {
		//LogWriter.instance.appendInfo("Connection singleCallMetaProgressStarted");
		interpret(arg0);
	}

	@Override
	public void singleCallMetaSnapshotEnded(MetaEvent arg0) {
		//LogWriter.instance.appendInfo("Connection singleCallMetaSnapshotEnded");
		interpret(arg0);		
	}

	@Override
	public void singleCallMetaSnapshotStarted(MetaEvent arg0) {
		//LogWriter.instance.appendInfo("Connection singleCallMetaSnapshotStarted");
		interpret(arg0);
	}
	
	private void interpret(MetaEvent event) {		
		String classsName = this.getClass().getName();
        String methodName = EventInterpreter.getMethodName(3);
        int id = event.getID();
        int cause = event.getCause();
        String causeText = EventInterpreter.parseCause(cause);
        
        LogWriter.instance.appendInfo(String.format("%S %s, id=%d, cause=%d-%s",
        		classsName, methodName, id, cause, causeText));
	}

	@Override
	public void connectionAlerting(ConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getAddressNameWithText(arg0) + "ALERTING");
		
		//{"Channel":"610001","EventName":"OriginateResponse","Exten":"fus00008043497192","Message":"success","Response":"Success","Timestamp":"11:20:04.4171"}
		//{"Channel":"420007","EventName":"Dial:Begin","Exten":"fus00005058371760","Message":"success","Timestamp":"09:53:09.9571"}
		String[] temp = getPhoneNumbers(arg0);
		
		if(temp != null && temp.length >= 1) {
			String extension = temp[0];
			String destNumber = this.toNumber;
			
/*			EventsReceived evtOriginateResponse = new EventsReceived("OriginateResponse", extension);
			evtOriginateResponse.Exten = destNumber;		
			evtOriginateResponse.Response = "Success";
			evtOriginateResponse.Message = "Success";
			
			String responseMsg = evtOriginateResponse.ToJson();
			WebClient.GetInstance().SendMessage(responseMsg);*/			
			
	    	EventsReceived evt = new EventsReceived("Dial:Begin", extension);
			evt.Message = "success";
			evt.Exten = destNumber;
		    evt.CallIndex = this.callIndex;
		    evt.SystemCallID = Integer.toString(this.callIndex);
			evt.pbxid = pbxID;
			
			String msg = evt.ToJson();
			//network.SocketHandler.notifyAllDailers(msg);

			//WebClient.GetInstance().SendMessage(msg);
		}
	}

	@Override
	public void connectionConnected(ConnectionEvent arg0) {
		//{"Channel":"610001","EventName":"bridgeoutbound","Exten":"900008043497192","Message":"success","Timestamp":"19:25:51.1233"}
		LogWriter.instance.appendInfo(getAddressNameWithText(arg0) + "CONNECTED");

		String[] temp = getPhoneNumbers(arg0);
		
		String realAddressName = arg0.getConnection().getAddress().getName();
		String extractedCustomerID = extractCustomerID(this.uui);
		String extractedCustomerNumber = extractCustomerPhoneNumber(this.uui);
		
		if(temp != null && temp.length >= 1) {
			String extension = realAddressName;
			String destNumber = this.toNumber;
			//EventsReceived evtOriginateResponse = new EventsReceived("OriginateResponse", extension);
			//evtOriginateResponse.Exten = destNumber;		
			//evtOriginateResponse.Response = "success";
			//evtOriginateResponse.Message = "Connected";
			//String responseMsg = evtOriginateResponse.ToJson();
			//LogWriter.instance.appendInfo(responseMsg);
			//network.SocketHandler.notifyAllDailers(responseMsg);
			//WebClient.GetInstance().SendMessage(responseMsg);	
			
			//EventsReceived evt = new EventsReceived("bridgeoutbound", extension);
		    //evt.Response = "success";
		    //evt.Message = extractedCustomerID;
		    //evt.Exten = extractedCustomerNumber;
		    //evt.CallIndex = this.callIndex;
		    //evt.SystemCallID = Integer.toString(this.callIndex);
		    
			//if(!extension.startsWith("T")) {
				EventsReceived evt = new EventsReceived("CustomerAnswer", extension);
				evt.CustomerID = extractedCustomerID;
				evt.SystemCallID = Integer.toString(this.callIndex);
				evt.Exten = extractedCustomerNumber;
				evt.pbxid = pbxID;
	
			    String msg = evt.ToJson();
			    //LogWriter.instance.appendInfo(msg);
				if (extension != null && evt.Exten != evt.Channel){
					network.SocketHandler.notifyAllDailers(msg);
					//WebClient.GetInstance().SendMessage(msg);
				}
			//}
			//else {
			//	LogWriter.instance.appendInfo("Blocking CustomerAnswer for trunk: " + extension);
			//}
		}
	}

	@Override
	public void connectionCreated(ConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getAddressNameWithText(arg0) + "CREATED");
	}

	@Override
	public void connectionDisconnected(ConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getAddressNameWithText(arg0) + "DISCONNECTED");
		
		this.disconnectTime = new Date();

		getPhoneNumbers(arg0);

		/*		
		String[] temp = getPhoneNumbers(arg0);
		
		if(temp != null && temp.length >= 1) {
			String callingNumber = temp[0];
			
			String extractedCustID = extractCustomerID(this.uui);
			String customerNumber = extractCustomerPhoneNumber(this.uui);
		
			EventsReceived evt = new EventsReceived("Hangup", callingNumber);
			evt.Exten = customerNumber;
			evt.Message = extractedCustID;
		    evt.CallIndex = this.callIndex;
			evt.pbxid = pbxID;
		    evt.SystemCallID = Integer.toString(this.callIndex);
			
			String responseMsg = evt.ToJson();
			//network.SocketHandler.notifyAllDailers(responseMsg);
		}		
		*/
	}

	@Override
	public void connectionFailed(ConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getAddressNameWithText(arg0) + "FAILED");
		
/*		String[] temp = getPhoneNumbers(arg0);
		
		if(temp != null && temp.length >= 1) {
			String extension = temp[0];
			String destNumber = this.toNumber;
	    	EventsReceived evt = new EventsReceived("", extension);
			evt.Message = "success";
			evt.Exten = destNumber;
			evt.pbxid = pbxID;
			String msg = evt.ToJson();
			WebClient.GetInstance().SendMessage(msg);
		}*/
	}

	@Override
	public void connectionInProgress(ConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getAddressNameWithText(arg0) + "IN PROGRESS");
	}

	@Override
	public void connectionUnknown(ConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getAddressNameWithText(arg0) + "UNKNOW");
	}

	@Override
	public void connectionAlerting(CallControlConnectionEvent event) {
		LogWriter.instance.appendInfo("Connection connectionAlerting");

		String calledNumber = this.toNumber;
		int calledConnState = Connection.ALERTING;
		int callCtrlCause = 100;
		
		if(event != null) {
			Object[] results = interpret(event);
			LogWriter.instance.appendInfo(String.format( "Alerting results: 0:%s 1:%s 2:%s 3:%s 4:%s" , results[0], results[1], results[2], results[3], results[4]));

			if(results == null || results.length != 5 ||
					results[2] == null || results[4] == null) {
				return;
			}
			calledNumber = (results[1] != null) ? (String)results[1] : null;
			calledConnState = (results[2] != null) ? (int)results[2] : 0;
			callCtrlCause = (int)results[4];
		}
		
		if(uui == null) {
			return; // to ignore agent alert
		}
	
		try {		
			LogWriter.instance.appendInfo(String.format( "Alerting calledConnState:%d callCtrlCause:%d calledNumber:%s" , calledConnState, callCtrlCause, calledNumber));
			if( calledConnState == Connection.ALERTING &&/*callCtrlCause == 100 &&*/
					ACDManager.GetInstance().GetAgent(calledNumber) == null) {
				
				LogWriter.instance.appendInfo(String.format( "hasAlertingResponded:%d" , hasAlertingResponded?1:0));
				if(hasAlertingResponded) {
					return;
				}
				
				hasAlertingResponded = true;
				
				String extractedCustID = extractCustomerID(this.uui);
				String customerNumber = extractCustomerPhoneNumber(this.uui);
				
				EventsReceived evt = new EventsReceived("OriginateResponse", calledNumber);
				evt.Context = "0";
				evt.Exten = customerNumber;
				evt.Message = extractedCustID;
			    evt.CallIndex = this.callIndex;
			    evt.SystemCallID = Integer.toString(this.callIndex);
				evt.pbxid = pbxID;
				String msg = evt.ToJson();
				msg = msg.replace("}",",\"SubEvent\":\"customer_alerting\"}");
				//LogWriter.instance.appendInfo(String.format("Response: %s", msg));
				network.SocketHandler.notifyAllDailers(msg);
/*				AvayaManager.instance.RunCheckTimeoutThread(event.getCall(), customerID,
						calledNumber, "", noAnsTimeout, requestHandler);*/
				
				//network.SocketHandler.notifyAllDailers(response);
			}
		} catch(Exception e) {
			LogWriter.instance.appendException(e);
		}
	}

	@Override
	public void connectionDialing(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("Connection connectionDialing");
	}

	@Override
	public void connectionDisconnected(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("Connection connectionDisconnected");
		
		handleHangUpCauseCode(arg0);
	}

	public String extractCustomerID(String text) {
		if (text == null || text.isEmpty()) {
			return "";
		}

		String customerID = "";
		
		//Format:  dialer:22;p:080111222;j:10
		String[] parts = text.split(";");
		if( parts.length > 2) {
			String parts2[] = parts[0].split(":");
			customerID = parts2[1];
		}

		return customerID;
	}
	
	public String extractCustomerPhoneNumber(String text) {
		if (text == null || text.isEmpty()) {
			return "";
		}

		//if (AppProperties.instance.bPredictiveCall == false) {
		//	return text;
		//}
		
		String customerPhoneNumber = "";
		
		//Format:  dialer:22;p:080111222;j:10
		String[] parts = text.split(";");
		if( parts.length > 2) {
			String parts2[] = parts[1].split(":");
			customerPhoneNumber = parts2[1];
		}

		return customerPhoneNumber;
	}
	
	@Override
	public void connectionEstablished(CallControlConnectionEvent event) {
		LogWriter.instance.appendInfo("Connection connectionEstablished");
		
		String callingNumber = this.vdnNumber;
		String calledNumber = this.toNumber;
		int calledConnState = Connection.CONNECTED;
		TerminalConnection terminalConn = null;
		int callCtrlCause = 100;

		if(event != null) {		
			Object[] results = interpret(event);
	
			if(event.getCause() == 110) {
				// set establish time for AU call
				this.establishTime = new Date();
				
				LogWriter.instance.appendInfo(String.format("Establish: time=%s custID=%s",
						this.establishTime.toString(), this.uui));
			}
			
			if(results == null || results.length != 5 ||
					results[0] == null || results[1] == null ||
					results[2] == null || results[4] == null) {
				return;
			}
			String isAgent = this.isAgent ? "true" :"false";
			LogWriter.instance.appendInfo(String.format( "Establish results: 0:%s 1:%s 2:%s 3:%s 4:%s isAgent:%s" , results[0], results[1], results[2], results[3], results[4],isAgent));

			callingNumber = (String)results[0];
			calledNumber = (String)results[1];
			calledConnState = (int)results[2];
			terminalConn = (results[3] != null) ? (TerminalConnection)results[3] : null;
			callCtrlCause = (int)results[4];
		}
		
		String extractedCustomerID = extractCustomerID(this.uui);
		String extractedCustomerNumber = extractCustomerPhoneNumber(this.uui);
		String addressName = "";
		
		try {
			addressName = (myAddress != null) ? myAddress.getName() : null;
			
			LogWriter.instance.appendInfo(String.format( "Establish calledConnState:%d callCtrlCause:%d calledNumber:%s" , calledConnState, callCtrlCause, calledNumber));
			
			if(calledConnState != Connection.CONNECTED) {
				return;
			}
			
			if (ACDManager.GetInstance().GetAgent(calledNumber) != null)
			    LogWriter.instance.appendInfo("Branch Check " + calledNumber + "   " + ACDManager.GetInstance().GetAgent(calledNumber).getAgentAddress().getName());
			else
				LogWriter.instance.appendInfo("Branch Check " + calledNumber + "   agent null");
			
			
			if(callCtrlCause == 100) {
			
				if(ACDManager.GetInstance().GetAgent(calledNumber) != null || this.isAgent) { // AGENT ANSWERS
					LucentV7Call call = (LucentV7Call)event.getCall();
					UserToUserInfo uui = call.getUserToUserInfo();
					String customerID = "";
					String customerNumber = "";
					
					if(uui != null) {						
						customerID = extractCustomerID(uui.getString());
						customerNumber = extractCustomerPhoneNumber(uui.getString());
					}
										
					if(customerNumber == null) {						
						return;
					}
					
					// context is for agent info, message is for customer info
					//String response = String.format("Event: OriginateResponse\r\nChannel: %s\r\nContext: %s\r\nExten: %s\r\nsubevent: agent_answer\r\nMessage: %s",
					//		this.vdnNumber, 0, customerNumber, customerID);		
					//network.SocketHandler.notifyAllDailers(response);
					
					EventsReceived evt = new EventsReceived("OriginateResponse", this.vdnNumber);
					evt.Context = "0";
					evt.Exten = customerNumber;
					evt.Message = customerID;
				    evt.CallIndex = this.callIndex;
				    evt.SystemCallID = Integer.toString(this.callIndex);
					evt.pbxid = pbxID;
					String msg = evt.ToJson();
					msg = msg.replace("}",",\"SubEvent\":\"agent_answer\"}");
					
					network.SocketHandler.notifyAllDailers(msg);
				} else if(callingNumber == addressName) { // CUSTOMER ANSWERS
					hasCustomerAnswered = true;
					
					//String response = String.format("Event: OriginateResponse\r\nChannel: %s\r\nContext: %s\r\nExten: %s\r\nsubevent: customer_answer\r\nMessage: %s",
					//		calledNumber, 0, extractedCustomerNumber, extractedCustomerID);	
					
					//network.SocketHandler.notifyAllDailers(response);
					
					EventsReceived evt = new EventsReceived("OriginateResponse", calledNumber);
					evt.Context = "0";
					evt.Exten = extractedCustomerNumber;
					evt.Message = extractedCustomerID;
				    evt.CallIndex = this.callIndex;
				    evt.SystemCallID = Integer.toString(this.callIndex);
					evt.pbxid = pbxID;
					String msg = evt.ToJson();
					msg = msg.replace("}",",\"SubEvent\":\"customer_answer\"}");
					
					network.SocketHandler.notifyAllDailers(msg);
				} else if(terminalConn == null && this.isAgent == false && hasCustomerAnswered == false) {
					hasCustomerAnswered = true;

					//String response = String.format("Event: OriginateResponse\r\nChannel: %s\r\nContext: %s\r\nExten: %s\r\nsubevent: customer_answer\r\nMessage: %s",
					//		calledNumber, 0, extractedCustomerNumber, extractedCustomerID);			

					//network.SocketHandler.notifyAllDailers(response);
					
					EventsReceived evt = new EventsReceived("OriginateResponse", calledNumber);
					evt.Context = "0";
					evt.Exten = extractedCustomerNumber;
					evt.Message = extractedCustomerID;
				    evt.CallIndex = this.callIndex;
				    evt.SystemCallID = Integer.toString(this.callIndex);
					evt.pbxid = pbxID;
					String msg = evt.ToJson();
					msg = msg.replace("}",",\"SubEvent\":\"customer_answer\"}");
					
					//LogWriter.instance.appendInfo(String.format("Response: %s", msg));
					network.SocketHandler.notifyAllDailers(msg);
					
				}//end if-else customer answers
				else if(callingNumber != addressName && terminalConn == null && this.isAgent == false && hasCustomerAnswered == true) {
					
				    //	String response = String.format("Event: OriginateResponse\r\nChannel: %s\r\nContext: %s\r\nExten: %s\r\nsubevent: agent_answer\r\nMessage: %s",
				    //			addressName, 0, extractedCustomerNumber, extractedCustomerID);	
					
					//LogWriter.instance.appendInfo(response);
					
					EventsReceived evt = new EventsReceived("OriginateResponse", event.getConnection().getAddress().getName());
					evt.Context = "0";
					evt.Exten = extractedCustomerNumber;
					evt.Message = extractedCustomerID;
				    evt.CallIndex = this.callIndex;
				    evt.SystemCallID = Integer.toString(this.callIndex);
					evt.pbxid = pbxID;
					String msg = evt.ToJson();
					msg = msg.replace("}",",\"SubEvent\":\"agent_answer\"}");
					LogWriter.instance.appendInfo(msg);
					//network.SocketHandler.notifyAllDailers(msg);
				}//end if-else agent answer
			} else if(callCtrlCause == 210 && this.isAgent) {
				//this.hasAgentAnswered = true;
				LucentCall call = (LucentCall)event.getCall();
				UserToUserInfo uuiInfo = call.getUserToUserInfo();
				String uuidata = uuiInfo.getString();
				String custID = extractCustomerID(uuidata);
				
				//String response2 = String.format("Event: OriginateResponse\r\nChannel: %s\r\nExten: %s\r\nsubevent: agent_answer\r\nMessage: %s",
				//		this.vdnNumber, calledNumber, custID);
				
				//network.SocketHandler.notifyAllDailers(response2);
				
				EventsReceived evt = new EventsReceived("OriginateResponse", this.vdnNumber);
				evt.Exten = calledNumber;
				evt.Message = custID;
			    evt.CallIndex = this.callIndex;
			    evt.SystemCallID = Integer.toString(this.callIndex);
				evt.pbxid = pbxID;
				String msg = evt.ToJson();
				msg = msg.replace("}",",\"SubEvent\":\"agent_answer\"}");
				LogWriter.instance.appendInfo(msg);
				//network.SocketHandler.notifyAllDailers(msg);
			}
		} catch(Exception e) {
			LogWriter.instance.appendException(e);
		}
	}

	@Override
	public void connectionFailed(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("Connection connectionFailed");
		
		handleHangUpCauseCode(arg0);		
	}
	
	public void handleHangUpCauseCode(CallControlConnectionEvent event) {		
		int lucentEventCauseCode = this.simResultCode;
		int callCtrlCause = 100;
		String calledNumber = this.toNumber;
		String callingNumber = this.vdnNumber;
		//int metaEvtCause = 0;
		
		String realAddressName = event.getConnection().getAddress().getName();
		/*if(realAddressName.contains(calledNumber) == false) {
			LogWriter.instance.appendInfo(String.format(
					"==> handleHangUpCauseCode skipping hangup because name does not match: %s, calledNumber=%s, callingNumber=%s",
					realAddressName, calledNumber, callingNumber));	
			return;
		}*/

		String huEventName = "CustomerHangup";
		
		if(hasSentHangup == true) {
			return;
		}
		hasSentHangup = true;
		
		try {
	    	Pbx pbx = PbxPool.GetInstance().GetPbx(Pbx.PbxType.AVAYA, pbxID);
	    	if( pbx != null) {
	    		pbx.removeCallObj(event.getCall());
	    	}

			if(event instanceof LucentV5CallControlConnectionEvent) {
				LucentV5CallControlConnectionEvent temp  = (LucentV5CallControlConnectionEvent)event;
				
				//int evtCause = temp.getCause();
				lucentEventCauseCode = temp.getCSTACause();
				callCtrlCause = temp.getCallControlCause();
				//metaEvtCause = temp.getMetaEvent().getCause();
				//int reason = temp.getReason();
				
				calledNumber = (temp.getCalledAddress() != null) ? temp.getCalledAddress().getName() : "";
				callingNumber = (temp.getCallingAddress() != null) ? temp.getCallingAddress().getName() : "";

				LogWriter.instance.appendInfo(String.format(
						"==> handleHangUpCauseCode. cstaCause=%d, calledNumber=%s, callingNumber=%s",
						lucentEventCauseCode, calledNumber, callingNumber));

/*				int isupCauseCode = EventInterpreter.toIsupCauseCode(lucentEventCauseCode);
				String isupCauseText = EventInterpreter.toIsupCauseText(isupCauseCode);
				String lucentEventCauseText = EventInterpreter.toLucentEventCauseText(lucentEventCauseCode);
	
				LogWriter.instance.appendInfo(String.format(
						"==> MyCallControlConnectionListener. cause=%d, cstaCause=%d-%s, isupCause=%d-%s, callCtrlCause=%d, metaEventCause=%d, reason=%d%n",
						evtCause, lucentEventCauseCode, lucentEventCauseText, isupCauseCode, isupCauseText, 
						callCtrlCause, metaEvtCause, reason));
						*/
			}
		} catch(Exception e) {
			LogWriter.instance.appendException(e);
		}

		String result = "failed";
		String msg = "";
		
		// process according to cause code
		switch (lucentEventCauseCode) {
			case 81 :// EC_CALL_CANCELLED - ISUP 21 (call rejected)
			{
				msg = "81 EC_UNASSIGNED_NUMBER - ISUP 0";
				huEventName = "Offline";
			}
			break;
		
			case 5 :// EC_CALL_CANCELLED - ISUP 21 (call rejected)
				{
					msg = "5 EC_CALL_CANCELLED - ISUP 21 (call rejected)";
					huEventName = "Cancel";
				}
				break;
				
			case 3 :// EC_BUSY - ISUP 17 (user busy)
				{
					msg = "3 EC_BUSY - ISUP 17 (user busy)";
					huEventName = "CustomerBusy";
				}
				break;
				
			case 60 :// EC_ALERT_TIME_EXPIRED - ISUP 18 (no user responding)
				{
					msg = "60 EC_ALERT_TIME_EXPIRED - ISUP 18 (no user responding)";
					huEventName = "CustomerNoanswer";
				}
				break;
				
			case 21 :// EC_NETWORK_NOT_OBTAINABLE - ISUP 19 (no answer from the user)
				{
					msg = "21 EC_NETWORK_NOT_OBTAINABLE - ISUP 19 (no answer from the user)";
				}
				break;
				
			case 29 :// EC_REORDER_TONE - ISUP 22 (number changed)
				{
					msg = "29 EC_REORDER_TONE - ISUP 22 (number changed)";
					huEventName = "NumberChanged";
				}
				break;
				
			case 15 :// EC_INCOMPATIBLE_DESTINATION
				{
					msg = "15 EC_INCOMPATIBLE_DESTINATION";
				}
				break;
			
			case 13 :// EC_DEST_NOT_OBTAINABLE - ISUP 31 (normal unspecified)
				{
					long timeTaken = 0;
					
					if(this.establishTime == null) {
						LogWriter.instance.appendInfo("TimeTaken: this.establishTime");
					} else if(this.disconnectTime == null){
						LogWriter.instance.appendInfo("TimeTaken: this.disconnectTime");
					} else {
						timeTaken = this.disconnectTime.getTime() - this.establishTime.getTime();
						timeTaken = timeTaken / 1000; //convert ms to sec
						
						LogWriter.instance.appendInfo(String.format("TimeTaken: time=%d sec", timeTaken));
					}
					
					if(hasNetworkReached) {
						msg = "15 EC_REORDER_TONE - ISUP 22 (number changed)";
						lucentEventCauseCode = 15;
						huEventName = "NumberChanged";
					} else if(timeTaken == 0) {
				    	msg = "13 EC_DEST_NOT_OBTAINABLE - ISUP 31 (normal unspecified)";
						huEventName = "Offline";
					} else if(timeTaken < 20/*AppProperties.intance.auThresholdBusy*/) {
						msg = "3 EC_BUSY - ISUP 17 (user busy)";
						lucentEventCauseCode = 3;
						huEventName = "CustomerBusy";
					} else if(timeTaken < 30/*AppProperties.intance.auThresholdPowerOff*/) {
						msg = "60 EC_ALERT_TIME_EXPIRED - ISUP 18 (no user responding)";
						lucentEventCauseCode = 60;
						huEventName = "Offline";
					} else {
						msg = "5 EC_CALL_CANCELLED - ISUP 21 (call rejected)";
						lucentEventCauseCode = 5;
						huEventName = "Offline";
					}
				}
				break;
			
			case 31 :
			case 22 :
			case -1 :
			{
				if(callCtrlCause != 100) {
					return;
				}
				
				result = "success";
			}
			break;
				
			case 10 :// EC_CALL_NOT_ANSWERED - for makePredictiveCall
			{
				msg = "10 EC_CALL_NOT_ANSWERED";
				huEventName = "CustomerNoanswer";
			}
			break;
				
			case 28 : // redirect
			{
				return;
			}
			
			default :
				{
					msg = "default";
				}
		}//end switch		
		
/*		if(this.requestHandler != null) {
			//hasDisconnectedResponded = true;
			
			String callingNumber = (this.myAddress != null) ? this.myAddress.getName() : "";
			String response = String.format("Event: OriginateResponse\r\nResponse: %s\r\nChannel: %s\r\nContext: %s\r\nExten: %s\r\nReason: %d\r\nMessage: %s",
					result, callingNumber, msg, calledNumber, lucentEventCauseCode, extractedCustomerID);
			
			if("success".equals(result) == false) {
				hasCustomerAnswered = true; //to cancel no answer timeout checking when call fails
				
				//Caller caller = AvayaManager.instance.getCaller(callingNumber);				
				if(caller != null) {
					caller.usedConnCount--;// customer connection
				}
			}

			this.requestHandler.sendBackToClient(response);
		}*/

/*		int finalCause = 0;
		EventsReceived evt = new EventsReceived("Hangup", callingNumber);
		evt.Exten = calledNumber;
		evt.Message = "success";
		evt.ChannelState = Integer.toString(finalCause);
		evt.pbxid = pbxID;
		evt.Response = String.format("call's hung up , cause=%d, finalCause=%d", lucentEventCauseCode, finalCause);
		
		String responseMsg = evt.ToJson();
		WebClient.GetInstance().SendMessage(responseMsg);*/
		
		//----------------------------------
/*		EventsReceived evt = new EventsReceived("OriginateResponse", callingNumber);
		evt.Exten = calledNumber;		
		evt.Response = result;
		evt.Context = msg;
		evt.Reason = Integer.toString(lucentEventCauseCode);
		evt.Message = extractCustomerID(this.uui);
	    evt.CallIndex = this.callIndex;
	    evt.SystemCallID = Integer.toString(this.callIndex);
		evt.pbxid = pbxID;
		
		String responseMsg = evt.ToJson();
		responseMsg = responseMsg.replace("}",",\"SubEvent\":\"hangup\"}");

		//LogWriter.instance.appendInfo(String.format("Response: %s", responseMsg));
		network.SocketHandler.notifyAllDailers(responseMsg);
		//WebClient.GetInstance().SendMessage(responseMsg);
*/		
		
		String extension = event.getConnection().getAddress().getName();

		EventsReceived evt = new EventsReceived(huEventName, extension);
		evt.CustomerID = extractCustomerID(this.uui);
		evt.SystemCallID = Integer.toString(this.callIndex);
		evt.Exten = extractCustomerPhoneNumber(this.uui);
		evt.pbxid = pbxID;
		String responseMsg = evt.ToJson();
		
		// LogWriter.instance.appendInfo(String.format("Response: %s", responseMsg));
		network.SocketHandler.notifyAllDailers(responseMsg);

	}

	@Override
	public void connectionInitiated(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("Connection connectionInitiated");
		
	}

	@Override
	public void connectionNetworkAlerting(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("Connection connectionNetworkAlerting");
		
	}

	@Override
	public void connectionNetworkReached(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("Connection connectionNetworkReached");
		
		this.hasNetworkReached = true;
	}

	@Override
	public void connectionOffered(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("Connection connectionOffered");
		
	}

	@Override
	public void connectionQueued(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("Connection connectionQueued(");
		
	}

	@Override
	public void connectionUnknown(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("Connection connectionUnknown");
		
	}
	
	private Object[] interpret(CallControlConnectionEvent event) {
		Object[] results = new Object[5];

		try {			
			String classsName = this.getClass().getName();
	        String methodName = EventInterpreter.getMethodName(3);
	        int id = event.getID();
	        int cause = event.getCause();
	        String causeText = EventInterpreter.parseCause(cause);
			int callCtrlCause = event.getCallControlCause();
			String callCtrlCauseText = EventInterpreter.parseCause(callCtrlCause);
	        
	        String callingNumber = "null";
	        String calledNumber = "null";
	        Call call = event.getCall();        
	        Connection[] conns = (call != null) ? call.getConnections() : null;
	        
	        int callState = call.getState();
	        String callStateText = EventInterpreter.parseCallState(callState);
	        String additional = ", hashCode=" + this.hashCode() + ", isAgent=" + this.isAgent + ", customerID=" + this.uui;
	        
	        if(conns != null && conns.length >= 2 && conns[0] != null && conns[1] != null) {        	
	            Connection callingConn = conns[0];
	            Connection calledConn = conns[1];
	            
	        	// Get connection state
	        	Address callingAddress = callingConn.getAddress(); 
	        	Address calledAddress = calledConn.getAddress();
	        	callingNumber = callingAddress.getName();
	        	calledNumber = calledAddress.getName();
	        	
	        	int callingConnState = callingConn.getState();
	        	int calledConnState = calledConn.getState();        	
	        	String callingConnStateText = EventInterpreter.parseConnectionState(callingConnState);
	        	String calledConnStateText = EventInterpreter.parseConnectionState(calledConnState);
	        	
	        	additional = String.format("%s, callingConnState=%d-%s, calledConnState=%d-%s",
	        			additional, callingConnState, callingConnStateText, calledConnState,calledConnStateText);
	        	
	        	// Get TerminalConnection
	        	TerminalConnection[] termConns = callingConn.getTerminalConnections();
	        	
	        	// Set results
	        	results[0] = callingNumber;
	        	results[1] = calledNumber;
	        	results[2] = calledConnState;
	        	results[3] = (termConns != null && termConns.length > 0) ? termConns[0] : null;
	        	results[4] = callCtrlCause;
	        } else if (conns != null && conns.length >= 1 && conns[0] != null) {
	        	results[2] = Connection.ALERTING;
	        	results[4] = callCtrlCause;
	        }
	        
	        LogWriter.instance.appendInfo(String.format("%S %s, id=%d, cause=%d-%s, calling=%s, called=%s, callState=%d-%s, callCtrlCause=%d-%s%s",
	        		classsName, methodName, id, cause, causeText, callingNumber, calledNumber, callState, callStateText, callCtrlCause, callCtrlCauseText, additional));
	        
		} catch(Exception e) {
			LogWriter.instance.appendException(e);
		}
        
        return results;
	}
}
