package pbx;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.telephony.Address;
import javax.telephony.Call;
import javax.telephony.CallListener;
import javax.telephony.Connection;
import javax.telephony.InvalidPartyException;
import javax.telephony.JtapiPeer;
import javax.telephony.JtapiPeerFactory;
import javax.telephony.Provider;
import javax.telephony.ProviderEvent;
import javax.telephony.ProviderListener;
import javax.telephony.Terminal;
import javax.telephony.TerminalConnection;
import javax.telephony.callcenter.Agent;
import javax.telephony.callcontrol.CallControlCall;
import javax.telephony.callcontrol.CallControlConnection;
import javax.telephony.callcontrol.CallControlTerminalConnection;
import javax.telephony.CallEvent;


import com.avaya.jtapi.tsapi.LucentAddress;
import com.avaya.jtapi.tsapi.LucentConnection;
import com.avaya.jtapi.tsapi.LucentV10Call;
import com.avaya.jtapi.tsapi.LucentV7CallInfo;
import com.avaya.jtapi.tsapi.TsapiInvalidArgumentException;
import com.avaya.jtapi.tsapi.TsapiPlatformException;
import com.avaya.jtapi.tsapi.TsapiUnableToSendException;
import com.avaya.jtapi.tsapi.UserToUserInfo;
import com.avaya.jtapi.tsapi.ITsapiCallIDPrivate;

import acd.ACDManager;
import core.AppProperties;
import core.EventsReceived;
import core.LogWriter;
import db.AgentModel;
import network.WebClient;

public class AvayaPbx extends Pbx implements ProviderListener {

	class AvayaChannelInfo {
		
		private LucentV7CallInfo _lv7call;
	    private int _callid;
	    private int _estcount;
	    private boolean _istalking;
	    private String _callerID;
	    private String _calledID;

	    public AvayaChannelInfo()
	    {
	        ClearCall();
	    }

	    public LucentV7CallInfo getLucentV7CallInfo()
	    {
	    	return this._lv7call;
	    }
	    public void setLucentV7Call(LucentV7CallInfo LV7Call)
	    {
	    	this._lv7call = LV7Call;
	    }
	    
	    public int getEstCount()
	    {
	    	return this._estcount;
	    }
	    public void setEstCount(int estCount)
	    {
	    	this._estcount = estCount;
	    }

	    public int getCallID()
	    {
	    	return this._callid;
	    }
	    public void SetCallID(int CallID)
	    {
	    	this._callid = CallID;
	    }
	    
	    public boolean getisTalking()
	    {
	    	return this._istalking;
	    }
	    public void setisTalking(boolean isTalking)
	    {
	    	this._istalking = isTalking;
	    }
	    
	    public String getCallerID()
	    {
	    	return this._callerID;
	    }
	    
	    public void setCallerID(String CallerID)
	    {
	    	this._callerID = CallerID;
	    }
	    
	    public String getCalledID()
	    {
	    	return this._calledID;
	    }
	    
	    public void setCalledID(String CalledID)
	    {
	    	this._calledID = CalledID;
	    }	    
	    
	    public boolean RegisterCall(Call call)
	    {
	        boolean result = false;
	        
	        if (_lv7call == null)
	        {
	            ClearCall();

	           _lv7call = (LucentV7CallInfo)call;

	           _callid= ((ITsapiCallIDPrivate)call).getTsapiCallID();
	           
	            result = true;
	        }
	        

	        return result;

	    }

	    public void ClearCall()
	    {
	        _lv7call = null;
	        _callid = 0;
	        _estcount=0;
	        _istalking = false;

	    }

	    public boolean isMineEvt(Call call)
	    {  
	        
	        if (_lv7call == null)
	            return false;

	        boolean result = false;
	        

	        if (((ITsapiCallIDPrivate)call).getTsapiCallID() == _callid)
	            result = true;

	        return result;
	    }

	    public boolean isActive()
	    {
	        boolean result = false;
	        if(_lv7call!=null)
	            result=true;
	        return result;
	    }

	    public int GetEstCountUpValue()
	    {
	        return ++_estcount;
	    }


	}
	
	public static int ESTCOUNT_TALKING = 2;
    public static int ESTCOUNT_TRANSFERCOMPLETE = 3;
    
	private Provider provider = null;
	private String serviceName = null;
	private String url = null;
	private String port = null;
	public static ArrayList<SimCall> simCallList = new ArrayList<>();
	public static ArrayList<LucentV10Call> dialedCallList = new ArrayList<>();
	
	public static AvayaChannelInfo[] channelInfo = new AvayaChannelInfo[2];
	
	public int masterCallIndex = 1;
	
	private int retryCount = 0;
	
	public AvayaPbx(pbx.PbxInfo pbxInfo) {
		super(pbxInfo);
		
		this.pbxID = pbxInfo.pbxID;
		this.Type = PbxType.AVAYA;
		
		for (int i = 0; i < 2; i++)
            if(channelInfo[i]==null)
                channelInfo[i] = new AvayaChannelInfo();
		
		if( AppProperties.instance.isOutgoingSimulation == true) {

	        new Thread("SimCall Manager") {		            	
				public void run() {
					while(true) {
						try {
							sleep(1000);
						
							//LogWriter.instance.appendInfo("Checking SimCalls: ");
							ArrayList<SimCall> finishedsimCalls = new ArrayList<>();
							synchronized(simCallList) {
								for( SimCall call : simCallList) {
									Date now = new Date();
									long nowTime = now.getTime();
									
									if( call.startAlertTime > 0 && call.startAlertTime < nowTime && call.alerted == false) {
										call.alerted = true;
										call.listener.connectionAlerting(null);
									}
									
									if( call.ansTime > 0 && call.ansTime < nowTime && call.answered == false) {
										call.answered = true;
										call.listener.connectionEstablished(null);								
									}
									
									if( call.endTime < nowTime) {
										call.listener.connectionDisconnected(null);
										finishedsimCalls.add(call);
									}
								}
								
								for( SimCall call : finishedsimCalls) {
									simCallList.remove(call);
								}
							}
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
	        }.start();
	        
			//AvayaCallControlConnectionListener simlisten = new AvayaCallControlConnectionListener("08011112222", null, "dialer:123;curphoneno:080111222", "18125");
			//simlisten.setSimData(-1);
			//simCallList.add(new SimCall(simlisten, 10, 20));
		}		
	}
	
	public String finishInit() {
		return "";
	}
	
	public void removeCallObj(Object call) {
		LogWriter.instance.appendInfo("removeCallObj: callCount=" + dialedCallList.size());
		if( dialedCallList.contains(call))
			dialedCallList.remove(call);
	}
	
	@Override
	public CallOriginator GetCallOriginator(String fromNumber, String option) {
		CallOriginator obj = null;

		Address address = null;
		Terminal terminal = null;
		
		String exceptionMsg = "";
		
		if(this.provider != null) {
			try {
				address = this.provider.getAddress(fromNumber);
				Terminal[] terms = address.getTerminals();
				if( terms != null && terms.length > 0 ) {
					//terminal = this.provider.getTerminal(fromNumber);
					terminal = terms[0];
				}
			} catch(TsapiInvalidArgumentException e) {
				exceptionMsg = e.getMessage();
				LogWriter.instance.appendError("Failed to create CallOriginator: fromNumber=" + fromNumber + ", ex=" + e.getMessage());
			} catch(Exception e) {
				exceptionMsg = e.getMessage();
				LogWriter.instance.appendException(e);
			}
		}
		
		obj = new AvayaCallOriginator(fromNumber, address, terminal, option);
		obj.Reason = exceptionMsg;
		
		return obj;
	}

	@Override
	public CallReceiver GetCallReceiver(String toNumber) {
		String toNum = toNumber;
		String cuID = "";
		String fac = "";
		String jobID = "";
		String timeOut = "";
		String cuName = "";
		String deliminator = ";";

		if(toNumber != null && toNumber.isEmpty() == false) {
			String[] split = toNumber.split(deliminator);
			if(split != null && split.length > 1) {
				toNum = split[0];
				cuID = split[1];
				
				if(split.length > 2) {
					fac = split[2];
				}
				
				if(split.length > 3) {
					jobID = split[3];
				}
				
				if(split.length > 4) {
					timeOut = split[4];
				}
				
				if(split.length > 5) {
					cuName = split[5];
				}
			}
		
			return new AvayaCallReceiver(toNum, cuID, fac, jobID, timeOut, cuName);
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see pbx.Pbx#Login()
	 */
	@Override
	public boolean Login() {
		if(WebClient.isShutdown) {
			return false;
		}
		
		if(this.PbxInfo == null) {
			return false;
		}
		
		if(this.provider != null && this.provider.getState() == 0) {
			LogWriter.instance.appendInfo("Already logged in: " + this.PbxInfo.toString(), this.pbxID);
			
			return true;
		}
		
		LogWriter.instance.appendInfo("Connecting to AES...", this.pbxID);

		try {
			String serviceName = this.PbxInfo.ServiceName;
			String username = this.PbxInfo.Username;
			String password = this.PbxInfo.Password;
			String url = this.PbxInfo.Url;
			String port = this.PbxInfo.Port;
			String providerString = serviceName + ";loginID=" + username + ";passwd=" + password
					+ ";servers=" + url + ":" + port;
			// create JtapiPeer
			JtapiPeer jtapiPeer = JtapiPeerFactory.getJtapiPeer("com.avaya.jtapi.tsapi.TsapiPeer");
			
			this.serviceName = serviceName;
			this.url = url;
			this.port = port;

			// create Provider
			provider = jtapiPeer.getProvider(providerString);
			LogWriter.instance.appendInfo("Provider created successfully.", this.pbxID);
			LogWriter.instance.appendInfo("Waiting for the provider to initialize...", this.pbxID);

			// add a ProviderListener to the Provider to be notified when it is in service
			provider.addProviderListener(this);
			
			retryCount = 0;

			// wait to be notified when the Provider is in service --
			// corresponding notify is in the providerChangedEvent() method
			synchronized (this) {
				wait();
			}
			
			LogWriter.instance.appendInfo("Provider is in service.", this.pbxID);
			LogWriter.instance.appendInfo("CALL.IDLE : " + Call.IDLE, this.pbxID);
			LogWriter.instance.appendInfo("CALL.ACTIVE : " + Call.ACTIVE, this.pbxID);
			LogWriter.instance.appendInfo("CALL.INVALID : " + Call.INVALID, this.pbxID);
			
			EventsReceived evt = new EventsReceived("Initialized", "");
			evt.Exten = "";
			evt.Response = "success";
			evt.Message = "success";
			evt.pbxid = this.pbxID;
			String msg = evt.ToJson();
			network.SocketHandler.notifyAllDailers(msg);
			
			sendAESConnectedEvent();
			
			ACDManager.GetInstance().MonitorAcd(provider);		
			
			return true;
		} catch (Exception e) {
			if(!WebClient.isShutdown) {
				LogWriter.instance.appendInfo("Failed to connect to AES: " + e.getMessage(), this.pbxID);
				
				retryCount++;
				if(retryCount > 3) {
					LogWriter.instance.appendInfo("Will not reconnect to AES", this.pbxID);
					sendAESDownEvent();
				}
				else {
					sendAESDiscEvent();
					int waitTime = AppProperties.instance.avaya_reconnect_waittime;
					LogWriter.instance.appendInfo("Try to connect to AES again in " + waitTime + " seconds.", this.pbxID);
					
					try {
						for(int i = 0; i < waitTime; i++) {
							if(WebClient.isShutdown) { 
								return false;
							}
							
							Thread.sleep(1000);
						}
					} catch (InterruptedException e1) {
						LogWriter.instance.appendException(e1);
					}
					
					// retry
					return Login();
				}
			}//end if
		}
		
		return false;
	}

	@Override
	public boolean Logout() {
		if(this.provider != null) {
			try {
				ACDManager.GetInstance().UnmonitorAcd(provider);

				this.provider.removeProviderListener(this);

				this.provider.shutdown();
				
				LogWriter.instance.appendInfo(
						"Shutdown AvayaPbx: url=" + this.url +
						", port=" + this.port +
						", serviceName=" + this.serviceName, this.pbxID);
				
				return true;
			} catch(Exception e) {
				LogWriter.instance.appendException(e);
			}
		}
		
		return false;
	}
	
	public void closeConnection() {
		if (pbxWebClient != null)
		pbxWebClient.Close();
	}

	@Override
	public boolean Monitor(CallOriginator originator, CallReceiver receiver) {
		if(AppProperties.instance.listenPort != 0) {
			LogWriter.instance.appendInfo("Server mode does not login agents. ext=" + originator.FromNumber, this.pbxID);
			return true;
		}
		
		if(originator == null || (originator instanceof AvayaCallOriginator) == false) {
			LogWriter.instance.appendInfo("Monitor failed: invalid input", this.pbxID);
			return false;	
		}
		
		boolean isSuccess = false;
		int result = 0;
		AvayaCallOriginator avayaCallOriginator = (AvayaCallOriginator)originator;
		
		if(avayaCallOriginator != null && avayaCallOriginator.Terminal != null) {
			try {
		    	if( ACDManager.GetInstance().IsLoggedIntoPBX(avayaCallOriginator.FromNumber, provider) ) {
					LogWriter.instance.appendInfo("Agent is already logged into PBX. ext=" + avayaCallOriginator.FromNumber, this.pbxID);
		    	}
		    	
		    	if( ACDManager.GetInstance().GetAgent(avayaCallOriginator.FromNumber) != null ) {
					LogWriter.instance.appendInfo("Warning: Agent is already on agent list. ext=" + avayaCallOriginator.FromNumber, this.pbxID);
		    	}
		    					
				String acdAccount = originator.Option;
				String deliminator = ";";
				
				if(originator.Option != null
						&& originator.Option.isEmpty() == false
						&& originator.Option.contains(deliminator)) {
					String[] split = originator.Option.split(deliminator);
					
					if(split != null && split.length >= 2) {
						acdAccount = split[0];
						originator.AgentID = split[1];
						
						if(split.length >= 3) {
							originator.AgentName = split[2];
							
						}
					}
				}
				
				if(acdAccount != null) {
					String[] split = acdAccount.split(",");
					if(split != null && split.length >= 2) {
						String acdUsername = split[0];
						String acdPassword = split[1];
						originator.AcdUsername = acdUsername;
						String[] acdNameList = ACDManager.GetInstance().GetAcdNamesList();
						String duplicateOption = "0";
						if (split.length >=3)
							duplicateOption = split[2].trim();
						
						if(acdNameList != null && acdNameList.length > 0) {
							for(String acdName : acdNameList) {
								result = ACDManager.GetInstance().LoginAgent(
									provider, acdName, avayaCallOriginator.FromNumber,
									acdUsername, acdPassword, duplicateOption);
							
								if(result == 1) {
									ACDManager.GetInstance().AddMonitoredAgent(new AgentModel(originator));
									isSuccess = true;
								}
							}//end foreach
						} else {
							LogWriter.instance.appendInfo("No ACD but login.", this.pbxID);
							//pass the empty string indicates a null ACD address 09-27-16
							result = ACDManager.GetInstance().LoginAgent(
									provider, "", avayaCallOriginator.FromNumber,
									acdUsername, acdPassword, duplicateOption);
							
						    if(result == 1) {
								ACDManager.GetInstance().AddMonitoredAgent(new AgentModel(originator));
								isSuccess = true;
							}
						    else if (result == 2){
						    	LogWriter.instance.appendInfo("Duplicate login warning sent.", this.pbxID);
						    	isSuccess = true;
						    }
						}
					}
				}
								
				// add agent listener				
				if(isSuccess && result == 1) {
					CallListener[] callListeners = avayaCallOriginator.Terminal.getCallListeners();
					
					if(callListeners != null && callListeners.length > 0) {
						LogWriter.instance.appendInfo("Warning: Listener already exists. ext=" + avayaCallOriginator.FromNumber, this.pbxID);				
					}
					else {
						AvayaCallControlTerminalConnectionListener listener = new AvayaCallControlTerminalConnectionListener();
						avayaCallOriginator.Terminal.addCallListener(listener);	
					}
				}				
			} catch (Exception e) {
				LogWriter.instance.appendException(e);
			}
		}
		
		LogWriter.instance.appendInfo("Monitor " + ((isSuccess) ? "success" : "failed")
				+ ": "+  originator.toString(), this.pbxID);
		
		return isSuccess;		
	}
	
	@Override
	public boolean Unmonitor(CallOriginator originator) {
		if(originator == null || (originator instanceof AvayaCallOriginator) == false) {
			LogWriter.instance.appendInfo("Unmonitor failed: invalid input", this.pbxID);
			return false;	
		}
		
    	if( ACDManager.GetInstance().GetAgent(originator.FromNumber) == null ) {
    		return true; 	//agent already logged out, silent success
    	}		
		
		boolean isSuccess = ACDManager.GetInstance().LogoutAgent(originator.FromNumber, provider);
		
		AvayaCallOriginator avayaCallOriginator = (AvayaCallOriginator)originator;
		
		if(isSuccess && avayaCallOriginator != null && avayaCallOriginator.Terminal != null) {
			try {
				CallListener[] callListeners = avayaCallOriginator.Terminal.getCallListeners();
				
				if(callListeners != null && callListeners.length > 0) {
					for(CallListener item : callListeners) {
						avayaCallOriginator.Terminal.removeCallListener(item);		
					}
				}

				LogWriter.instance.appendInfo("Unmonitor success: " + originator.FromNumber, this.pbxID);
				
				// Remove agent from list
				AgentModel agentModel = ACDManager.GetInstance().GetMonitoredAgent(originator.FromNumber);
				ACDManager.GetInstance().RemoveMonitoredAgent(agentModel);				
			} catch (Exception e) {
				LogWriter.instance.appendException(e);
			}
		}
		
		LogWriter.instance.appendInfo("Unmonitor " + ((isSuccess) ? "success" : "failed")
				+ ": " + originator.FromNumber, this.pbxID);
		
		return isSuccess;		
	}
	
	@Override
	public boolean SetAgentReady(CallOriginator originator) {
		if(originator == null || originator.FromNumber == null) {
			LogWriter.instance.appendInfo("SetAgentReady failed: invalid input", this.pbxID);
			return false;	
		}
		
		boolean isSuccessful = ACDManager.GetInstance().SetAgentState(originator.FromNumber, Agent.READY, 0);
		
		if(isSuccessful) {
			LogWriter.instance.appendInfo("SetAgentReady success: " + originator.toString(), this.pbxID);
		}
		else {
			LogWriter.instance.appendInfo("SetAgentReady failed: " + originator.toString(), this.pbxID);	
		}
		
		return isSuccessful;		
	}
	
	@Override
	public boolean SetAgentNotReady(CallOriginator originator) {
		if(originator == null || originator.FromNumber == null) {
			LogWriter.instance.appendInfo("SetAgentNotReady failed: invalid input", this.pbxID);
			return false;	
		}
		
		int reasonCode = 0;
		
		try {
			reasonCode = Integer.parseInt(originator.Option);
		}
		catch(Exception e) {}
		
		boolean isSuccessful = ACDManager.GetInstance().SetAgentState(originator.FromNumber, Agent.NOT_READY, reasonCode);
		
		if(isSuccessful) {
			LogWriter.instance.appendInfo("SetAgentNotReady success: " + originator.toString(), this.pbxID);
		}
		else {
			LogWriter.instance.appendInfo("SetAgentNotReady failed: " + originator.toString(), this.pbxID);	
		}
		
		return isSuccessful;		
	}
	
	@Override
	public boolean SetAgentPostwork(CallOriginator originator) {
		if(originator == null || originator.FromNumber == null) {
			LogWriter.instance.appendInfo("SetAgentPostwork failed: invalid input", this.pbxID);
			return false;	
		}
		
		boolean isSuccessful = ACDManager.GetInstance().SetAgentState(originator.FromNumber, Agent.WORK_NOT_READY, 0);
		
		if(isSuccessful) {
			LogWriter.instance.appendInfo("SetAgentPostwork success: " + originator.toString(), this.pbxID);
		}
		else {
			LogWriter.instance.appendInfo("SetAgentPostwork failed: " + originator.toString(), this.pbxID);	
		}
		
		return isSuccessful;		
	}
	
	@Override
	public boolean Dial(CallOriginator originator, CallReceiver receiver) {
		if(originator == null || receiver == null
				|| (originator instanceof AvayaCallOriginator) == false) {
			LogWriter.instance.appendInfo("Dial failed: invalid input", this.pbxID);
			return false;
		}
		
		String errMsg = "";
		
		try {
			AvayaCallOriginator avayaOriginator = (AvayaCallOriginator)originator;			
			Address address = avayaOriginator.Address;
			Terminal terminal = avayaOriginator.Terminal;
			
			receiver.callIndex = masterCallIndex;
			AvayaCallControlConnectionListener callListener = new AvayaCallControlConnectionListener(masterCallIndex++, receiver.ToNumber, address, "notused", "notused", this.pbxID);
			
			Call call = this.provider.createCall();
			call.addCallListener(callListener);
			
			callListener.setSystemCallID(call);
			
			call.connect(terminal, address, receiver.ToNumber);
			
			LogWriter.instance.appendInfo("Dial success: " + originator.toString() + receiver.toString(), this.pbxID);
			
			return true;
		} catch(TsapiInvalidArgumentException e) {
			errMsg = e.getMessage();
		} catch (Exception e) {
			errMsg = e.getMessage();
			LogWriter.instance.appendException(e);
		}
		
		LogWriter.instance.appendInfo("Dial failed: " + originator.toString() + receiver.toString() + ", errMsg=" + errMsg, this.pbxID);
		return false;
	}
	
	public int ToNumberOfRings(String timeStr) {
		int numrings = 0;
		try {
			int timeint = Integer.parseInt(timeStr);
			numrings = timeint / 3;			
		}
		catch (Exception e) {		
		}
		
		if (numrings < 2)
			numrings = 2;
		if (numrings > 15)
			numrings = 15;
		
		return numrings;
	}
	
	@Override
	public boolean DialPredictive(CallOriginator originator, CallReceiver receiver) {
		AvayaCallOriginator avayaOriginator = (AvayaCallOriginator)originator;			
		LucentAddress address = (LucentAddress) avayaOriginator.Address;
		String callerNumber = avayaOriginator.FromNumber;
		String calledNumber = receiver.ToNumber;
		String customerID = receiver.ToCUNumber;
		String fac = receiver.fac;
		String jobID = receiver.jobID;
		String customerName = receiver.ToCUName;
		if (callerNumber == null || callerNumber.isEmpty() || calledNumber == null || calledNumber.isEmpty()) {
			return false;
		}
		
		String uuiMsg = "dialer:" + customerID + ";p:" + calledNumber + ";j:" + jobID +";c:"+ customerName;
		int numOfRings = this.ToNumberOfRings(receiver.timeOut);
		LucentV10Call call = null;

		try {
			LogWriter.instance.appendInfo(String.format(
					">>> MakePredictiveCall initiate: callerNumber=%s, calledNumber=%s, uui=%s, numOfRings=%d",
					callerNumber, calledNumber, uuiMsg, numOfRings), this.pbxID);

			if (address == null && AppProperties.instance.isOutgoingSimulation == false) {
				LogWriter.instance.appendInfo(String.format(
						"MakePredictiveCall failed: cause=nulladdress, callerNumber=%s, calledNumber=%s, uui=%s, numOfRings=%d",
						callerNumber, calledNumber, uuiMsg, numOfRings), this.pbxID);

				if(originator.Reason.isEmpty() == false)
					receiver.Reason = originator.Reason;
				else 
					receiver.Reason = "nulladdress";
					
				return false;
			}

			receiver.callIndex = masterCallIndex;
			AvayaCallControlConnectionListener callListener = new AvayaCallControlConnectionListener(masterCallIndex++, calledNumber, address, uuiMsg, callerNumber, this.pbxID);
			
			if( AppProperties.instance.isOutgoingSimulation == true) {
				int resultCode = -1;
				int ringingTime = 10;
				int talkingTime = 10;
				
				if( receiver.setting != null) {
					String[] parts = receiver.setting.split(",");
					
					if(parts.length > 2) {
						ringingTime = Integer.valueOf(parts[0]);
						talkingTime = Integer.valueOf(parts[1]);
						resultCode = Integer.valueOf(parts[2]);
					}
				}
					
				callListener.setSimData(resultCode);
				synchronized(simCallList) {
					simCallList.add(new SimCall(callListener, ringingTime, talkingTime));
				}
				
				return true;
			}
			
			UserToUserInfo uui = null;
			// String utfbytes = new String(uuiMsg.getBytes(),"SJIS");

			if (AppProperties.instance.isNewUUI) {
				uui = new UserToUserInfo(("~.\u0010\u000C\u0000\u0000" + customerID + "\u001F" + uuiMsg).getBytes(),
						(short) 0);
			} else {
				uui = new UserToUserInfo(uuiMsg);
			}

						 
			call = (LucentV10Call) provider.createCall();
			call.addCallListener(callListener);	

			callListener.setSystemCallID(call);
			dialedCallList.add(call);	//So we don't lose the reference

			if (fac != null)  //Add the feature access code if available
				calledNumber = fac + calledNumber;

			Date now = new Date();
			long nowTime = now.getTime();

			LogWriter.instance.appendInfo(String.format(
					"connectPredictive START: callerNumber=%s, calledNumber=%s, uuiMsg=%s",
					callerNumber, calledNumber, uuiMsg), this.pbxID);
			
			Connection[] conns = call.connectPredictive(null, address, calledNumber, LucentConnection.CONNECTED,
					numOfRings, LucentV10Call.ANSWERING_TREATMENT_NONE, LucentV10Call.ENDPOINT_ANY, false, uui);			

			now = new Date();
			long endTime = now.getTime();

			LogWriter.instance.appendInfo(String.format(
					"connectPredictive END: elapsedTime=%d callerNumber=%s, calledNumber=%s, uuiMsg=%s",
					endTime - nowTime, callerNumber, calledNumber, uuiMsg), this.pbxID);
			
			if (conns == null || conns.length == 0) {
				LogWriter.instance.appendInfo(String.format(
						"MakePredictiveCall failed: cause=connect failed, callerNumber=%s, calledNumber=%s, uuiMsg=%s",
						callerNumber, calledNumber, uuiMsg), this.pbxID);

				call.removeCallListener(callListener);
				removeCallObj(call);
				
				receiver.Reason = "connect failed";
				
				/*
				String response = String.format(
						 "Event: OriginateResponse\r\nResponse: %s\r\nChannel: %s\r\nContext: %s\r\nExten: %s\r\nReason: %d\r\nMessage: %s"
						 , "failed", callerNumber,
						 "connectPredictive failed",
						 calledNumber, 60, customerID);
						 
						requestHandler.sendBackToClient(response);
				*/		 
				//AvayaManager.instance.removeCustomerNumber(customerID);
				//AvayaManager.instance.removeCallObj(customerID);

				return false;
			}

			//RunCheckTimeoutThread(call, customerID, calledNumber, callerNumber, timeout);

			return true;
		} catch (TsapiUnableToSendException ue) {
			LogWriter.instance.appendError("AES connection loss");
			receiver.Reason = ue.getMessage();
		} catch (TsapiPlatformException pe) {
			receiver.Reason = pe.getMessage();

			if (pe.getMessage() == "Could not meet post-conditions of connectPredictive()") {
				LogWriter.instance.appendInfo("Could not meet post-conditions of connectPredictive()", this.pbxID);

				//AddCnmp(customerID);

				//RunCheckTimeoutThread(call, customerID, calledNumber, callerNumber, timeout, requestHandler);
				
				//if(AppProperties.instance.pCondReconnect) {
				//	AvayaManager.instance.connect(true);
					
	/*				Event: OriginateResponse
					Response: Failed
					Channel:  18125
					Context:  No answer hangup: customerID=653, number=*0510508067254856, timeout=28 secs, state=51
					Exten:  *0510508067254856
					Reason: -2
					Message: 653*/
					
				//	String response = String.format(
				//			"Event: OriginateResponse\r\nResponse: Failed\r\nChannel: %s\r\nContext: No answer hangup\r\nExten: %s\r\nReason: -2\r\nMessage: %s",
				//			callerNumber, calledNumber, customerID);
					
				//	requestHandler.sendBackToClient(response);
				//}
				
				LogWriter.instance.appendInfo("MakePredictiveCall return false", this.pbxID);

				return false;
			} else {
				LogWriter.instance.appendException(pe);
			}
		} catch (InvalidPartyException excp) {
			LogWriter.instance.appendInfo(String.format(
					"MakePredictiveCall failed: cause=Originating or destination party is invalid, callerNumber=%s, calledNumber=%s, customerID=%s",
					callerNumber, calledNumber, customerID), this.pbxID);

			receiver.Reason = excp.getMessage();

			//ReturnOriginateFailed(requestHandler, callerNumber, calledNumber, customerID);
		} catch (Exception e) {
			LogWriter.instance.appendException(e);
			receiver.Reason = e.getMessage();
		}

		//ReturnOriginateFailed(requestHandler, callerNumber, calledNumber, customerID);

		LogWriter.instance.appendInfo("MakePredictiveCall return false", this.pbxID);

		return false;
	}
	
	@Override
	public boolean Drop(CallOriginator originator) {
		if(originator == null || (originator instanceof AvayaCallOriginator) == false) {
			LogWriter.instance.appendInfo("Drop failed: invalid input", this.pbxID);
			return false;
		}
		
		try {
			AvayaCallOriginator avayaOriginator = (AvayaCallOriginator)originator;			
			Terminal terminal = avayaOriginator.Terminal;
			TerminalConnection[] terminalConnections = (terminal != null) ? terminal.getTerminalConnections() : null;
			TerminalConnection terminalConnection = (terminalConnections != null && terminalConnections.length > 0) ?
					terminalConnections[0] : null;

			if (terminalConnection != null) {
				Connection conn = terminalConnection.getConnection();
				if (conn.getState()  == Connection.CONNECTED){
					conn.disconnect();
					LogWriter.instance.appendInfo("Drop success: " + originator.toString(), this.pbxID);
				}
				else if (conn.getState()  == Connection.ALERTING){
					CallControlCall call = (CallControlCall) conn.getCall(); 
				    call.drop();
				    LogWriter.instance.appendInfo("Reject success: " + originator.toString(), this.pbxID);
				}
				return true;				
			}
		} catch (Exception e) {
			LogWriter.instance.appendException(e);
		}
		
		LogWriter.instance.appendInfo("Drop failed: " + originator.toString(), this.pbxID);
		return false;
	}
	
	public boolean Answer(CallOriginator originator) {
		if(originator == null || (originator instanceof AvayaCallOriginator) == false) {
			LogWriter.instance.appendInfo("Answer failed: invalid input", this.pbxID);
			return false;
		}
		
		try {
			AvayaCallOriginator avayaOriginator = (AvayaCallOriginator)originator;			
			Terminal terminal = avayaOriginator.Terminal;
			
			if( terminal != null) {
				TerminalConnection[] terms = terminal.getTerminalConnections();
				
				if(terms != null) {
					TerminalConnection terminalConnection = terms[0];
		
					if (terminalConnection != null) {
						terminalConnection.answer();
						
						LogWriter.instance.appendInfo("Answer success: " + originator.toString(), this.pbxID);
						return true;				
					}
				}
				else {
					LogWriter.instance.appendInfo("Answer: No connections on " + originator.toString(), this.pbxID);
				}
			}
		} catch (Exception ex) {
			LogWriter.instance.appendException(ex);
		}
		
		LogWriter.instance.appendInfo("Answer failed: " + originator.toString(), this.pbxID);
		
		return false;
	}
	
	@Override
	public boolean Hold(CallOriginator originator) {
		if(originator == null || (originator instanceof AvayaCallOriginator) == false) {
			LogWriter.instance.appendInfo("Hold failed: invalid input", this.pbxID);
			return false;
		}		
		
		try {
			AvayaCallOriginator avayaOriginator = (AvayaCallOriginator)originator;
			CallControlTerminalConnection terminalConnection = avayaOriginator.TerminalConnection;
			
			if (terminalConnection.getCallControlState() == CallControlTerminalConnection.HELD){
				LogWriter.instance.appendInfo("Already on Hold: FromNumber=" + avayaOriginator.FromNumber, this.pbxID);
				return true;	
			}
				
			
			if (terminalConnection != null) {
				terminalConnection.hold();
				
				LogWriter.instance.appendInfo("Hold success: FromNumber=" + avayaOriginator.FromNumber, this.pbxID);
				return true;				
			}
		} catch (Exception e) {
			LogWriter.instance.appendError("Hold failed: " + originator.toString());
			LogWriter.instance.appendException(e);
		}		
		
		return false;
	}

	@Override
	public boolean Unhold(CallOriginator originator) {
		if(originator == null || (originator instanceof AvayaCallOriginator) == false) {
			LogWriter.instance.appendInfo("Unhold failed: invalid input", this.pbxID);
			return false;
		}		
		
		try {
			AvayaCallOriginator avayaOriginator = (AvayaCallOriginator)originator;
			CallControlTerminalConnection terminalConnection = avayaOriginator.TerminalConnection;
			
			if (terminalConnection != null) {
				terminalConnection.unhold();
				
				LogWriter.instance.appendInfo("Unhold success: FromNumber=" + avayaOriginator.FromNumber, this.pbxID);
				return true;				
			}
		} catch (Exception e) {
			LogWriter.instance.appendError("Unhold failed: " + originator.toString());
			LogWriter.instance.appendException(e);
		}		
		
		return false;
	}
	
	@Override
	public boolean TransferSetup(CallOriginator originator, CallReceiver receiver) {
		if(originator == null || receiver == null) {
			LogWriter.instance.appendInfo("TransferSetup failed: invalid input", this.pbxID);
			return false;
		}
		
		boolean isSuccess = this.Hold(originator);
		
		if(isSuccess) {
			
			EventsReceived evt = new EventsReceived("onhold", "whoeveris");
			evt.Response = "Hold customer in transfersetup ";
			evt.Message = "success";
			String msg = evt.ToJson();
			network.SocketHandler.notifyAllDailers(msg);
			
			isSuccess = this.Dial(originator, receiver);
		}
		
		LogWriter.instance.appendInfo(String.format("TransferSetup %s",
				(isSuccess) ? "success" : "failed"), this.pbxID);
		
		return isSuccess;
	}

	@Override
	public boolean TransferComplete(CallOriginator originator) {
		if(originator == null) {
			LogWriter.instance.appendInfo("TransferComplete failed: invalid input", this.pbxID);
			return false;
		}
		
		try {
			/*Call[] calls = this.provider.getCalls();
			
			LogWriter.instance.appendInfo("TransferComplete debug: how manycalls: " + calls.length, this.pbxID);
			LogWriter.instance.appendInfo("TransferComplete debug: the first item: " + (calls[0] == null? "null": calls[0].toString()), this.pbxID);
			LogWriter.instance.appendInfo("TransferComplete debug: the second item: " + (calls[1] == null? "null": calls[1].toString()), this.pbxID);
			Call call1 = null;
			Call call2 = null;
			
			if(calls != null && calls.length > 0) {
				for(Call item : calls) {
					try {						
						String connAdd = item.getConnections()[0].getAddress().getName();
						if (originator.FromNumber.equals(connAdd) == false) {
							continue;
						}

						if(call1 != null && call2 != null) {
							break;
						} else if(call1 == null) {
							call1 = item;
						} else {
							call2 = item;
						}

						continue;
					} catch(Exception e) {
						LogWriter.instance.appendException(e);
					}
				}//end for each
			}
			*/
			CallControlCall call2 = (CallControlCall)channelInfo[1].getLucentV7CallInfo();
		    Call call1 = (Call)channelInfo[0].getLucentV7CallInfo();

			LogWriter.instance.appendInfo("TransferComplete debug: call1: " + (call1 == null ? "null": call1.toString()), this.pbxID);
			LogWriter.instance.appendInfo("TransferComplete debug: call1 state: " + call1.getState(), this.pbxID);
			LogWriter.instance.appendInfo("TransferComplete debug: call2: " + (call2 == null ? "null": call2.toString()), this.pbxID);
			LogWriter.instance.appendInfo("TransferComplete debug: call2 state: " + call2.getState(), this.pbxID);

		    if (call1==null)
		    {
				LogWriter.instance.appendError("TransferComplete failed, call1 channel lost: " + originator.toString());
		    }
		    else if (call2==null)
		    {
				LogWriter.instance.appendError("TransferComplete failed, call2 channel lost: " + originator.toString());
		    }
		    else
		    {
	            call2.transfer(call1);
				//((CallControlCall) call1).transfer(call2);
	
				LogWriter.instance.appendInfo("TransferComplete success: " + originator.toString(), this.pbxID);
				
				EventsReceived evt = new EventsReceived("incoming_call", originator.FromNumber);
				evt.Exten = channelInfo[0].getCallerID();
				evt.Message = channelInfo[0].getCalledID();
				evt.Response = "transfercomplete";
				String msg = evt.ToJson();
				LogWriter.instance.appendInfo(msg, this.pbxID);
				//network.SocketHandler.notifyAllDailers(msg);
				
				return true;
		    }
		} catch (Exception e) {
			LogWriter.instance.appendError("TransferComplete failed: " + originator.toString());
			LogWriter.instance.appendException(e);
		}
		
		return false;
	}
	
	@Override
	public boolean TransferCancel(CallOriginator originator) {
		if(originator == null) {
			LogWriter.instance.appendInfo("TransferCancel failed: invalid input", this.pbxID);
			return false;
		}
		
		try {
			/*Call[] calls = this.provider.getCalls();			
			Call talkingCall = null;
			
			if(calls != null)
			for(Call item : calls) {
				try {
					String connAdd = item.getConnections()[0].getAddress().getName();
					if (originator.FromNumber.equals(connAdd) == false) {
						continue;
					}
					
					Connection[] conns = item.getConnections();
					
					if(conns != null)
					for(Connection conn : conns) {
						TerminalConnection[] termConns = conn.getTerminalConnections();
						
						if(termConns != null)
						for(TerminalConnection tConn : termConns) {						
							int state = ((CallControlTerminalConnection) tConn).getCallControlState();
							
							if(state == CallControlTerminalConnection.TALKING) {
								talkingCall = item;
								break;
							}
						}
					}
				} catch(Exception e) {
					LogWriter.instance.appendException(e);
				}
			}//end for each 	
			
			boolean isSuccess = false;
			if(talkingCall != null) {
				talkingCall.getConnections()[0].disconnect();
				
				isSuccess = this.Unhold(originator);
			} */
			
			boolean isSuccess = false;
			if (!channelInfo[1].isActive())
            {
                LogWriter.instance.appendInfo("TransferCancel error: can not transferCancel", this.pbxID);
                return false;
            }
            
			//this.Drop(originator);
            
			try {
				AvayaCallOriginator avayaOriginator = (AvayaCallOriginator)originator;			
				Terminal terminal = avayaOriginator.Terminal;
				TerminalConnection[] terminalConnections = (terminal != null) ? terminal.getTerminalConnections() : null;
				TerminalConnection terminalConnection = (terminalConnections != null && terminalConnections.length > 0) ?
						terminalConnections[terminalConnections.length - 1] : null;

				if (terminalConnection != null) {
					terminalConnection.getConnection().disconnect();
					LogWriter.instance.appendInfo("Drop success: " + originator.toString(), this.pbxID);

					if(channelInfo[0].isActive()){
					    this.Unhold(originator);
					    isSuccess = true;
						LogWriter.instance.appendInfo("Unhold success: " + originator.toString(), this.pbxID);
					}
				}
			} 
			catch (Exception e) {
				LogWriter.instance.appendException(e);
			}

            LogWriter.instance.appendInfo(String.format("TransferCancel %s: %s",
					(isSuccess) ? "success" : "failed", originator.toString()), this.pbxID);
			
			return isSuccess;
		} catch (Exception e) {
			LogWriter.instance.appendError("TransferCancel failed: " + originator.toString());
			LogWriter.instance.appendException(e);
		}
		
		return false;
	}
	
	@Override
	public void providerEventTransmissionEnded(ProviderEvent arg0) {
		LogWriter.instance.appendInfo("providerEventTransmissionEnded", this.pbxID);
		
	}

	@Override
	public void providerInService(ProviderEvent arg0) {
		LogWriter.instance.appendInfo("providerInService", this.pbxID);
		
		synchronized (this) {
			notify(); // registerRouteCallback() is waiting on this
						// event
		}		
	}

	@Override
	public void providerOutOfService(ProviderEvent arg0) {
		LogWriter.instance.appendInfo("providerOutOfService", this.pbxID);
		
		//Login();
		
	}

	@Override
	public void providerShutdown(ProviderEvent arg0) {
		LogWriter.instance.appendInfo("providerShutdown", this.pbxID);
		
		if(!WebClient.isShutdown) {
			LogWriter.instance.appendInfo("Connection to AES down", this.pbxID);
			
			retryCount++;
			if(retryCount > 3) {
				LogWriter.instance.appendInfo("Will not reconnect to AES", this.pbxID);
				sendAESDownEvent();
			}
			else {
				sendAESDiscEvent();
				int waitTime = AppProperties.instance.avaya_reconnect_waittime;
				LogWriter.instance.appendInfo("Try to connect to AES again in " + waitTime + " seconds.", this.pbxID);
				
				try {
					for(int i = 0; i < waitTime; i++) {
						if(WebClient.isShutdown) { 
							return;
						}
						
						Thread.sleep(1000);
					}
				} catch (InterruptedException e1) {
					LogWriter.instance.appendException(e1);
				}
				
				// retry
				Login();
			}
		}//end if
	}

	public void sendAESDownEvent() {
		pbxIsActive = false;
		
		EventsReceived evt = new EventsReceived("avayaaesdown", "");
		evt.pbxid = this.pbxID;
		String outmsg = evt.ToJson();
		// LogWriter.instance.appendInfo(String.format("Response: %s", outmsg));
		network.SocketHandler.notifyAllDailers(outmsg);		
	}	

	public void sendAESDiscEvent() {
		pbxIsActive = false;

		EventsReceived evt = new EventsReceived("avayaaesdisconnect", "");
		evt.pbxid = this.pbxID;
		String outmsg = evt.ToJson();
		// LogWriter.instance.appendInfo(String.format("Response: %s", outmsg));
		network.SocketHandler.notifyAllDailers(outmsg);		
	}	

	public void sendAESConnectedEvent() {
		pbxIsActive = true;

		EventsReceived evt = new EventsReceived("avayaaesconnect", "");
		evt.pbxid = this.pbxID;
		String outmsg = evt.ToJson();
		// LogWriter.instance.appendInfo(String.format("Response: %s", outmsg));
		network.SocketHandler.notifyAllDailers(outmsg);		
	}	
}
