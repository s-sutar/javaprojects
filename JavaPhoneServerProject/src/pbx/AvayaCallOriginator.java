package pbx;

import javax.telephony.Address;
import javax.telephony.Terminal;
import javax.telephony.callcontrol.CallControlTerminalConnection;

public class AvayaCallOriginator extends CallOriginator {
	
	public Address Address = null;
	public Terminal Terminal = null;
	public CallControlTerminalConnection TerminalConnection = null;
	
	public AvayaCallOriginator(String fromNumber, Address address, Terminal terminal,
			String option) {
		this.FromNumber = fromNumber;
		this.Address = address;
		this.Terminal = terminal;
		this.Option = option;
		
		if(this.Terminal != null
				&& this.Terminal.getTerminalConnections() != null
				&& this.Terminal.getTerminalConnections().length > 0) {
			this.TerminalConnection = (CallControlTerminalConnection) this.Terminal.getTerminalConnections()[0];
		}
	}

}
