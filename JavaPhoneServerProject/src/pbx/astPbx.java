package pbx;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.NoRouteToHostException;
import java.net.URL;
import java.net.URLConnection;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import commands.astPredictiveDialCommand;
import core.LogWriter;
import core.RecordInfo;
import mit.mitAvayaController;
import network.WebClient;
import core.AppProperties;
import core.AsteriskChannelPeer;
import core.EventsReceived;

public class astPbx extends Pbx {
	static {
		TrustManager[] trustAllCertificates = new TrustManager[] { new X509TrustManager() {
			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return null; // Not relevant.
			}

			@Override
			public void checkClientTrusted(X509Certificate[] certs, String authType) {
				// Do nothing. Just allow them all.
			}

			@Override
			public void checkServerTrusted(X509Certificate[] certs, String authType) {
				// Do nothing. Just allow them all.
			}
		} };

		HostnameVerifier trustAllHostnames = new HostnameVerifier() {
			@Override
			public boolean verify(String hostname, SSLSession session) {
				return true; // Just allow them all.
			}
		};

		try {
			// System.setProperty("jsse.enableSNIExtension", "false");
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCertificates, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier(trustAllHostnames);
		} catch (GeneralSecurityException e) {
			throw new ExceptionInInitializerError(e);
		}
	}

	public static ConcurrentHashMap<String, astCallInfo> channelList = new ConcurrentHashMap<String, astCallInfo>();
	public static ConcurrentHashMap<String, astConference> confList = new ConcurrentHashMap<String, astConference>();

	public String stasisApp;
	public String trunkName;

	public astPbx(pbx.PbxInfo pbxInfo) {
		super(pbxInfo);

		this.Type = PbxType.MPBX;

		this.stasisApp = pbxInfo.StasisApp;
		this.trunkName = pbxInfo.TrunkName;
		this.pbxID = pbxInfo.pbxID;
		this.pbxWebClient.pbxID = pbxInfo.pbxID;

	}
	
	public void removeCallObj(Object call) {
	}
	
	public String finishInit() {
		String protocol = "ws";
		if (AppProperties.instance.pbxEnableSSL)
			protocol = "wss";

		String errorStr = pbxWebClient.Connect(protocol + "://" + PbxInfo.Url + ":" + PbxInfo.Port + "/ari/events?api_key="
				+ PbxInfo.Username + ":" + PbxInfo.Password + "&app=" + this.stasisApp, true);

		// DialPredictive(null, null);
		
		if(errorStr.isEmpty())
			pbxIsActive = true;
		
		//Test GET
//		sendToAsterisk("GET", "/ari/applications/"+ this.stasisApp, "", false, null, null);	
		
		//Filter events
		//Turn on if we upgrade to Asterisk 16
//		sendToAsterisk("PUT", "/ari/applications/"+ this.stasisApp + "/eventFilter", "{ \"allowed\": [ { \"type\": \"StasisStart\" }, { \"type\": \"StasisEnd\" } ] }", false, null, null);				
		return errorStr;
	}
	
	public void closeConnection() {
		if (pbxWebClient != null)
		pbxWebClient.Close();
	}

	@Override
	public boolean Login() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean Logout() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean Monitor(CallOriginator originator, CallReceiver receiver) {
		
			receiver = new astCallReceiver();
			receiver.ToNumber = originator.Option;
			receiver.ToCUNumber = "Snoop/" + originator.Option;


			// Make sure the agent is in a conference room
			boolean agentInConf = false;
			for (String p : channelList.keySet()) {
				astCallInfo cInfo = channelList.get(p);
				if (cInfo.ToCUNumber.equals(originator.FromNumber) && cInfo.isAgent == true) {
					agentInConf = true;
				}
			}
			
			if (!agentInConf) {
				originator.delayDial = "";
				originator.delayedMonitorDial = originator.Option;
				SetAgentReady(originator);
				agentInConf = true;
				//return true;
			}
			astCallInfo cInfo = null;
			String targetAgentChannelID = null;
			for (String p : channelList.keySet()) {
				cInfo = channelList.get(p);
				if (cInfo.AgentExtension.equals(originator.Option) && cInfo.isAgent == true) {
					targetAgentChannelID = p;
					break;
				}
			}
			
			 astPredictiveDialCommand awsReq = new astPredictiveDialCommand();
			 awsReq.spy = "both";
			 awsReq.whisper = "none";
			 awsReq.app = this.stasisApp;
			 String outStr = awsReq.ToJson();
			 
			 if(targetAgentChannelID == null || targetAgentChannelID.isEmpty()) {
				LogWriter.instance.appendInfo("Monitor: could not find target Channel ID  ");
				return false;
			 }

			 if (sendToAsterisk("POST", "/ari/channels/" + targetAgentChannelID + "/snoop", outStr, true, receiver, null, 0)) {
				 cInfo.SnoopChannel = receiver.SystemCallID;
				//Add snoop channel to the SV's bridge
				 MonitorAddChannel(originator.FromNumber, receiver.SystemCallID, true);
				 
					
				return true;
			}
			return false;
	}
	
	public synchronized boolean MonitorAddChannel(String confID, String legID, boolean legIsAlreadyID) {
		// Search for an active call
		String foundLeg = "";
		boolean isAgent = false;
		String CustomerID = null;
		if (legIsAlreadyID) {
			foundLeg = legID;
			astCallInfo cInfo = channelList.get(foundLeg);
			isAgent = cInfo.isAgent;
			cInfo.AgentExtension = confID;
		} else {
			for (String p : channelList.keySet()) {
				astCallInfo cInfo = channelList.get(p);
				if (cInfo.ToCUNumber.equals(legID)) {
					foundLeg = p;
					isAgent = cInfo.isAgent;
					cInfo.AgentExtension = confID;
					CustomerID = cInfo.ToCUNumber;
					break;
				}
			}
		}

		if (foundLeg.isEmpty()) {
			LogWriter.instance.appendInfo("MonitorAddChannel: could not find leg " + legID);
			return false;
		}

		astConference conf = null;
		if (confList.containsKey(confID) == false) {
			conf = new astConference();
			conf.name = confID;
			confList.put(confID, conf);
		} else {
			conf = confList.get(confID);
		}

		if (isAgent) {
			sendToAsterisk("DELETE", "/ari/channels/" + foundLeg + "/silence", "", false, null, null, 0);
		}

		sendToAsterisk("POST", "/ari/bridges/" + confID, "", false, null, null, 0);

		astPredictiveDialCommand awsReq = new astPredictiveDialCommand();
		awsReq.channel = foundLeg;
		String outStr = awsReq.ToJson();

		if(confID == null) {
			LogWriter.instance.appendInfo("MonitorAddChannel: could not find target Channel ID  ");
			return false;
		}
		sendToAsterisk("POST", "/ari/bridges/" + confID + "/addChannel", outStr, false, null, null, 0);

		if (conf.participants.contains(awsReq.channel) == false)
			conf.participants.add(awsReq.channel);

		return false;
	}
	
	@Override
	public boolean Unmonitor(CallOriginator originator) {
		// Drop the conference room call if they have one
		for (String p : channelList.keySet()) {
			astCallInfo chID = channelList.get(p);
			if (chID.AgentExtension != null && originator.FromNumber != null && chID.AgentExtension.equals(originator.FromNumber)) {
				originator.Option = p;
				Drop(originator);
				originator.delayedMonitorDial = "";
			}
		}

		return true;
	}

	@Override
	public boolean SetAgentReady(CallOriginator originator) {
		// astPredictiveDialCommand awsReq = new astPredictiveDialCommand();
		// awsReq.endpoint = "Local/b" + originator.FromNumber;
		// awsReq.extension = originator.FromNumber;
		astCallInfo callInfo = null;
		String agentExten = originator.FromNumber;
		for (String p : channelList.keySet()) {
			astCallInfo cInfo = channelList.get(p);

			if (cInfo.AgentExtension.equals(originator.FromNumber) && cInfo.isAgent == true) {
				callInfo = cInfo;
				break;
			}
		}

		if(callInfo != null && callInfo.isInConfRoom) {
			EventsReceived evt = new EventsReceived("ReadyEvent", callInfo.AgentExtension);
			evt.SystemCallID = originator.SystemCallID;
			evt.Exten = callInfo.AgentExtension;
			evt.pbxid = this.pbxID;
			String outmsg = evt.ToJson();
			// LogWriter.instance.appendInfo(String.format("Response: %s", outmsg));
			network.SocketHandler.notifyAllDailers(outmsg);
			return true;
		}
		
		astPredictiveDialCommand awsReq = new astPredictiveDialCommand();
		awsReq.endpoint = "SIP/" + originator.FromNumber;
		awsReq.app = this.stasisApp;
		String outStr = awsReq.ToJson();

		astCallReceiver rcv = new astCallReceiver();
		rcv.ToNumber = originator.FromNumber;
		rcv.ToCUNumber = originator.FromNumber;
		rcv.isAgent = true;
		rcv.delayDial = originator.delayDial;
		if(originator.delayedMonitorDial != null && originator.delayedMonitorDial.isEmpty() == false)
			rcv.delayMonitorDial = originator.delayedMonitorDial;

		if (sendToAsterisk("POST", "/ari/channels", outStr, true, rcv, null, 0)) {
			originator.SystemCallID = rcv.SystemCallID;

			return true;
		}

		EventsReceived evt = new EventsReceived("ConfRoomFailed", agentExten);
		evt.SystemCallID = "";
		evt.Exten = agentExten;
		evt.pbxid = this.pbxID;
		String outmsg = evt.ToJson();
		network.SocketHandler.notifyAllDailers(outmsg);

		return false;
	}

	@Override
	public boolean SetAgentNotReady(CallOriginator originator) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean SetAgentPostwork(CallOriginator originator) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean Dial(CallOriginator originator, CallReceiver receiver) {
		astPredictiveDialCommand awsReq = new astPredictiveDialCommand();
		//String toNum = receiver.ToNumber.substring(4);
		
		awsReq.endpoint = this.trunkName + receiver.ToNumber;
		awsReq.app = this.stasisApp;
		awsReq.timeout = 30;
		String outStr = awsReq.ToJson();

		// Make sure the agent is in a conference room
		boolean agentInConf = false;
		for (String p : channelList.keySet()) {
			astCallInfo cInfo = channelList.get(p);

			if (cInfo.ToCUNumber.equals(originator.FromNumber) && cInfo.isAgent == true) {
				//ConferenceAddChannel(originator.FromNumber, p, true);
				agentInConf = true;
			}
		}

		if (!agentInConf) {
			originator.delayDial = receiver.ToNumber;
			SetAgentReady(originator);
			//return true;
		}
		if (sendToAsterisk("POST", "/ari/channels/create", outStr, true, receiver, originator, 0)) {
			originator.SystemCallID = receiver.SystemCallID;

			// Add this call to the agent's conference
			ConferenceAddChannel(originator.FromNumber, originator.SystemCallID, true);

			// Actually start the dialing process
			sendToAsterisk("POST", "/ari/channels/" + originator.SystemCallID + "/dial", "", false, null, null, 0);
			return true;
		}

		return false;
	}

	public void cleanupConferences(String systemCallID) {
		// Search for an active conference
		synchronized (confList) {
			for (String p : confList.keySet()) {
				astConference conf = confList.get(p);
				if (conf.participants.contains(systemCallID)) {
					// Play silence to the other participant
					for (String pi : conf.participants) {
						if (pi.equals(systemCallID) == false) {
							//sendToAsterisk("POST", "/ari/channels/" + pi + "/silence", "", false, null, null, 0);
						}
					}

					//ConferenceStopRecording(conf.filename);

					// Clean up the conference
				//	sendToAsterisk("DELETE", "/ari/bridges/" + p, "", false, null, null, 0);
			//	confList.remove(p);
				}
			}
		}
	}

	@Override
	public boolean Drop(CallOriginator originator) {
		String targetID = originator.Option;
		boolean isAgent = true;

		if (targetID.isEmpty()) {
			for (String p : channelList.keySet()) {
				astCallInfo cInfo = channelList.get(p);
				if (cInfo.AgentExtension.equals(originator.FromNumber) && cInfo.isAgent == false) {
					targetID = p;
					isAgent = false;
				}
			}
		}

		// cleanupConferences(targetID);

		if (targetID.isEmpty()) {
			LogWriter.instance.appendInfo("Drop: could not find leg for " + originator.FromNumber);
			return false;
		}
		//temporary
		originator.SystemCallID = targetID;

		astPredictiveDialCommand awsReq = new astPredictiveDialCommand();
		awsReq.reason = "normal";
		String outStr = awsReq.ToJson();

		if(sendToAsterisk("DELETE", "/ari/channels/" + targetID, outStr, false, null, originator, 0) == false) {
			//There was an error, send the CustomerHangup event
			String CUID = "";
			String Exten = "";
			if (astPbx.channelList.containsKey(targetID)) {
				astCallInfo callInfo = astPbx.channelList.get(targetID);
				CUID = callInfo.ToCUNumber;
				//AgentChannel = callInfo.AgentExtension;
				Exten = callInfo.ToNumber;
			}
			
			EventsReceived evt = new EventsReceived("CustomerHangup", originator.FromNumber);
			evt.CustomerID = CUID;
			evt.SystemCallID = targetID;
			evt.Exten = Exten;
			evt.pbxid = this.pbxID;
			evt.Reason = "DROP FAILED: sending CustomerHangup";
			String outmsg = evt.ToJson();
			
			network.SocketHandler.notifyAllDailers(outmsg);
		}

		if (!isAgent) {
			// Play silence on the agent's extension (so they don't hear static)
			for (String p : channelList.keySet()) {
				astCallInfo cInfo = channelList.get(p);
				if (cInfo.ToNumber.equals(originator.FromNumber) && cInfo.isAgent == true) {
					targetID = p;
				}
			}

		//	sendToAsterisk("POST", "/ari/channels/" + targetID + "/silence", "", false, null, null, 0);
		}

		return true;
	}

	@Override
	public boolean Answer(CallOriginator originator) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean Hold(CallOriginator originator) {
		// Search for an active call
		String foundLeg = "";
		for (String p : channelList.keySet()) {
			astCallInfo cInfo = channelList.get(p);
			if (cInfo.AgentExtension.equals(originator.FromNumber) && cInfo.isAgent == false) {
				foundLeg = p;
				break;
			}
		}

		if (foundLeg.isEmpty()) {
			LogWriter.instance.appendInfo("Hold: could not find leg for " + originator.FromNumber);
			return false;
		}

		TransferAddChannel("", originator.Option, originator.FromNumber, foundLeg, true, false);

		// Remove them from agent's conference
		ConferenceRemoveChannel(originator.FromNumber, foundLeg, true);

		// Put them in their own conference
		sendToAsterisk("POST", "/ari/channels/" + foundLeg + "/hold", "", false, null, null, 0);

		// Play music to them
		sendToAsterisk("POST", "/ari/channels/" + foundLeg + "/moh", "", false, null, null, 0);

		return true;
	}

	@Override
	public boolean Unhold(CallOriginator originator) {
		// Search for an active call
		String foundLeg = "";
		for (String p : channelList.keySet()) {
			astCallInfo cInfo = channelList.get(p);
			if (cInfo.AgentExtension.equals(originator.FromNumber) && cInfo.isAgent == false) {
				foundLeg = p;
				break;
			}
		}

		if (foundLeg.isEmpty()) {
			LogWriter.instance.appendInfo("Unhold: could not find leg for " + originator.FromNumber);
			return false;
		}

		sendToAsterisk("DELETE", "/ari/channels/" + foundLeg + "/moh", "", false, null, null, 0);
		sendToAsterisk("DELETE", "/ari/channels/" + foundLeg + "/hold", "", false, null, null, 0);

		// Put them into the agent's conference
		TransferAddChannel("", originator.Option, originator.FromNumber, foundLeg, true, false);

		return true;
	}

	@Override
	public boolean TransferSetup(CallOriginator originator, CallReceiver receiver) {
		// Search for an active call
		String foundLeg = "";
		String CustomerID = null;
		for (String p : channelList.keySet()) {
			astCallInfo cInfo = channelList.get(p);
			if (cInfo.AgentExtension.equals(originator.FromNumber) && cInfo.isAgent == false) {
				foundLeg = p;
				CustomerID = cInfo.ToCUNumber;
				break;
			}
		}

		if (foundLeg.isEmpty()) {
			LogWriter.instance.appendInfo("Hold: could not find leg for " + originator.FromNumber);
			return false;
		}

		astPredictiveDialCommand awsReq = new astPredictiveDialCommand();
		awsReq.channel = foundLeg;
		String outStr = awsReq.ToJson();

		sendToAsterisk("POST", "/ari/bridges/" + originator.FromNumber + "/removeChannel", outStr, false, null, null, 0);

		// Put them in their own conference
		awsReq = new astPredictiveDialCommand();
		awsReq.endpoint = "Local/" + receiver.ToNumber;
		awsReq.app = this.stasisApp;
		outStr = awsReq.ToJson();

		// Make sure the agent is in a conference room
		boolean agentInConf = false;
		for (String p : channelList.keySet()) {
			astCallInfo cInfo = channelList.get(p);
			if (cInfo.ToCUNumber != null && cInfo.ToCUNumber.equals(originator.FromNumber)) {
				cInfo.TransferDesti = receiver.ToNumber;
//				ConferenceAddChannel(originator.FromNumber, p, true);
				agentInConf = true;
			}
		}

		if (!agentInConf) {
			originator.delayDial = receiver.ToNumber;
			SetAgentReady(originator);
			return true;
		}

		if (sendToAsterisk("POST", "/ari/channels/create", outStr, true, receiver, originator, 0)) {
			originator.SystemCallID = receiver.SystemCallID;

			// Add this call to the agent's conference
			TransferAddChannel(CustomerID, originator.Option, originator.FromNumber, originator.SystemCallID, true,
					true);

			// Actually start the dialing process
			sendToAsterisk("POST", "/ari/channels/" + originator.SystemCallID + "/dial", "", false, null, null, 0);
			return true;
		}
		return true;
	}

	public synchronized boolean TransferAddChannel(String CustomerID, String toNumber, String confID, String legID,
			boolean legIsAlreadyID, boolean transferbegin) {
		// Search for an active call
		String foundLeg = "";
		boolean isAgent = false;

		if (legIsAlreadyID) {
			foundLeg = legID;
			astCallInfo cInfo = channelList.get(foundLeg);
			isAgent = cInfo.isAgent;
			cInfo.AgentExtension = confID;
		} else {
			for (String p : channelList.keySet()) {
				astCallInfo cInfo = channelList.get(p);
				if (cInfo.ToCUNumber.equals(legID)) {
					foundLeg = p;
					isAgent = cInfo.isAgent;
					cInfo.AgentExtension = confID;
					CustomerID = cInfo.ToCUNumber;
					break;
				}
			}
		}

		if (foundLeg.isEmpty()) {
			LogWriter.instance.appendInfo("TransferAddChannel: could not find leg " + legID);
			return false;
		}

		astConference conf = null;
		if (confList.containsKey(confID) == false) {
			conf = new astConference();
			conf.name = confID;
			confList.put(confID, conf);
		} else {
			conf = confList.get(confID);
		}

		if (isAgent) {
			sendToAsterisk("DELETE", "/ari/channels/" + foundLeg + "/silence", "", false, null, null, 0);
		}

		sendToAsterisk("POST", "/ari/bridges/" + confID, "", false, null, null, 0);

		astPredictiveDialCommand awsReq = new astPredictiveDialCommand();
		awsReq.channel = foundLeg;
		String outStr = awsReq.ToJson();

		sendToAsterisk("POST", "/ari/bridges/" + confID + "/addChannel", outStr, false, null, null, 0);

		if (conf.participants.contains(awsReq.channel) == false)
			conf.participants.add(awsReq.channel);

		if (conf.isRecording) {
			//ConferencePauseRecording(confID);
			conf.isRecording = false;
			// notify to dialer
			RecordInfo recordinfo = new RecordInfo();
			recordinfo.EventName = "recordingpause";
			recordinfo.CustomerID = CustomerID;
			recordinfo.toPhoneNumber = legID;
			recordinfo.filename = conf.filename;
			recordinfo.toAgentID = toNumber;
			recordinfo.endtime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());

			String outmsg = recordinfo.ToJson();

			network.SocketHandler.notifyAllDailers(outmsg);
		} else {
		//	ConferenceUnPauseRecording(confID);
			conf.isRecording = true;
			// notify to dialer
			RecordInfo recordinfo = new RecordInfo();
			recordinfo.toPhoneNumber = CustomerID;
			recordinfo.CustomerID = CustomerID;
			recordinfo.EventName = "recordingunpause";
			recordinfo.filename = conf.filename;
			recordinfo.endtime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());

			String outmsg = recordinfo.ToJson();

			network.SocketHandler.notifyAllDailers(outmsg);
		}

		// for transfer
		if (transferbegin) {

			if (conf.filename != null && (!CustomerID.isEmpty())) {
				if (conf.filename.contains("child"))
					conf.filename = conf.filename + "-2";
				else
					conf.filename = conf.filename + "-child";

				//ConferenceStartRecording(confID);

				RecordInfo recordinfo = new RecordInfo();
				recordinfo.EventName = "recordingstart";
				recordinfo.CustomerID = CustomerID;
				recordinfo.callid = awsReq.channel;
				recordinfo.filename = conf.filename;
				recordinfo.fileextension = confID;
				recordinfo.fromphoneNumber = confID;
				recordinfo.toPhoneNumber = CustomerID;
				recordinfo.toAgentID = toNumber;
				recordinfo.endtime = new SimpleDateFormat("yyyy/MM/dd-HH:mm:ss").format(new Date());

				String outmsg = recordinfo.ToJson();

				network.SocketHandler.notifyAllDailers(outmsg);
			}
			conf.isRecording = true;
		}

		return false;
	}

	@Override
	public boolean TransferComplete(CallOriginator originator) {
		String foundLeg = "";
		String transferNumber = "";
		String CustomerID = "";
		for (String p : channelList.keySet()) {
			astCallInfo cInfo = channelList.get(p);
			if (cInfo.AgentExtension.equals(originator.FromNumber) && cInfo.isAgent == true) {
				foundLeg = p;
				transferNumber = cInfo.TransferDesti;
				break;
			}
		}

		for (String p : channelList.keySet()) {
			astCallInfo cInfo = channelList.get(p);
			if (cInfo.AgentExtension.equals(originator.FromNumber) && cInfo.ToNumber == transferNumber
					&& cInfo.isAgent == false) {
				foundLeg = p;
				break;
			}
		}

		if (foundLeg.isEmpty()) {
			LogWriter.instance.appendInfo("Hold: could not find leg for " + originator.FromNumber);
			return false;
		}

		ConferenceRemoveChannel(originator.FromNumber, foundLeg, true);

		// assign filename
		astConference conf = null;

		if (confList.containsKey(originator.FromNumber) != false) {
			conf = new astConference();
			conf.filename = confList.get(originator.FromNumber).filename;
			conf.isRecording = false;
			confList.put(transferNumber, conf);
		}

		TransferAddChannel("", originator.Option, transferNumber, foundLeg, true, true);

		for (String p : channelList.keySet()) {
			astCallInfo cInfo = channelList.get(p);
			if (cInfo.AgentExtension.equals(originator.FromNumber) && cInfo.isAgent == false && (!p.equals(foundLeg))) {
				foundLeg = p;
				CustomerID = cInfo.ToCUNumber;
				break;
			}
		}

		TransferAddChannel(CustomerID, originator.Option, transferNumber, foundLeg, true, true);

		return true;
	}

	@Override
	public boolean TransferCancel(CallOriginator originator) {
		String foundLeg = "";
		String CustomerID = "";
		List<Entry<String, astCallInfo>> Filteredchannel = channelList.entrySet().stream()
				.filter(e -> e.getValue().AgentExtension.equals(originator.FromNumber) && e.getValue().isAgent == false)
				.collect(Collectors.toList());
		for (Entry<String, astCallInfo> p : Filteredchannel) {
			if (p.getValue().ToCUNumber.isEmpty() || p.getValue().ToCUNumber == null) {
				astCallInfo cInfo = p.getValue();

				foundLeg = p.getKey();
				originator.FromNumber = cInfo.AgentExtension;
				break;
			}
		}
		astConference conf = null;
		if (confList.containsKey(originator.FromNumber) == false) {
			conf = new astConference();
			conf.name = originator.FromNumber;
			confList.put(originator.FromNumber, conf);
		} else {
			conf = confList.get(originator.FromNumber);
		}
		conf.isRecording = false;
		if (foundLeg.isEmpty()) {
			LogWriter.instance.appendInfo("Hold: could not find leg for " + originator.FromNumber);
			return false;
		}
		ConferenceRemoveChannel(originator.FromNumber, foundLeg, true);
		channelList.remove(foundLeg);
		for (Entry<String, astCallInfo> p : Filteredchannel) {
			if (!p.getKey().equals(foundLeg)) {
				foundLeg = p.getKey();
				CustomerID = p.getValue().ToCUNumber;
				break;
			}
		}
		TransferAddChannel(CustomerID, originator.Option, originator.FromNumber, foundLeg, true, false);
		return true;
	}

	@Override
	public CallOriginator GetCallOriginator(String fromNumber, String option) {
		astCallOriginator orig = new astCallOriginator();
		orig.FromNumber = fromNumber;
		orig.Option = option;

		return orig;
	}

	@Override
	public CallReceiver GetCallReceiver(String toNumber) {
		String toNum = toNumber;
		String cuID = "";
		String prefix = "";
		String jobID = "";
		String timeOut = "";
		String cuName = "";

		String deliminator = ";";
//"Option":"910003177;35;24961;4;3"

		if (toNumber != null && toNumber.isEmpty() == false) {
			String[] split = toNumber.split(deliminator);
			if (split != null && split.length > 1) {
				toNum = split[0];
				timeOut = split[1];

				if(split.length > 2)
					cuName = split[2];

				
//				if(split.length > 2) {
//					fac = split[2];
//				}
				if(split.length > 3) {
					cuID = split[3];
				}

				if(split.length > 4) {
					jobID = split[4];
				}
				
				if(split.length > 5) {
					prefix = split[5];
				}
			}
		}

		astCallReceiver orig = new astCallReceiver();
		orig.ToNumber = toNum;
		orig.ToCUNumber = cuID;
		orig.timeOut = timeOut;
		orig.jobID   = jobID;
		orig.fac = prefix;
		orig.ToCUName = cuName;

		return orig;
	}

	@Override
	public boolean DialPredictive(CallOriginator originator, CallReceiver receiver) {

		//add uui 
		String callerNumber = receiver.fac;
		String calledNumber = receiver.ToNumber;
		String customerID = receiver.ToCUNumber;
		String fac = receiver.fac;
		String jobID = receiver.jobID;
		String customerName = receiver.ToCUName;
		String uuiMsg = "dialer:" + customerID + ";p:" + calledNumber + ";j:" + jobID +";c:"+ customerName;

		astPredictiveDialCommand awsReq = new astPredictiveDialCommand();
		
		awsReq.endpoint = this.trunkName + receiver.ToNumber;
		awsReq.app = this.stasisApp;
		awsReq.uui = uuiMsg;
		String outStr = awsReq.ToJson();

		if (sendToAsterisk("POST", "/ari/channels", outStr, true, receiver, null, 0)) {
			originator.SystemCallID = receiver.SystemCallID;
			
			return true;
		}

		return false;
	}

	public synchronized boolean ConferenceAddChannel(String confID, String legID, boolean legIsAlreadyID) {
		// Search for an active call
		String foundLeg = "";
		boolean isAgent = false;
		String CustomerID = null;
		if (legIsAlreadyID) {
			foundLeg = legID;
			astCallInfo cInfo = channelList.get(foundLeg);
			isAgent = cInfo.isAgent;
			cInfo.AgentExtension = confID;
		} else {
			for (String p : channelList.keySet()) {
				astCallInfo cInfo = channelList.get(p);

				if (cInfo.ToCUNumber != null && cInfo.ToCUNumber.equals(legID)) {
					foundLeg = p;
					isAgent = cInfo.isAgent;
					cInfo.AgentExtension = confID;
					CustomerID = cInfo.ToCUNumber;
					
					break;
				}
			}
		}

		if (foundLeg.isEmpty()) {
			LogWriter.instance.appendInfo("ConferenceAddChannel: could not find leg " + legID);
			return false;
		}
		LogWriter.instance.appendInfo("ConferenceAddChannel: found leg " + legID);

		
		astConference conf = null;
		if (confList.containsKey(confID) == false) {
			conf = new astConference();
			conf.name = confID;
			confList.put(confID, conf);
		} else {
			conf = confList.get(confID);
		}

		if (isAgent) {
			sendToAsterisk("DELETE", "/ari/channels/" + foundLeg + "/silence", "", false, null, null, 0);
		}

		sendToAsterisk("POST", "/ari/bridges/" + confID, "", false, null, null, 0);

		astPredictiveDialCommand awsReq = new astPredictiveDialCommand();
		awsReq.channel = foundLeg;
		String outStr = awsReq.ToJson();

		sendToAsterisk("POST", "/ari/bridges/" + confID + "/addChannel", outStr, false, null, null, 0);

		if (conf.participants.contains(awsReq.channel) == false)
			conf.participants.add(awsReq.channel);

		//ConferenceStartRecording(confID);

		if (conf.filename != null) {
			RecordInfo recordinfo = new RecordInfo();
			recordinfo.EventName = "recordingstart";
			recordinfo.CustomerID = CustomerID;
			recordinfo.callid = awsReq.channel;
			recordinfo.filename = conf.filename;
			recordinfo.fileextension = confID;
			recordinfo.fromphoneNumber = confID;
			recordinfo.toPhoneNumber = legID;

			String outmsg = recordinfo.ToJson();

			network.SocketHandler.notifyAllDailers(outmsg);
		}
		return false;
	}

	public synchronized boolean ConferenceRemoveChannel(String confID, String legID, boolean legIsAlreadyID) {
		// Search for an active call
		String foundLeg = "";
		if (legIsAlreadyID) {
			foundLeg = legID;
		} else {
			for (String p : channelList.keySet()) {
				astCallInfo chID = channelList.get(p);
				if (chID.ToCUNumber.equals(legID)) {
					foundLeg = p;
					break;
				}
			}
		}

		if (foundLeg.isEmpty()) {
			LogWriter.instance.appendInfo("ConferenceRemoveChannel: could not find leg " + legID);
			return false;
		}

		astPredictiveDialCommand awsReq = new astPredictiveDialCommand();
		awsReq.channel = foundLeg;
		String outStr = awsReq.ToJson();

		sendToAsterisk("POST", "/ari/bridges/" + confID + "/removeChannel", outStr, false, null, null, 0);

		return false;
	}

	public synchronized boolean ConferenceStartRecording(String confID) {
		astConference conf = confList.get(confID);

		if (conf == null) {
			LogWriter.instance.appendInfo("ConferenceStartRecording: could not find confID: " + confID);
			return false;
		}

		if (conf.isRecording) {
			return true;
		}

		// Get the customer's channelID
		String foundLeg = "";
		String fromNumber = "";
		String toNumber = "";
		for (String p : channelList.keySet()) {
			astCallInfo cInfo = channelList.get(p);
			if (cInfo.AgentExtension.equals(confID) && cInfo.isAgent == false) {
				fromNumber = cInfo.AgentExtension;
				toNumber = cInfo.ToNumber;
				foundLeg = p;
				break;
			}
		}

		if (foundLeg.isEmpty()) {
			return false;
		}
		String date = new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date());
		conf.isRecording = true;
		conf.liveRecordingName = foundLeg;
		if (toNumber.contains(",")) {
			toNumber = toNumber.split(",")[0];
		}
		if (conf.filename == null) {
			conf.filename = fromNumber + "-" + toNumber + "-" + date;
			conf.parentid = conf.filename;
		}
		astPredictiveDialCommand awsReq = new astPredictiveDialCommand();
		awsReq.name = conf.filename;
		awsReq.format = "wav";
		String outStr = awsReq.ToJson();

		sendToAsterisk("POST", "/ari/bridges/" + confID + "/record", outStr, false, null, null, 0);
		return true;
	}

	public synchronized boolean ConferenceStopRecording(String confID) {
		astConference conf = confList.get(confID);

		if (conf == null) {
			LogWriter.instance.appendInfo("ConferenceStopRecording: could not find confID: " + confID);
			return false;
		}

		if (conf.isRecording == false) {
			return false;
		}

		conf.isRecording = false;

		sendToAsterisk("POST", "/ari/recordings/live/" + conf.filename + "/stop", "", false, null, null, 0);
		return true;
	}

	public synchronized boolean ConferencePauseRecording(String confID) {
		astConference conf = confList.get(confID);

		if (conf == null) {
			LogWriter.instance.appendInfo("ConferenceStopRecording: could not find confID: " + confID);
			return false;
		}

		if (conf.isRecording == false) {
			return false;
		}

		conf.isRecording = false;

		sendToAsterisk("POST", "/ari/recordings/live/" + conf.filename + "/pause", "", false, null, null, 0);

		return true;
	}

	public synchronized boolean ConferenceUnPauseRecording(String confID) {
		astConference conf = confList.get(confID);

		if (conf == null) {
			LogWriter.instance.appendInfo("ConferenceStopRecording: could not find confID: " + confID);
			return false;
		}

		if (conf.isRecording == false) {
			return false;
		}

		conf.isRecording = false;

		sendToAsterisk("DELETE", "/ari/recordings/live/" + conf.filename + "/pause", "", false, null, null, 0);

		return true;
	}

	private static String getBasicAuthenticationEncoding(String username, String password) {
		String userPassword = username + ":" + password;
		return Base64.getEncoder().encodeToString(userPassword.getBytes());
	}

	public boolean sendToAsterisk(String requestMethod, String urlType, String params, boolean addChannel,
			CallReceiver receiver, CallOriginator originator, int retryLevel) {
		//sendToAsterisk("DELETE", "/ari/channels/" + targetID, outStr, false, null, null, 0);

		// http://localhost:8088/ari/channels/1400609726.3/play?media=sound:hello-world

		boolean retVal = false;
		retryLevel++;

		LogWriter.instance.appendInfo("sendToAsterisk: " + requestMethod + " " + urlType + " " + params);

		if (requestMethod.isEmpty())
			requestMethod = "POST";

		HttpURLConnection http = null;
		
		try {
			String protocol = "http";
			if (AppProperties.instance.pbxEnableSSL)
				protocol = "https";
			String postUrl = protocol + "://" + PbxInfo.Url + ":" + PbxInfo.Port + urlType;
			
			URL url = new URL(postUrl);
			URLConnection con = url.openConnection();
			http = (HttpURLConnection) con;
			http.setRequestMethod(requestMethod);

			// http.setRequestProperty("Authorization", "Basic " +
			// getBasicAuthenticationEncoding("ast20190529","sakura529"));
			http.setRequestProperty("Authorization",
					"Basic " + getBasicAuthenticationEncoding(PbxInfo.Username, PbxInfo.Password));
			
			if (requestMethod != "GET") {
				http.setDoOutput(true);

				byte[] out = params.getBytes();
				int length = out.length;

				http.setFixedLengthStreamingMode(length);
				
				http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
				http.connect();
			
					OutputStream os = http.getOutputStream();
					os.write(out);
					os.close();
						
			}
			else {
				http.connect();
			}

			BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
			StringBuilder stringBuilder = new StringBuilder();

			String line = null;
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line + "\n");
			}

			String retStr = stringBuilder.toString();
			LogWriter.instance.appendInfo("Asterisk replied: " + retStr);

			if (addChannel && receiver != null) {
				AsteriskChannelPeer chpeer = new AsteriskChannelPeer(retStr);

				synchronized (channelList) {
					astCallInfo callInfo = new astCallInfo();
					callInfo.ToNumber = receiver.ToNumber;
					callInfo.ToCUNumber = receiver.ToCUNumber;
					callInfo.isAgent = receiver.isAgent;
					callInfo.delayedDial = receiver.delayDial;
					callInfo.delayedMonitorDial = receiver.delayMonitorDial;
					
					if (originator != null)
						callInfo.AgentExtension = originator.FromNumber;

					channelList.put(chpeer.id, callInfo);
					receiver.SystemCallID = chpeer.id;
				}
			}

			retVal = retStr.contains("error") == false;
		} catch (NoRouteToHostException ex) {
			LogWriter.instance.appendInfo("Asterisk exception, NoRouteToHostException");
			LogWriter.instance.appendInfo(ex.getLocalizedMessage());
			LogWriter.instance.appendInfo(ex.getMessage());

			LogWriter.instance.appendInfo(""+ex.toString());
		
		} catch (FileNotFoundException ex) {
			LogWriter.instance.appendInfo("Asterisk exception, FileNotFoundException");
			LogWriter.instance.appendInfo(ex.getLocalizedMessage());
			LogWriter.instance.appendInfo(ex.getMessage());
			//if("DELETE".equals(requestMethod) && originator != null && originator.SystemCallID != null) {
			//	LogWriter.instance.appendInfo("[FileNotFoundException@sendtoAsterisk] channel id removed by error :" + originator.SystemCallID);
			//	channelList.remove(originator.SystemCallID);
			//}
			retVal=false;
		}  
		catch (ConnectException ex) {
			LogWriter.instance.appendInfo("Asterisk exception, ConnectException");

			LogWriter.instance.appendInfo(ex.getLocalizedMessage());
			LogWriter.instance.appendInfo(ex.getMessage());

			LogWriter.instance.appendInfo(""+ex.toString());

			if(retryLevel < AppProperties.instance.asteriskReconAttemptCount) {
				sendAsteriskDiscEvent(this.pbxID);
				sendToAsterisk(requestMethod, urlType, params, addChannel, receiver, originator, retryLevel);
			} else {
				//sendAsteriskDownEvent(this.pbxID);
			}
		} catch (IOException ex) {
			LogWriter.instance.appendInfo("Asterisk exception, IOException");

			LogWriter.instance.appendInfo(ex.getLocalizedMessage());
			LogWriter.instance.appendInfo(ex.getMessage());
			if("DELETE".equals(requestMethod) && originator != null && originator.SystemCallID != null) {
				LogWriter.instance.appendInfo("[IOException@sendtoAsterisk] channel id removed by error :" + originator.SystemCallID);
				channelList.remove(originator.SystemCallID);

			}
			if(retryLevel < AppProperties.instance.asteriskReconAttemptCount) {
				/*
				 * sendAsteriskDiscEvent(this.pbxID); sendToAsterisk(requestMethod, urlType,
				 * params, addChannel, receiver, originator, retryLevel);
				 */
			} else {
				// Normal Asterisk errors, dropped wrong channel, recording wrong bridge ...

				int code = 0;
				try {
					if (http != null)
						code = http.getResponseCode();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					LogWriter.instance.appendInfo("Asterisk exception, NoRouteToHostException");
					LogWriter.instance.appendInfo(e.getLocalizedMessage());
					LogWriter.instance.appendInfo(e.getMessage());

					LogWriter.instance.appendInfo(""+e.toString());
				}

				// Not a normal Asterisk error
				if (code >= 500) {
					LogWriter.instance.appendInfo("Asterisk exception, code > 500");
					LogWriter.instance.appendInfo(ex.getLocalizedMessage());
					LogWriter.instance.appendInfo(ex.getMessage());
					LogWriter.instance.appendInfo(""+ex.toString());
					return false;

					/* LogWriter.instance.appendInfo("Asterisk is down. error code: " + code);
					if(retryLevel < AppProperties.instance.asteriskReconAttemptCount) {
						sendAsteriskDiscEvent(this.pbxID);
						sendToAsterisk(requestMethod, urlType, params, addChannel, receiver, originator, retryLevel);
					} else {
						sendAsteriskDownEvent(this.pbxID);
					}
					*/			
				}
				/*
				 * else sendAsteriskDownEvent(this.pbxID);
				 */
			}
		} catch (Exception ex) {
			// Normal Asterisk errors, dropped wrong channel, recording wrong bridge ...
			LogWriter.instance.appendInfo("Asterisk exception, Exception");

			int code = 0;
			try {
				if (http != null)
					code = http.getResponseCode();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				LogWriter.instance.appendInfo("Asterisk exception, Exception");
				LogWriter.instance.appendInfo(e.getLocalizedMessage());
				LogWriter.instance.appendInfo(e.getMessage());

				LogWriter.instance.appendInfo(""+e.toString());
			}

			// Not a normal Asterisk error
			if (code > 500) {
				LogWriter.instance.appendInfo(ex.getLocalizedMessage());
				LogWriter.instance.appendInfo(ex.getMessage());

				LogWriter.instance.appendInfo(""+ex.toString());

				/* LogWriter.instance.appendInfo("Asterisk is down. error code: " + code);
				if(retryLevel < AppProperties.instance.asteriskReconAttemptCount) {
					sendAsteriskDiscEvent(this.pbxID);
					sendToAsterisk(requestMethod, urlType, params, addChannel, receiver, originator, retryLevel);
				} else {
					sendAsteriskDownEvent(this.pbxID);
				}
				*/			
			}
			
		}

		return retVal;
	}

	public void sendAsteriskDownEvent(int pbxID) {
		EventsReceived evt = new EventsReceived("asteriskdown", "");
		evt.pbxid = pbxID;
		String outmsg = evt.ToJson();
		// LogWriter.instance.appendInfo(String.format("Response: %s", outmsg));
		network.SocketHandler.notifyAllDailers(outmsg);		
	}

	public void sendAsteriskDiscEvent(int pbxID) {
		EventsReceived evt = new EventsReceived("asteriskdisconnect", "");
		evt.pbxid = pbxID;
		String outmsg = evt.ToJson();
		// LogWriter.instance.appendInfo(String.format("Response: %s", outmsg));
		network.SocketHandler.notifyAllDailers(outmsg);		
	}
}
