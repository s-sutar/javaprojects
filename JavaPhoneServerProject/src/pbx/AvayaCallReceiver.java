package pbx;

public class AvayaCallReceiver extends CallReceiver {
	
	public AvayaCallReceiver(String toNumber, String cuNumber, String featureAccessCode, String jobID, String timeOut, String cuName) {
		this.ToNumber = toNumber;
		this.ToCUNumber = cuNumber;
		this.fac = featureAccessCode;
		this.jobID = jobID;
		this.timeOut = timeOut;
		this.ToCUName = cuName;
	}

}
