package pbx;

import javax.telephony.Call;

import network.WebClient;

public abstract class Pbx {
	
	public enum PbxType {
		AVAYA(1),
		ASTERISK(2),
		SIMULATION(3),
		MPBX(4);

	  private int value;    

	  private PbxType(int value) {
	    this.value = value;
	  }

	  public int getValue() {
	    return value;
	  }
	}
	
	public int pbxID;
	public WebClient pbxWebClient = new WebClient();
	
	public PbxType Type;
	public PbxInfo PbxInfo;
	
	public Pbx(PbxInfo pbxInfo) {
		this.PbxInfo = pbxInfo;
	}
	
	public boolean pbxIsActive = false;
	
	public abstract boolean Login();
	public abstract boolean Logout();
	public abstract boolean Monitor(CallOriginator originator, CallReceiver receiver);
	public abstract boolean Unmonitor(CallOriginator originator);
	public abstract boolean SetAgentReady(CallOriginator originator);
	public abstract boolean SetAgentNotReady(CallOriginator originator);
	public abstract boolean SetAgentPostwork(CallOriginator originator);
	public abstract boolean Dial(CallOriginator originator, CallReceiver receiver);
	public abstract boolean Drop(CallOriginator originator);
	public abstract boolean Answer(CallOriginator originator);
	public abstract boolean Hold(CallOriginator originator);
	public abstract boolean Unhold(CallOriginator originator);
	public abstract boolean TransferSetup(CallOriginator originator, CallReceiver receiver);
	public abstract boolean TransferComplete(CallOriginator originator);
	public abstract boolean TransferCancel(CallOriginator originator);
	
	public abstract CallOriginator GetCallOriginator(String fromNumber, String option);
	public abstract CallReceiver GetCallReceiver(String toNumber);

	public abstract boolean DialPredictive(CallOriginator originator, CallReceiver receiver);

	public abstract String finishInit();

	public abstract void closeConnection();

	public abstract void removeCallObj(Object call);

}
