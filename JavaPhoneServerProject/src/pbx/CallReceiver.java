package pbx;

public abstract class CallReceiver {
	
	public String SystemCallID = null;
	public String ToNumber = null;
	public String ToCUNumber = null;
	public String ToCUName = null;
	public String fac = null;
	public String setting = null;
	public int callIndex = 0;
	public String jobID = null;
	public String timeOut = null;
	public boolean isAgent = false;
	public String delayDial = null;
	public String delayMonitorDial = null;
	public String BaseID = null;
	public String Reason = "0";
}
