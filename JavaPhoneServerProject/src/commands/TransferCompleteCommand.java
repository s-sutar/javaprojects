package commands;

import core.EventsReceived;
import pbx.CallOriginator;
import pbx.Pbx;

public class TransferCompleteCommand extends Command {
	
	public TransferCompleteCommand(EventsReceived evt, Pbx pbx, CallOriginator originator) {
		super(evt, pbx, originator);
	}

	@Override
	public void Execute() {		
		this.isSuccess = this.pbx.TransferComplete(originator);
		
		ProcessCallback();
	}
	
}
