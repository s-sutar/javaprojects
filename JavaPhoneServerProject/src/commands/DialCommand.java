package commands;

import core.EventsReceived;
import pbx.CallOriginator;
import pbx.CallReceiver;
import pbx.Pbx;

public class DialCommand extends Command {

	public DialCommand(EventsReceived evt, Pbx pbx, CallOriginator originator, CallReceiver receiver) {
		super(evt, pbx, originator, receiver);
	}

	@Override
	public void Execute() {		
		this.isSuccess = this.pbx.Dial(originator, receiver);
		
		if(receiver != null) {
			this.evt.CallIndex = receiver.callIndex;
		}
		
		ProcessCallback();
	}

}
