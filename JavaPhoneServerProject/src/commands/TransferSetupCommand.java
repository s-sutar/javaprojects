package commands;

import core.EventsReceived;
import pbx.CallOriginator;
import pbx.CallReceiver;
import pbx.Pbx;

public class TransferSetupCommand extends Command {
	
	public TransferSetupCommand(EventsReceived evt, Pbx pbx, CallOriginator originator, CallReceiver receiver) {
		super(evt, pbx, originator, receiver);
	}

	@Override
	public void Execute() {	
		this.isSuccess = this.pbx.TransferSetup(originator, receiver);
		
		ProcessCallback();
	}
	
}
