package commands;

import core.EventsReceived;
import pbx.CallOriginator;
import pbx.Pbx;

public class DropCommand extends Command {

	public DropCommand(EventsReceived evt, Pbx pbx, CallOriginator originator) {
		super(evt, pbx, originator);
	}

	@Override
	public void Execute() {		
		this.isSuccess = this.pbx.Drop(originator);
		ProcessCallback();
	}

}
