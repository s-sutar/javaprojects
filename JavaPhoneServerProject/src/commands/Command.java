package commands;

import core.EventsReceived;
import network.WebClient;
import pbx.CallOriginator;
import pbx.CallReceiver;
import pbx.Pbx;

public abstract class Command {
	
	protected EventsReceived evt;
	public Pbx pbx;
	protected CallOriginator originator;
	protected CallReceiver receiver;
	protected boolean isSuccess;
	
	public Command(EventsReceived evt, Pbx pbx, CallOriginator originator) {
		this.evt = evt;
		this.pbx = pbx;
		this.originator = originator;
	}
	
	public Command(EventsReceived evt, Pbx pbx, CallOriginator originator, CallReceiver receiver) {
		this.evt = evt;
		this.pbx = pbx;
		this.originator = originator;
		this.receiver = receiver;
	}
	
	public String getInternalCallID() {
		String ret = "";
		
		if(this.receiver != null) {
			String calledNumber = receiver.ToNumber;
			String customerID = receiver.ToCUNumber;

			ret = customerID + "-" + calledNumber;
		}
		
		return ret;
	}
	
	public void ProcessCallback() {
		if(this.evt != null) {
			this.evt.Message = (this.isSuccess) ? "success" : "failed";
			
			if(this.pbx != null)
				this.evt.pbxid = this.pbx.pbxID;
			
			String msg = this.evt.ToJson();
//			if(this.pbx != null) {
//				this.pbx.pbxWebClient.SendMessage(msg);
//			}
			network.SocketHandler.notifyAllDailers(msg);
		}
	}
	
	public abstract void Execute();

	@Override
	public String toString() {
		String text = "Command: ";
		
		if(this.pbx != null) {
			text += this.pbx.toString();
		}
		
		if(this.originator != null) {
			text += "," + this.originator.toString();
		}
		
		if(this.receiver != null) {
			text += "," + this.receiver.toString();
		}
		
		return text;
	}

}
