package commands;

import core.EventsReceived;
import pbx.CallOriginator;
import pbx.Pbx;

public class SetAgentNotReadyCommand extends Command {
	
	public SetAgentNotReadyCommand(EventsReceived evt, Pbx pbx, CallOriginator originator) {
		super(evt, pbx, originator);
	}

	@Override
	public void Execute() {
		this.isSuccess = this.pbx.SetAgentNotReady(originator);
		ProcessCallback();
	}

}
