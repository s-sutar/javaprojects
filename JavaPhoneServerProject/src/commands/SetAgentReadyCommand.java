package commands;

import core.EventsReceived;
import pbx.CallOriginator;
import pbx.Pbx;

public class SetAgentReadyCommand extends Command {
	
	public SetAgentReadyCommand(EventsReceived evt, Pbx pbx, CallOriginator originator) {
		super(evt, pbx, originator);
	}

	@Override
	public void Execute() {
		this.isSuccess = this.pbx.SetAgentReady(originator);
		if(originator != null) {
			this.evt.SystemCallID = originator.SystemCallID;
		}
		
		ProcessCallback();
	}

}
