package commands;

import core.EventsReceived;
import pbx.CallOriginator;
import pbx.Pbx;

public class TransferCancelCommand extends Command {
	
	public TransferCancelCommand(EventsReceived evt, Pbx pbx, CallOriginator originator) {
		super(evt, pbx, originator);
	}

	@Override
	public void Execute() {		
		this.isSuccess = this.pbx.TransferCancel(originator);
		
		ProcessCallback();
	}
	
}
