package commands;

import core.EventsReceived;
import pbx.CallOriginator;
import pbx.Pbx;

public class SetAgentPostworkCommand extends Command {
	
	public SetAgentPostworkCommand(EventsReceived evt, Pbx pbx, CallOriginator originator) {
		super(evt, pbx, originator);
	}

	@Override
	public void Execute() {
		this.isSuccess = this.pbx.SetAgentPostwork(originator);
		ProcessCallback();
	}

}
