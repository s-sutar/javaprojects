package commands;

import core.EventsReceived;
import pbx.CallOriginator;
import pbx.Pbx;

public class HoldCommand extends Command {
	
	public HoldCommand(EventsReceived evt, Pbx pbx, CallOriginator originator) {
		super(evt, pbx, originator);
	}

	@Override
	public void Execute() {		
		this.isSuccess = this.pbx.Hold(originator);
		ProcessCallback();
	}
	
}
