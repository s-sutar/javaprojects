package commands;

import core.EventsReceived;
import core.LogWriter;
import pbx.CallOriginator;
import pbx.Pbx;

public class AnswerCommand extends Command {
	
	public AnswerCommand(EventsReceived evt, Pbx pbx, CallOriginator originator) {
		super(evt, pbx, originator);
	}

	@Override
	public void Execute() {
		if(originator.Option != null && "acd".equals(originator.Option.toLowerCase())) {
			LogWriter.instance.appendInfo("Answer: returning success because Option=ACD");
			this.isSuccess = true;	
		}
		else {
			this.isSuccess = this.pbx.Answer(originator);	
		}
		
		ProcessCallback();
	}


}
