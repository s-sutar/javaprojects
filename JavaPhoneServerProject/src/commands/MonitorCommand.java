package commands;

import core.EventsReceived;
import db.RecordDBManager;
import pbx.CallOriginator;
import pbx.CallReceiver;
import pbx.Pbx;

public class MonitorCommand extends Command {
	
	public MonitorCommand(EventsReceived evt, Pbx pbx, CallOriginator originator, CallReceiver receiver) {
		super(evt, pbx, originator);
	}

	@Override
	public void Execute() {
		this.isSuccess = this.pbx.Monitor(originator, receiver);
		
		if(this.isSuccess) {
			RecordDBManager.GetInstance().updateRecordTable(originator.FromNumber, originator.AcdUsername, 0);
		}
		
		ProcessCallback();
	}

}
