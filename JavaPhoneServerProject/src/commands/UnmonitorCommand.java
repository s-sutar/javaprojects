package commands;

import core.EventsReceived;
import db.RecordDBManager;
import pbx.CallOriginator;
import pbx.Pbx;

public class UnmonitorCommand extends Command {
	
	public UnmonitorCommand(EventsReceived evt, Pbx pbx, CallOriginator originator) {
		super(evt, pbx, originator);
	}

	@Override
	public void Execute() {
		this.isSuccess = this.pbx.Unmonitor(originator);
		
		if(this.isSuccess) {
			RecordDBManager.GetInstance().updateRecordTable(originator.FromNumber, null, 0);
		}
		
		ProcessCallback();
	}

}
