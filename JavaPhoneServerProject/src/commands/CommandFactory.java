package commands;

import core.EventsReceived;
import core.LogWriter;
import pbx.CallOriginator;
import pbx.CallReceiver;
import pbx.Pbx;

public class CommandFactory {
	
	private static CommandFactory _instance = new CommandFactory();
	
	
	private CommandFactory() {
	}
	
	public static CommandFactory GetInstance() {		
		if(_instance == null) {
			synchronized(CommandFactory.class) {
				if(_instance == null) {
					_instance = new CommandFactory();
				}
			}//end synchronized
		}
		
		return _instance;
	}
	
	public Command GetCommand(String commandName, Pbx pbx,
			CallOriginator originator, CallReceiver receiver) {
		if(pbx == null || commandName == null || commandName.isEmpty()) {
			LogWriter.instance.appendError("Failed to GetCommand: invalid inputs");
			
			return null;
		}

		String fromNumber = (originator != null) ? originator.FromNumber : "";
		EventsReceived evt = new EventsReceived(commandName, fromNumber);
		
		switch(commandName) {
			case "agentlogin": {
				evt.EventName = "loggedin";
				return new MonitorCommand(evt, pbx, originator, receiver);
			}
			
			case "agentlogout": {
				evt.EventName = "logout";
				return new UnmonitorCommand(evt, pbx, originator);
			}
			case "dial": {
				return new DialCommand(evt, pbx, originator, receiver);
			}
			case "dialpredictive": {
				return new DialPredictiveCommand(evt, pbx, originator, receiver);
			}
			case "drop": {
				return new DropCommand(evt, pbx, originator);
			}
			case "onhold": {
				return new HoldCommand(evt, pbx, originator);
			}
			case "offhold": {
				return new UnholdCommand(evt, pbx, originator);
			}
			case "transfersetup": {
				return new TransferSetupCommand(evt, pbx, originator, receiver);
			}
			case "transfercomplete": {
				return new TransferCompleteCommand(evt, pbx, originator);
			}
			case "transfercancel": {
				return new TransferCancelCommand(evt, pbx, originator);
			}
			case "answer" : {
				return new AnswerCommand(evt, pbx, originator);
			}
			case "ready" : {
				return new SetAgentReadyCommand(evt, pbx, originator);
			}
			case "notready" :
			case "away" :
			case "rest" : {
				return new SetAgentNotReadyCommand(evt, pbx, originator);
			}
			case "postwork" : {
				return new SetAgentPostworkCommand(evt, pbx, originator);
			}
			case "monitor": {
				evt.EventName = "monitor";
				return new MonitorCommand(evt, pbx, originator, receiver);
			}
			case "unmonitor": {
				evt.EventName = "unmonitor";
				return new UnmonitorCommand(evt, pbx, originator);
			}
			default : {
				return null;
			}
		}//end switch-case	
	}

}
