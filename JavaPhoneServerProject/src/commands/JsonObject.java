package commands;

import com.google.gson.Gson;

import core.LogWriter;

public class JsonObject {
	public String ToJson() {
		String json = "";

		try {
			Gson gson = new Gson();
			json = gson.toJson(this);
		} catch (Exception e) {
			LogWriter.instance.appendException(e);
		}

		return json;
	}
}
