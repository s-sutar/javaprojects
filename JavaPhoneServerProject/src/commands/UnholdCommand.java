package commands;

import core.EventsReceived;
import pbx.CallOriginator;
import pbx.Pbx;

public class UnholdCommand extends Command {
	
	public UnholdCommand(EventsReceived evt, Pbx pbx, CallOriginator originator) {
		super(evt, pbx, originator);
	}

	@Override
	public void Execute() {		
		this.isSuccess = this.pbx.Unhold(originator);
		ProcessCallback();
	}
	
}
