package commands;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import core.AppProperties;
import core.EventsReceived;
import core.LogWriter;
import core.QueueManagerForPred;
import pbx.CallOriginator;
import pbx.CallReceiver;
import pbx.Pbx;

public class DialPredictiveCommand extends Command {
	static ThreadPoolExecutor executor = null;			  
	
	public DialPredictiveCommand(EventsReceived evt, Pbx pbx, CallOriginator originator, CallReceiver receiver) {
		super(evt, pbx, originator, receiver);
	}

	@Override
	public void Execute() {	
		DialPredictiveCommand thisCommand = this;

		if( executor == null)
		{
			executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(AppProperties.instance.maxthread_avaya_dialing);
		}
		
		LogWriter.instance.appendInfo(String.format(
				"Start adding MakePredictiveCall cuID:%s maxpoolSize:%d activeThreads: %d queueSize: %d predictiveQueueSize: %d",
				receiver.ToCUNumber,
				executor.getMaximumPoolSize(),
				executor.getActiveCount(),
				executor.getQueue().size(),
				QueueManagerForPred.GetInstance().size()));

		if( executor.getQueue().size() >= executor.getMaximumPoolSize()) {
			//save this command for later
			LogWriter.instance.appendInfo(String.format(
					"Saving MakePredictiveCall cuID:%s maxpoolSize:%d activeThreads: %d queueSize: %d predictiveQueueSize: %d",
					thisCommand.receiver.ToCUNumber,
					executor.getMaximumPoolSize(),
					executor.getActiveCount(),
					executor.getQueue().size(),
					QueueManagerForPred.GetInstance().size()));

			QueueManagerForPred.GetInstance().AddCommand(thisCommand);
		}
		else {
			executor.submit(() -> {
				thisCommand.isSuccess = thisCommand.pbx.DialPredictive(thisCommand.originator, thisCommand.receiver);
								
				if(receiver != null) {
					thisCommand.evt.CallIndex = receiver.callIndex;
					thisCommand.evt.CustomerID = receiver.ToCUNumber;
					thisCommand.evt.SystemCallID = receiver.SystemCallID;
					thisCommand.evt.BaseID = receiver.BaseID;
					thisCommand.evt.Reason = receiver.Reason;
					thisCommand.evt.Exten = receiver.ToNumber;
					thisCommand.evt.CustomerName = receiver.ToCUName;
				}
				
				thisCommand.ProcessCallback();
				
				Command nextCommand = QueueManagerForPred.GetInstance().GetCommand();
				
				if( nextCommand != null) {
					LogWriter.instance.appendInfo(String.format(
							"Re-adding MakePredictiveCall cuID:%s maxpoolSize:%d activeThreads: %d queueSize: %d predictiveQueueSize: %d",
							nextCommand.receiver.ToCUNumber,
							executor.getMaximumPoolSize(),
							executor.getActiveCount(),
							executor.getQueue().size(),
							QueueManagerForPred.GetInstance().size()));

					nextCommand.Execute();
				}
					
				return null;
			});			
		}
		
		LogWriter.instance.appendInfo(String.format(
				"Added MakePredictiveCall cuID:%s maxpoolSize:%d activeThreads: %d queueSize: %d predictiveQueueSize: %d",
				receiver.ToCUNumber,
				executor.getMaximumPoolSize(),
				executor.getActiveCount(),
				executor.getQueue().size(),
				QueueManagerForPred.GetInstance().size()));
		
		//try {
		//	Thread.sleep(15000);
		//} catch (InterruptedException e) {
		//	e.printStackTrace();
		//}	
		
	}
}
