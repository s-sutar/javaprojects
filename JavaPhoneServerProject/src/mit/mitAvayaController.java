package mit;
import core.AppProperties;
import core.LogWriter;
import db.DBManager;
import network.ClientSocket;
import network.SocketReader;
import pbx.AvayaPbx;
import pbx.Pbx;
import pbx.PbxInfo;
import pbx.PbxPool;
import pbx.SimPbx;
import pbx.astPbx;

public class mitAvayaController {
	private SocketReader sockrd = new SocketReader(null);
	private ClientSocket csock = new ClientSocket(true, 0, null);
	private mitAvayaListener myAvayaListener = null;

	public static mitAvayaController instance = null;

	public mitAvayaController() {
		System.out.println("mitAvayaController initializing");
		
		instance = this;

		LogWriter.instance.initializeFile();
		AppProperties.instance.load();
		DBManager.GetInstance().initialize();
		LogWriter.instance.appendInfo("Starting version " + LogWriter.VERSION);

		if(AppProperties.instance.isOutgoingSimulation == true)
			LogWriter.instance.appendInfo("outgoing_simulation is on");

        new Thread("AES Connect") {		            	
			public void run() {
				System.out.println("mitAvayaController AES Connect started");

				// initialize PBX
				PbxPool.GetInstance().clearPbxList();
//				configureAndOpenPBX(1, "192.168.22.200", "8088", "ast20190529", "sakura529", "", "OrionsApp", "6");
/*				Pbx pbx = configurePBX(1);
				
				PbxPool.GetInstance().AddPbx(pbx);
								
				if(AppProperties.instance.isOutgoingSimulation == false)
					pbx.Login();
				
				
				//Add the alternate PBXs
				String pbxListSetting = AppProperties.instance.pbxtemp_address;
				if (pbxListSetting != null) {
					String[] pbxList = pbxListSetting.split(",");
					for (String pbxStr : pbxList) {
						String[] pbxDetails = pbxStr.split(":");
						
						int pbxID = Integer.parseInt(pbxDetails[0]);
						pbx = configurePBX(pbxID);
						
						PbxPool.GetInstance().AddPbx(pbx);
										
						if(AppProperties.instance.isOutgoingSimulation == false)
							pbx.Login();
					}
				}
				*/
			}
        }.start();      
	}
	
	public String configureAndOpenPBX(Pbx.PbxType pbxType, String serverIP, String portNumber, String login, String password, String serviceName, String stasisApp, String trunk_name, String pbxNum) {
		String errorStr = "";

		try {
			PbxInfo pbxInfo = new PbxInfo(
					serverIP,
					portNumber,
					login,
					password,
					serviceName,
					stasisApp,
					Integer.parseInt(pbxNum));
			
			Pbx pbx = null;
								
			if (pbxType == Pbx.PbxType.MPBX) {
				AppProperties.instance.pbxtype = "mpbx";
			} else {
				AppProperties.instance.pbxtype = "avaya";			
			}
			
			LogWriter.instance.appendInfo("Enabling/Opening PBX: " + pbxNum + " " + pbxInfo.Url);		
	
			if(pbxType == Pbx.PbxType.MPBX) {
				if (AppProperties.instance.stasisApp != null && !AppProperties.instance.stasisApp.isEmpty() && pbxInfo.StasisApp == null) {
					pbxInfo.StasisApp = AppProperties.instance.stasisApp + pbxNum;
				}
				pbxInfo.TrunkName = trunk_name;
				pbx = new astPbx(pbxInfo);
				errorStr = pbx.finishInit();
			}
			else if(AppProperties.instance.isSimulation) {
				pbx = new SimPbx(pbxInfo);
			} else {
				pbx = new AvayaPbx(pbxInfo);
			}
	
			PbxPool.GetInstance().AddPbx(pbx);
			
			if(AppProperties.instance.isOutgoingSimulation == false) {
				if(pbx.Login() == false)
					errorStr = "Error logging into PBX";
			}
		
		} catch (Exception ex) {
			errorStr = "configureAndOpenPBX: " + ex.getMessage();
		}

		return errorStr;		
	}
	
	public Pbx configurePBX(int pbxNum) {
		PbxInfo pbxInfo = new PbxInfo(
				AppProperties.instance.serverIP,
				AppProperties.instance.portNumber,
				AppProperties.instance.login,
				AppProperties.instance.password,
				AppProperties.instance.serviceName,
				AppProperties.instance.stasisApp,
				pbxNum);
		
		if (pbxNum > 1) {
			//Keep the settings from pbx1, just change the IP address
			String pbxListSetting = AppProperties.instance.pbxtemp_address;
			if (pbxListSetting != null) {
				String[] pbxList = pbxListSetting.split(",");
				for (String pbxStr : pbxList) {
					String[] pbxDetails = pbxStr.split(":");
					
					if (pbxInfo.pbxID == Integer.parseInt(pbxDetails[0])) {
						pbxInfo.Url = pbxDetails[1];
					}
				}
			}
		}
		
		LogWriter.instance.appendInfo("Enabling PBX: " + pbxNum + " " + pbxInfo.Url);		
		
		Pbx pbx = null;
		
		if(AppProperties.instance.pbxtype.equals("mpbx")) {
			if (AppProperties.instance.stasisApp != null) {
				pbxInfo.StasisApp = AppProperties.instance.stasisApp + pbxNum;
			}
			pbx = new astPbx(pbxInfo);
		}
		else if(AppProperties.instance.isSimulation) {
			pbx = new SimPbx(pbxInfo);
		} else {
			pbx = new AvayaPbx(pbxInfo);
		}
		
		return pbx;
	}	

	public String closePBX(Pbx.PbxType pbxType, String pbxNum) {
		int pbxID = Integer.parseInt(pbxNum);
		
		Pbx pbx = PbxPool.GetInstance().GetPbx(pbxType, pbxID);
		
		if(pbx == null)
			return "PBX not found";
		
		pbx.closeConnection();		//mPBX
		pbx.Logout();				//Avaya
		
		PbxPool.GetInstance().RemovePbx(pbx);

		return "";
	}
	
	public void registerListener(mitAvayaListener mal) {
		this.myAvayaListener = mal;
	}
	
	public void sendMessageToPBX(String msg, int pbxType) {   
		//if (pbxType == 0 || pbxType == 1) {
			csock.onMessage(msg);
		//}
		//else {
		//	sockrd.onMessage(msg);
		//}
	}

	public void sendMessageToListener(String msg) {   	    
	    if(this.myAvayaListener != null) {
	    	this.myAvayaListener.onMessageFromPBX(msg);
	    }
	}

}
