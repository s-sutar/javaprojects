package pbx;

import java.util.Date;

import javax.telephony.Address;
import javax.telephony.Call;
import javax.telephony.CallEvent;
import javax.telephony.Connection;
import javax.telephony.ConnectionEvent;
import javax.telephony.MetaEvent;
import javax.telephony.callcontrol.CallControlConnectionEvent;
import javax.telephony.callcontrol.CallControlConnectionListener;

import com.avaya.jtapi.tsapi.impl.events.conn.LucentV5CallControlConnectionEvent;

import core.EventsReceived;
import core.LogWriter;
import network.WebClient;

public class AvayaCallControlConnectionListener implements CallControlConnectionListener {

	private String toNumber = null;
	private boolean hasNetworkReached = false;
	private Date establishTime;
	private Date disconnectTime;
	
	public AvayaCallControlConnectionListener(String toNumber) {
		this.toNumber = toNumber;
	}
	
    private String getAddressNameWithText(ConnectionEvent event) {
        String name = null;
        
        try {
          Connection connection = event.getConnection();
          Address addr = connection.getAddress();
          name = addr.getName();
        } catch (Exception e) {
        	LogWriter.instance.appendException(e);
		}
        
		return "Connection to Address " + name + " is ";
	}
    
	private String[] getPhoneNumbers(ConnectionEvent event) {
		String callingNumber = "null";
		String calledNumber = "null";

		try {
			int id = event.getID();
			int cause = event.getCause();

			Call call = event.getCall();
			Connection[] conns = (call != null) ? call.getConnections() : null;

			int callState = call.getState();
			String additional = "";

			if (conns != null && conns.length == 2 && conns[0] != null && conns[1] != null) {
				callingNumber = conns[0].getAddress().getName();
				calledNumber = conns[1].getAddress().getName();

				int callingConnState = conns[0].getState();
				int calledConnState = conns[1].getState();

				additional = String.format(", callingConnState=%d, calledConnState=%d", callingConnState,
						calledConnState);
			}

			LogWriter.instance.appendInfo(String.format("id=%d, cause=%d, calling=%s, called=%s, callState=%d %s", id,
					cause, callingNumber, calledNumber, callState, additional));
		} catch (Exception e) {
			LogWriter.instance.appendException(e);
		}
		
		return new String[] { callingNumber, calledNumber };
	}
    
	@Override
	public void callActive(CallEvent arg0) {
		LogWriter.instance.appendInfo("Connection callActive");
		
	}

	@Override
	public void callEventTransmissionEnded(CallEvent arg0) {
		LogWriter.instance.appendInfo("Connection callEventTransmissionEnded");
		
	}

	@Override
	public void callInvalid(CallEvent arg0) {
		LogWriter.instance.appendInfo("Connection callInvalid");
		
	}

	@Override
	public void multiCallMetaMergeEnded(MetaEvent arg0) {
		LogWriter.instance.appendInfo("Connection multiCallMetaMergeEnded");
		
	}

	@Override
	public void multiCallMetaMergeStarted(MetaEvent arg0) {
		LogWriter.instance.appendInfo("Connection multiCallMetaMergeStarted");
		
	}

	@Override
	public void multiCallMetaTransferEnded(MetaEvent arg0) {
		LogWriter.instance.appendInfo("Connection multiCallMetaTransferEnded");
		
	}

	@Override
	public void multiCallMetaTransferStarted(MetaEvent arg0) {
		LogWriter.instance.appendInfo("Connection multiCallMetaTransferStarted");
		
	}

	@Override
	public void singleCallMetaProgressEnded(MetaEvent arg0) {
		LogWriter.instance.appendInfo("Connection singleCallMetaProgressEnded");
		
	}

	@Override
	public void singleCallMetaProgressStarted(MetaEvent arg0) {
		LogWriter.instance.appendInfo("Connection singleCallMetaProgressStarted");
		
	}

	@Override
	public void singleCallMetaSnapshotEnded(MetaEvent arg0) {
		LogWriter.instance.appendInfo("Connection singleCallMetaSnapshotEnded");
		
	}

	@Override
	public void singleCallMetaSnapshotStarted(MetaEvent arg0) {
		LogWriter.instance.appendInfo("Connection singleCallMetaSnapshotStarted");
		
	}

	@Override
	public void connectionAlerting(ConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getAddressNameWithText(arg0) + "ALERTING");
		
		//{"Channel":"610001","EventName":"OriginateResponse","Exten":"fus00008043497192","Message":"success","Response":"Success","Timestamp":"11:20:04.4171"}
		//{"Channel":"420007","EventName":"Dial:Begin","Exten":"fus00005058371760","Message":"success","Timestamp":"09:53:09.9571"}
		String[] temp = getPhoneNumbers(arg0);
		
		if(temp != null && temp.length >= 1) {
			String extension = temp[0];
			String destNumber = this.toNumber;
			
/*			EventsReceived evtOriginateResponse = new EventsReceived("OriginateResponse", extension);
			evtOriginateResponse.Exten = destNumber;		
			evtOriginateResponse.Response = "Success";
			evtOriginateResponse.Message = "Success";
			
			String responseMsg = evtOriginateResponse.ToJson();
			WebClient.GetInstance().SendMessage(responseMsg);*/			
			
	    	EventsReceived evt = new EventsReceived("Dial:Begin", extension);
			evt.Message = "success";
			evt.Exten = destNumber;
			
			String msg = evt.ToJson();
			WebClient.GetInstance().SendMessage(msg);
		}
	}

	@Override
	public void connectionConnected(ConnectionEvent arg0) {
		//{"Channel":"610001","EventName":"bridgeoutbound","Exten":"900008043497192","Message":"success","Timestamp":"19:25:51.1233"}
		LogWriter.instance.appendInfo(getAddressNameWithText(arg0) + "CONNECTED");

		String[] temp = getPhoneNumbers(arg0);
		
		if(temp != null && temp.length >= 1) {
			String extension = temp[0];
			String destNumber = this.toNumber;
			EventsReceived evtOriginateResponse = new EventsReceived("OriginateResponse", extension);
			evtOriginateResponse.Exten = destNumber;		
			evtOriginateResponse.Response = "Success";
			evtOriginateResponse.Message = "Success";
			
			String responseMsg = evtOriginateResponse.ToJson();
			WebClient.GetInstance().SendMessage(responseMsg);	
			
			
	    	EventsReceived evt = new EventsReceived("bridgeoutbound", extension);
			evt.Message = "success";
			evt.Exten = destNumber;
			
			String msg = evt.ToJson();
			WebClient.GetInstance().SendMessage(msg);
		}
	}

	@Override
	public void connectionCreated(ConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getAddressNameWithText(arg0) + "CREATED");
	}

	@Override
	public void connectionDisconnected(ConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getAddressNameWithText(arg0) + "DISCONNECTED");
		
		this.disconnectTime = new Date();
	}

	@Override
	public void connectionFailed(ConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getAddressNameWithText(arg0) + "FAILED");
		
/*		String[] temp = getPhoneNumbers(arg0);
		
		if(temp != null && temp.length >= 1) {
			String extension = temp[0];
			String destNumber = this.toNumber;
	    	EventsReceived evt = new EventsReceived("", extension);
			evt.Message = "success";
			evt.Exten = destNumber;
			String msg = evt.ToJson();
			WebClient.GetInstance().SendMessage(msg);
		}*/
	}

	@Override
	public void connectionInProgress(ConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getAddressNameWithText(arg0) + "IN PROGRESS");
	}

	@Override
	public void connectionUnknown(ConnectionEvent arg0) {
		LogWriter.instance.appendInfo(getAddressNameWithText(arg0) + "UNKNOW");
	}

	@Override
	public void connectionAlerting(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("Connection connectionAlerting");
	}

	@Override
	public void connectionDialing(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("Connection connectionDialing");
	}

	@Override
	public void connectionDisconnected(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("Connection connectionDisconnected");
		
	}

	@Override
	public void connectionEstablished(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("Connection connectionEstablished");
		
		if(arg0.getCause() == 110) {
			// set establish time for AU call
			this.establishTime = new Date();
		}		
	}

	@Override
	public void connectionFailed(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("Connection connectionFailed");
		
		handleHangUpCauseCode(arg0);		
	}
	
	public void handleHangUpCauseCode(CallControlConnectionEvent event) {		
		int lucentEventCauseCode = 0;
		int callCtrlCause = 0;
		String calledNumber = "";
		String callingNumber = "";
		//int metaEvtCause = 0;
		
		try {
			if(event instanceof LucentV5CallControlConnectionEvent) {
				LucentV5CallControlConnectionEvent temp  = (LucentV5CallControlConnectionEvent)event;
				
				//int evtCause = temp.getCause();
				lucentEventCauseCode = temp.getCSTACause();
				callCtrlCause = temp.getCallControlCause();
				//metaEvtCause = temp.getMetaEvent().getCause();
				//int reason = temp.getReason();
				
				calledNumber = (temp.getCalledAddress() != null) ? temp.getCalledAddress().getName() : "";
				callingNumber = (temp.getCallingAddress() != null) ? temp.getCallingAddress().getName() : "";
				
/*				int isupCauseCode = EventInterpreter.toIsupCauseCode(lucentEventCauseCode);
				String isupCauseText = EventInterpreter.toIsupCauseText(isupCauseCode);
				String lucentEventCauseText = EventInterpreter.toLucentEventCauseText(lucentEventCauseCode);
	
				LogWriter.instance.appendInfo(String.format(
						"==> MyCallControlConnectionListener. cause=%d, cstaCause=%d-%s, isupCause=%d-%s, callCtrlCause=%d, metaEventCause=%d, reason=%d%n",
						evtCause, lucentEventCauseCode, lucentEventCauseText, isupCauseCode, isupCauseText, 
						callCtrlCause, metaEvtCause, reason));
						*/
			}
		} catch(Exception e) {
			LogWriter.instance.appendException(e);
		}

		String result = "failed";
		String msg = "";
		
		// process according to cause code
		switch (lucentEventCauseCode) {
			case 81 :// EC_CALL_CANCELLED - ISUP 21 (call rejected)
			{
				msg = "81 EC_UNASSIGNED_NUMBER - ISUP 0";
			}
			break;
		
			case 5 :// EC_CALL_CANCELLED - ISUP 21 (call rejected)
				{
					msg = "5 EC_CALL_CANCELLED - ISUP 21 (call rejected)";
				}
				break;
				
			case 3 :// EC_BUSY - ISUP 17 (user busy)
				{
					msg = "3 EC_BUSY - ISUP 17 (user busy)";
				}
				break;
				
			case 60 :// EC_ALERT_TIME_EXPIRED - ISUP 18 (no user responding)
				{
					msg = "60 EC_ALERT_TIME_EXPIRED - ISUP 18 (no user responding)";
				}
				break;
				
			case 21 :// EC_NETWORK_NOT_OBTAINABLE - ISUP 19 (no answer from the user)
				{
					msg = "21 EC_NETWORK_NOT_OBTAINABLE - ISUP 19 (no answer from the user)";
				}
				break;
				
			case 29 :// EC_REORDER_TONE - ISUP 22 (number changed)
				{
					msg = "29 EC_REORDER_TONE - ISUP 22 (number changed)";
				}
				break;
				
			case 15 :// EC_INCOMPATIBLE_DESTINATION
				{
					msg = "15 EC_INCOMPATIBLE_DESTINATION";
				}
				break;
			
			case 13 :// EC_DEST_NOT_OBTAINABLE - ISUP 31 (normal unspecified)
				{
					long timeTaken = 0;
					
					if(this.establishTime == null) {
						LogWriter.instance.appendInfo("TimeTaken: this.establishTime");
					} else if(this.disconnectTime == null){
						LogWriter.instance.appendInfo("TimeTaken: this.disconnectTime");
					} else {
						timeTaken = this.disconnectTime.getTime() - this.establishTime.getTime();
						timeTaken = timeTaken / 1000; //convert ms to sec
						
						LogWriter.instance.appendInfo(String.format("TimeTaken: time=%d sec", timeTaken));
					}
					
					if(hasNetworkReached) {
						msg = "15 EC_REORDER_TONE - ISUP 22 (number changed)";
						lucentEventCauseCode = 15;
					} else if(timeTaken == 0) {
				    	msg = "13 EC_DEST_NOT_OBTAINABLE - ISUP 31 (normal unspecified)";
					} else if(timeTaken < 20/*AppProperties.intance.auThresholdBusy*/) {
						msg = "3 EC_BUSY - ISUP 17 (user busy)";
						lucentEventCauseCode = 3;
					} else if(timeTaken < 30/*AppProperties.intance.auThresholdPowerOff*/) {
						msg = "60 EC_ALERT_TIME_EXPIRED - ISUP 18 (no user responding)";
						lucentEventCauseCode = 60;
					} else {
						msg = "5 EC_CALL_CANCELLED - ISUP 21 (call rejected)";
						lucentEventCauseCode = 5;
					}
				}
				break;
				
			case -1 :
			{
				if(callCtrlCause != 100) {
					return;
				}
				
				result = "success";
			}
			break;
				
			case 10 :// EC_CALL_NOT_ANSWERED - for makePredictiveCall
			{
				msg = "10 EC_CALL_NOT_ANSWERED";
			}
			break;
				
			case 28 : // redirect
			{
				return;
			}
			
			default :
				{
					msg = "default";
				}
		}//end switch		
		
/*		if(this.requestHandler != null) {
			//hasDisconnectedResponded = true;
			
			String callingNumber = (this.myAddress != null) ? this.myAddress.getName() : "";
			String response = String.format("Event: OriginateResponse\r\nResponse: %s\r\nChannel: %s\r\nContext: %s\r\nExten: %s\r\nReason: %d\r\nMessage: %s",
					result, callingNumber, msg, calledNumber, lucentEventCauseCode, extractedCustomerID);
			
			if("success".equals(result) == false) {
				hasCustomerAnswered = true; //to cancel no answer timeout checking when call fails
				
				//Caller caller = AvayaManager.instance.getCaller(callingNumber);				
				if(caller != null) {
					caller.usedConnCount--;// customer connection
				}
			}

			this.requestHandler.sendBackToClient(response);
		}*/

/*		int finalCause = 0;
		EventsReceived evt = new EventsReceived("Hangup", callingNumber);
		evt.Exten = calledNumber;
		evt.Message = "success";
		evt.ChannelState = Integer.toString(finalCause);
		evt.Response = String.format("call's hung up , cause=%d, finalCause=%d", lucentEventCauseCode, finalCause);
		
		String responseMsg = evt.ToJson();
		WebClient.GetInstance().SendMessage(responseMsg);*/
		
		//----------------------------------
		EventsReceived evt = new EventsReceived("OriginateResponse", callingNumber);
		evt.Exten = calledNumber;		
		evt.Response = result;
		evt.Context = msg;
		evt.Reason = Integer.toString(lucentEventCauseCode);
		
		String responseMsg = evt.ToJson();
		WebClient.GetInstance().SendMessage(responseMsg);
	}

	@Override
	public void connectionInitiated(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("Connection connectionInitiated");
		
	}

	@Override
	public void connectionNetworkAlerting(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("Connection connectionNetworkAlerting");
		
	}

	@Override
	public void connectionNetworkReached(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("Connection connectionNetworkReached");
		
		this.hasNetworkReached = true;
	}

	@Override
	public void connectionOffered(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("Connection connectionOffered");
		
	}

	@Override
	public void connectionQueued(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("Connection connectionQueued(");
		
	}

	@Override
	public void connectionUnknown(CallControlConnectionEvent arg0) {
		LogWriter.instance.appendInfo("Connection connectionUnknown");
		
	}

}
