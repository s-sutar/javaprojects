package acd;

import java.util.Hashtable;

import javax.telephony.Address;
import javax.telephony.Provider;
import javax.telephony.callcenter.ACDAddress;
import javax.telephony.callcenter.Agent;
import javax.telephony.callcenter.AgentTerminal;
import javax.telephony.callcenter.CallCenterProvider;

import com.avaya.jtapi.tsapi.LucentAgent;
import com.avaya.jtapi.tsapi.TsapiPlatformException;

import core.LogWriter;
import db.AgentModel;

public class ACDManager {
	
	private static ACDManager _instance = new ACDManager();
	private ACDAddress[] acdAddresses = null;
	private String[] acdNameList;
	private Hashtable<String, Agent> agentMapping = new Hashtable<String, Agent>();
	
	// key=extension
	protected Hashtable<String, AgentModel> agentList = new Hashtable<String, AgentModel>();
	
	private ACDManager() {
	}
	
	public static ACDManager GetInstance() {		
		if(_instance == null) {
			synchronized(ACDManager.class) {
				if(_instance == null) {
					_instance = new ACDManager();
				}
			}
		}
		
		return _instance;
	}
	
	public void MonitorAcd(Provider provider) {
		if(provider == null) {
			return;
		}
		
		if((provider instanceof CallCenterProvider) == false) {
			return;
		}	
		
		try {
			acdAddresses = ((CallCenterProvider) provider).getACDAddresses();
			
			if(acdAddresses == null || acdAddresses.length == 0) {
				return;
			}
			
			// print all acd addresses
			LogWriter.instance.appendInfo("\nACDAddresses for this provider:" + provider.getName() + ":");
			int len = acdAddresses.length;
			this.acdNameList = new String[len];
			
            for (int i = 0; i < len; i++) {
            	ACDAddress myACDAddress =  acdAddresses[i];
            	
            	String acdName = myACDAddress.getName();
            	this.acdNameList[i] = acdName;
            	LogWriter.instance.appendInfo("\tAdded listener for ACDAddress=" + acdName);

            	myACDAddress.addAddressListener(new MyACDListener(acdName));
            }
		} catch(Exception e) {
			LogWriter.instance.appendException(e);
		}
	}

	public Agent[] GetAgents(ACDAddress[] acdAddresses) {
		Agent[] agents = null;
		
	    for (int i = 0; i < acdAddresses.length; i++) {
	        try {
	            agents = acdAddresses[i].getLoggedOnAgents();
	        } catch (Exception e) {
	        	LogWriter.instance.appendError("Could not obtain logged on agents.");
	        }
	
	        if (agents != null) {	
	        	LogWriter.instance.appendInfo("Agents at " + acdAddresses[i].getName() + ":");
	        	
	            for (int k = 0; k < agents.length; k++) {
	            	Agent agent = agents[k];
	            	String extension = agent.getAgentTerminal().getName();
	            	int stateId = agent.getState();
	            	String state = stateToString(stateId);
	            	
	            	String log = String.format("\tAgent[%d]= %s %d-%s", k, extension, stateId,state);
	            	LogWriter.instance.appendInfo(log);
	            }
	            
	        } else {
	        	LogWriter.instance.appendInfo("Agents at " + acdAddresses[i].getName() + ": <none>");
	        }
	    }//end for
	    
	    return agents;
    }
	
	public static String stateToString(int state) {
		
        String strState;

        switch (state) {
        case (Agent.LOG_IN):
            strState = "LOG_IN";
            break;
        case (Agent.LOG_OUT):
            strState = "LOG_OUT";
            break;
        case (Agent.NOT_READY):
            strState = "NOT_READY";
            break;
        case (Agent.READY):
            strState = "READY";
            break;
        case (Agent.WORK_NOT_READY):
            strState = "WORK_NOT_READY";
            break;
        case (Agent.WORK_READY):
            strState = "WORK_READY";
            break;
        case (Agent.BUSY):
            strState = "BUSY";
            break;
        case (Agent.UNKNOWN):
            strState = "UNKNOWN";
            break;
        default:
            strState = "Other: " + state;
            break;
        }
        return strState;
    }
	
    public static String modeToString(int mode) {

        String strMode;

        switch (mode) {
        case LucentAgent.MODE_AUTO_IN:
            strMode = "MODE_AUTO_IN";
            break;
        case LucentAgent.MODE_MANUAL_IN:
            strMode = "MODE_MANUAL_IN";
            break;
        case LucentAgent.MODE_NONE:
            strMode = "MODE_NONE";
            break;
        default:
            strMode = "Other: " + mode;
            break;
        }
        return strMode;
    }
    
    public String[] GetAcdNamesList() {
    	
    	return this.acdNameList;
    }
    
    public boolean LoginAgent(Provider provider, String acd, String extension,
    		String agentID, String password) {
    	if(extension == null || extension.isEmpty()) {
    		return false;
    	}
    	
    	try {
    		ACDAddress acdAddress = (ACDAddress) provider.getAddress(acd);
    		Address agentAddress = provider.getAddress(extension);
    		AgentTerminal myAgent2Terminal = (AgentTerminal) provider.getTerminal(extension);
    		
    		// login agent
            Agent agent = myAgent2Terminal.addAgent(
            		agentAddress, // Agent's address
                    acdAddress, // ACD's address
                    Agent.LOG_IN, // Agent's initial state
                    agentID, // Agent ID
                    password); // Agent password
            
            // add into agentList
            AddAgent(agent);
            
/*            new Thread() {
            	Agent temp = agent;
                public void run() {
                    while(true) {
                    	try {
                    		Logger.Write("Agent: ext=" + temp.getAgentAddress().getName() + ", state=" + temp.getState());
							sleep(5000);
						} catch (InterruptedException e) {
							Logger.Write(e);
						}
                    }
                }
            }.start();*/
            
            return true;
    	} catch (TsapiPlatformException te) {
    		String errMsg = te.getMessage();
    		if("An attempt to log in an ACD agent with an incorrect password".equals(errMsg) 
    				|| "Invalid AgentId is specified".equals(errMsg)) {
    			LogWriter.instance.appendInfo("Failed to login: extension="
    				+ extension + ", agentID=" + agentID  +", exception=" + errMsg);
    		} else {    			
    			LogWriter.instance.appendException(te);
    		}
        } catch (Exception e) {
        	LogWriter.instance.appendException(e);
        }
    	
    	return false;
    }
    
    public boolean LogoutAgent(String extension, Provider provider) {
    	Agent agent = GetAgent(extension);
    	
    	try {
    		if(agent != null) {
	    		AgentTerminal myAgent2Terminal = (AgentTerminal) provider.getTerminal(extension);
	    		
	    		// log out
	            //myAgent2Terminal.removeAgent(agent);
	            Agent[] agents = myAgent2Terminal.getAgents();
	            
	            if(agents != null && agents.length > 0) {
	            	for(Agent item : agents) {
	            		if(item.getAgentID().equals(agent.getAgentID())) {
	            			myAgent2Terminal.removeAgent(item);
	            			
	        	            // remove agent list
	        	            RemoveAgent(agent);
	            			return true;
	            		}
	            	}
	            }
    		}
        } catch (Exception e) {
        	LogWriter.instance.appendException(e);
        }
    	
    	return false;
    }
    
    public boolean SetAgentState(String extension, int state) {
    	Agent agent = GetAgent(extension);
    	
    	if(agent != null) {
    		try {
	    		((LucentAgent)agent).setState(state, LucentAgent.MODE_MANUAL_IN);
	    		return true;
    		} catch(Exception e) {
    			LogWriter.instance.appendException(e);
    		}
    	}
    	
    	return false;
    }
    
    private Agent GetAgent(String extension) {
    	Agent agent = null;
    	
    	if(extension != null) {
    		if(this.agentMapping.containsKey(extension)) {
    			agent = this.agentMapping.get(extension);
    		}
    	}
    	
    	if(agent == null) {
    		LogWriter.instance.appendError("Could not find agent: ext=" + extension);
    	}
    	
    	return agent;
    }
    
    private void AddAgent(Agent agent) {
    	if(agent == null || agent.getAgentID() == null) {
    		return;
    	}
    	
    	String extension = agent.getAgentAddress().getName();
    	
    	if(this.agentMapping.containsKey(extension) == false) {
        	synchronized(this.agentMapping) {
        		if(this.agentMapping.containsKey(extension) == false) {
        			this.agentMapping.put(extension, agent);
        			LogWriter.instance.appendInfo("Added agent: ext=" + extension);
        		}
        	}
        }
    }
    
    private void RemoveAgent(Agent agent) {
    	if(agent == null || agent.getAgentID() == null) {
    		return;
    	}
    	
    	String extension = agent.getAgentAddress().getName();
    	
    	if(this.agentMapping.containsKey(extension) == false) {
        	synchronized(this.agentMapping) {
        		if(this.agentMapping.containsKey(extension) == false) {
        			this.agentMapping.remove(extension);
        			LogWriter.instance.appendInfo("Removed agent: ext=" + extension);
        		}
        	}
        }
    }
    
    public boolean CheckAgentNewState(String extension, int stateId) {    	
    	Agent agent = GetAgent(extension);
    	
    	if(agent != null) {
    		int currState = agent.getState();
    		
    		if(currState != stateId) {
    			return true;
    		}
    	}
    	
    	return false;
    }

	public boolean AddMonitoredAgent(AgentModel agent) {
		boolean isSuccess = false;
		
		if(agent != null && agent.extension != null) {
			synchronized (this.agentList) {				
				//The method put will replace the value of an existing key and will create it if doesn't exist.
				this.agentList.put(agent.extension, agent);				
				
				isSuccess = true;
			}
		}
		
		LogWriter.instance.appendInfo("AddMonitoredAgent(" +
				((isSuccess) ? "success" : "failed") + "): extension=" + agent.extension);
		
		return isSuccess;
	}

	public boolean RemoveMonitoredAgent(AgentModel agent) {
		boolean isSuccess = false;
		
		if(agent != null && agent.extension != null) {
			synchronized (this.agentList) {
				if(this.agentList.containsKey(agent.extension)) {
					this.agentList.remove(agent);
					isSuccess = true;
				}
			}
		}
		
		LogWriter.instance.appendInfo("RemoveMonitoredAgent(" +
				((isSuccess) ? "success" : "failed") + "): extension=" + agent.extension);
		
		return isSuccess;
	}

	public AgentModel GetMonitoredAgent(String extension) {
		
		if(extension != null) {
			AgentModel temp = this.agentList.get(extension);

			return temp;
		}
		
		return null;
	}

}
