package network;

import java.net.URI;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;

import core.AppProperties;
import core.LogWriter;
import core.PhoneManager;

public class WebClient {
	public static boolean isShutdown = false;
	private static WebClient _instance = new WebClient();
	private WebSocketClient client = null;
	private ClientSocket socket = null;
	
	private WebClient() {
	}
	
	public static WebClient GetInstance() {
		if(_instance == null) {
			synchronized(WebClient.class) {
				if(_instance == null) {
					_instance = new WebClient();
				}
			}
		}
		return _instance;
	}
	
	public void SendMessage(String msg) {
		if(this.socket == null || msg == null || msg.isEmpty()) {
			return;
		}
		
		this.socket.sendMessage(msg);
	}
	
	public void Connect(String url) {
		if(PhoneManager.isRunning == false) {
			LogWriter.instance.appendInfo("Terminated WebClient connecting - PhoneManager.isRunning=false");
			
			return;
		}
		
		Close();
		
		this.client = new WebSocketClient();
		this.socket = new ClientSocket();
		
		try {
			URI toUri = new URI(url);
			if(this.client != null) {
				this.client.start();
				this.client.connect(this.socket, toUri, new ClientUpgradeRequest());
			}
			
			LogWriter.instance.appendInfo("WebClient: connecting to " + toUri);
			
			if(this.socket != null) {
				this.socket.awaitClose(5, TimeUnit.SECONDS);
			}
			
			if(this.socket != null && this.socket.isConnected && this.client.isRunning()) {
				LogWriter.instance.appendInfo("WebClient: connected to " + toUri);
				final ClientSocket mySocket = this.socket;
				
				String machineName = AppProperties.instance.hostname + "-debug:" + java.util.UUID.randomUUID();
				String loginRequest = "{\"Secret\":\"sakura123\",\"Sender\":\"" + machineName + "\",\"UserName\":\"predial\"}";
				WebClient.GetInstance().SendMessage(loginRequest);				
				
	            new Thread() {	            	
	            	int pingTime = 30;
	            	
					public void run() {
						LogWriter.instance.appendInfo("Created - Pinging loop");
						
	                    while(PhoneManager.isRunning && mySocket != null && mySocket.isConnected) {
	                    	try {
	                    		mySocket.sendMessage("ping");
	                    		
	                    		int count = 0;
	                    		//while(count < pingTime && PhoneManager.isRunning) {
	                    		while(count < pingTime && PhoneManager.isRunning && mySocket != null && mySocket.isConnected) {
	                    			sleep(1000);
	                    			count++;
	                    		}
							} catch (InterruptedException e) {
								LogWriter.instance.appendException(e);
							}
	                    }
	                    
	                    LogWriter.instance.appendInfo("Terminated - Pinging loop");
	                }
	            }.start();				
			} else {
				String details = "socket is not null";
				//this.socket != null && this.socket.isConnected && this.client.isRunning()
				if (this.socket == null) {
					details = "socket is null";
				} else if(this.socket.isConnected) {
					details = "socket is not connected";
				}

				if (this.client != null && this.client.isRunning()) {
					details += " & client is not running";
				}

				LogWriter.instance.appendError("WebClient: failed to connect to="+ toUri + ", details=" + details);
				//jack
				//this.Close();
				
				if(!WebClient.isShutdown) {
		           /* new Thread() {		            	
						public void run() {*/
							int waitTime = AppProperties.instance.reconnect_waittime;
							LogWriter.instance.appendInfo("Try to connect to web server again in " + waitTime + " seconds, url=" + toUri);
							
							try {
								for(int i = 0; i < waitTime; i++) {
									if(WebClient.isShutdown) { 
										return;
									}
									
									Thread.sleep(1000);
								}
							} catch (InterruptedException e1) {
								LogWriter.instance.appendException(e1);
							}
							
							if(!WebClient.isShutdown) {
								Connect(url);
							}
/*		                }
		            }.start();*/
				}
			}
		} catch(Exception e) {
			LogWriter.instance.appendException(e);
		}//end try-catch
	}
	
	public void Close() {		
		try {
			try {
				if(this.socket != null) {
					this.socket.close();
					this.socket = null;
					
					LogWriter.instance.appendInfo("WebClient: stop ClientSocket succeeded");
				}
			} catch (Exception e) {
				LogWriter.instance.appendInfo("WebClient: stop ClientSocket failed, err=" + e.getMessage());
			}
			
			try {
				if(this.client != null) {
					this.client.stop();
					this.client = null;
					
					LogWriter.instance.appendInfo("WebClient: stop WebSocketClient succeeded");
				}
			} catch (Exception e) {
				LogWriter.instance.appendInfo("WebClient: stop WebSocketClient failed, err=" + e.getMessage());
			}
		} catch (Exception e) {
			LogWriter.instance.appendException(e);
		}
		
		LogWriter.instance.appendInfo("WebClient: clean up");
	}

}
