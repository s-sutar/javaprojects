package main;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import core.Agent;
import core.AgentState;
import core.AppProperties;
import core.CallRecord;
import core.CallState;
import core.LogWriter;
import db.DBManager;
import preFunction.pFunc;


public class Main {
	private static ArrayList<Agent> agentList = new ArrayList<>();
	private static ArrayList<CallRecord> callRecList = new ArrayList<>();
	private static pFunc pf = new pFunc();

	public static void main(String[] args) {
		LogWriter.instance.initializeFile();
		LogWriter.instance.appendInfo("Starting version " + LogWriter.VERSION);
		
		AppProperties.instance.load();
		int historicalBlockNumber = 100;
		DBManager.GetInstance().initialize();
	    ArrayList<CallRecord> historicalList =  DBManager.GetInstance().getCallRecords(historicalBlockNumber);
				
		long masterCallID = 0;
		int numAgents = 100;
		
		for( int i = 0; i < numAgents; i++) {
			Agent a = new Agent(i, 60);
			a.ready(0);
			agentList.add(a);
		}
		
		int overcallCount = 0;
		int overcallAbandoned = 0;
		
		long totalOCAnswered = 0;
		long totalOCWaitTime = 0;
		
		long totalCalls = 0;
		long totalSuccess = 0;
		
		long myClock = 0;
		
		while(myClock < 10000) {
			try {
				//Thread.sleep(100);
				myClock++;
	
				ArrayList<CallRecord> endedCalls = new ArrayList<>();

				//update the call states
				for (CallRecord cr : callRecList) {
					cr.pushClock(myClock);
					
					if (cr.getState() == CallState.ENDED) {
						endedCalls.add(cr);
						
						totalCalls++;
						if(cr.resultCode == -1)
							totalSuccess++;

						if (cr.resultCode != -1) {
							//If the call failed then pick an agent waitingforRingingCall and reset them
							for (Agent ag : agentList) {
								if (ag.waitingforRingingCall) {
									ag.waitingforRingingCall = false;
									break;
								}
							}
						}
						
						if (cr.getOverCallWaitingTime(myClock) > 0) {
							overcallAbandoned++;
						}	
					}
				}
				
				//Clean up ended calls
				for (CallRecord en : endedCalls) {
					callRecList.remove(en);
				}
				
				//update the agent states
				for (Agent ag : agentList) {
					ag.pushClock(myClock);
				}
				
				//Check to see if calls were answered
				for (CallRecord cr : callRecList) {
					if( cr.needsAgent() ) {
						//Select an agent
						Agent selectedAgent = null;
						for (Agent ag : agentList) {
							if( ag.state == AgentState.READY) {
								if(selectedAgent == null)
									selectedAgent = ag;
								else if( ag.readyStartedTime() < selectedAgent.readyStartedTime())
									selectedAgent = ag;
							}
						}
						
						long ocwaitingTime = cr.getOverCallWaitingTime(myClock);
						if(selectedAgent == null) {
							if (ocwaitingTime == 0) {
								overcallCount++;
								cr.setOverCall(myClock);
								//LogWriter.instance.appendInfo("OVERCALL: " + overcallCount);
							} 
						}
						else {
							if (ocwaitingTime > 0) {
								totalOCAnswered++;
								totalOCWaitTime += ocwaitingTime;
							}
							
							cr.assignAgent(selectedAgent);
							selectedAgent.talking(myClock);
						}
					}
				}
				
				
				//Count the agents
				int readyAgents = 0;
				int talkingAgents = 0;
				int postworkAgents = 0;
				int readyAgentsThatNeedCalls = 0;
				double sumAveWaitTime = 0.0;
				for (Agent ag : agentList) {
					switch(ag.state) {
						case READY:
							readyAgents++;
							
							if (ag.waitingforRingingCall == false) {
								readyAgentsThatNeedCalls++;
								
								ag.waitingforRingingCall = true;		//this agent will be included in this batch of calls
							}
							break;
						case TALKING:
							talkingAgents++;
							break;
						case POSTWORK:
							postworkAgents++;
							break;
						default:
							break;
					}
					
					sumAveWaitTime += ag.getAverageReadyTime();
				}
				
				double aveAveWaitTime = sumAveWaitTime / agentList.size();
				double aveOCWaitTime = 0;
				
				if (totalOCAnswered > 0)
					aveOCWaitTime = totalOCWaitTime / totalOCAnswered;
				
				double ocPercent = 0;
				if(overcallCount > 0 && masterCallID > 0)
					ocPercent = 100.0 * overcallCount / (masterCallID + historicalBlockNumber * 1000);
				
				double totalSuccessPercent = 0;
				if(totalCalls > 0)
					totalSuccessPercent = 100.0 * totalSuccess / totalCalls;
				
				//Count the calls
				int ringingCalls = 0;
				for (CallRecord cr : callRecList) {
					if( cr.getState() == CallState.RINGING)
						ringingCalls++;
				}				
				
				//Get prediction
				int numCallsToMake = 0;
				
				if(readyAgentsThatNeedCalls > 0) {
					numCallsToMake = pf.pFunction(readyAgentsThatNeedCalls);
				}
				
				NumberFormat formatter = new DecimalFormat("#0.0");     
				LogWriter.instance.appendInfo( "[" + myClock + "] Ready: " + readyAgents 
						+ " CallsToMake: " + numCallsToMake
						+ " RingingCalls: " + ringingCalls
						+ " Talking: " + talkingAgents 
						+ " Postwork: " + postworkAgents 
						+ " AveWait: " + aveAveWaitTime 
						+ " overcalls: " + overcallCount + "(" + formatter.format(ocPercent) + "%)"
						+ " ocAbandoned: " + overcallAbandoned
						+ " aveOCWaitTime: " + aveOCWaitTime
						+ " conRate: " + formatter.format(totalSuccessPercent) + "%"
						);
				
				//Make the calls
				for( int i = 0; i < numCallsToMake; i++) {
					CallRecord cr = null;
					
					if( masterCallID >= historicalList.size()) {
					    historicalList =  DBManager.GetInstance().getCallRecords(++historicalBlockNumber);
					    
					    if( historicalList.size() == 0 ) {
					    	LogWriter.instance.appendInfo( "Ran out of numbers to dial" );
					    	break;
					    }
					    
					    masterCallID = 0;
					}
					
/*					long mode = masterCallID % 10;
					if(mode == 0)
						cr = new CallRecord(myClock, masterCallID++, 5, 0, 3);		// 5 seconds and then BUSY
					else if(mode < 2)
						cr = new CallRecord(myClock, masterCallID++, 30, 0, 0);		// 30 seconds and NoAnswer
					else
						cr = new CallRecord(myClock, masterCallID++, 10, 60, -1);	// successful call
*/
					CallRecord mc = historicalList.get((int) masterCallID);
					cr = new CallRecord(myClock, masterCallID++, mc.org_ringTime, mc.org_talkTime, mc.resultCode);
					callRecList.add(cr);
				}
						
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		DBManager.GetInstance().close();
	}

}
