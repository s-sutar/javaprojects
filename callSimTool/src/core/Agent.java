package core;

//import java.util.Date;

public class Agent {
	public int AgentID;
	public AgentState state = AgentState.IDLE;
	
	public int pwTime;
	public boolean waitingforRingingCall = false;
	
	private long readyStartedTime;
	private long postworkStartedTime;
	
	private long totalWaitingTime = 0;
	private long totalCalls = 0;
	
	public Agent(int id, int postworkDur) {
		this.AgentID = id;
		this.pwTime = postworkDur;
	}
	
	public void ready(long nowTime) {
		//Date now = new Date();	
		//long nowTime = now.getTime();
		
		this.state = AgentState.READY;
		this.readyStartedTime = nowTime;
	}

	public void talking(long nowTime) {
		//Date now = new Date();	
		//long nowTime = now.getTime();
		
		this.waitingforRingingCall = false;
		this.state = AgentState.TALKING;
		
		totalCalls++;
		totalWaitingTime += nowTime - this.readyStartedTime;
	}
	
	//when call ends, agent starts postwork phase
	public void postwork(long nowTime) {
		//Date now = new Date();	
		//long nowTime = now.getTime();
		
		this.state = AgentState.POSTWORK;
		this.postworkStartedTime = nowTime;
	}
	
	public void pushClock(long nowTime) {
		//Date now = new Date();	
		//long nowTime = now.getTime();
		
		if( this.state == AgentState.POSTWORK && nowTime > this.postworkStartedTime + pwTime) {
			this.ready(nowTime);
		}
	}
	
	public long readyStartedTime() {
		return this.readyStartedTime;
	}
	
	public double getAverageReadyTime() {
		if(totalCalls == 0)
			 return 0.0;
		
		return this.totalWaitingTime / this.totalCalls;
	}
}

