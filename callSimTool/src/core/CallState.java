package core;

public enum CallState {
	IDLE, RINGING, TALKING, ENDED
}
