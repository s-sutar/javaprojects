package core;

//import java.util.Date;

public class CallRecord {
	public long callID;
	public int resultCode;
	private CallState state = CallState.IDLE;
	
	private long startAlertTime;
	private long ansTime;
	private long endTime;
	private boolean alerted;
	private boolean answered;
	private boolean ended;
	
	private Agent agent;
	
	private long overCallStartTime;
	
	public int org_ringTime;
	public int org_talkTime;
	
	public CallRecord(long nowTime, long masterCallID, int ringtime, int talktime, int resultCode) {
		this.callID = masterCallID;
		this.resultCode = resultCode;
		
		this.org_ringTime = ringtime;
		this.org_talkTime = talktime;
		
		int delay = 0;	
		
		//Date now = new Date();
		
		if(ringtime > 0) {
			this.startAlertTime = nowTime + delay;
			delay += ringtime;
		}
		
		if( talktime > 0 ) {
			this.ansTime = nowTime + delay;
			delay += talktime;
		}
		
		this.endTime = nowTime + delay;	
	}
	
	public void pushClock(long nowTime) {
		//Date now = new Date();
		//long nowTime = now.getTime();

		if( this.startAlertTime > 0 && nowTime > this.startAlertTime && this.alerted == false) {
			this.alerted = true;
			this.state = CallState.RINGING;
		}
		
		if( this.ansTime > 0 && nowTime > this.ansTime && this.answered == false) {
			this.answered = true;
			this.state = CallState.TALKING;
		}

		if( this.endTime > 0 && nowTime > this.endTime && this.ended == false) {
			this.ended = true;
			this.state = CallState.ENDED;
			
			if(this.agent != null)
				this.agent.postwork(nowTime);
		}
		
	}
	
	public CallState getState() {
		return this.state;
	}
	
	public boolean needsAgent() {
		if( this.state != CallState.TALKING)
			return false;
		
		return this.agent == null;
	}
	
	public void assignAgent(Agent ag) {
		this.agent = ag;
		this.overCallStartTime = 0;
	}
	
	public void setOverCall(long nowTime) {
		//Date now = new Date();
		this.overCallStartTime = nowTime;
	}
	
	public long getOverCallWaitingTime(long nowTime) {
		if(this.overCallStartTime > 0) {
			//Date now = new Date();
			return nowTime - this.overCallStartTime;
		}
		return 0;
	}

}

