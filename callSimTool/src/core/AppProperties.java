package core;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.Properties;

public class AppProperties {

	public String db_name;
	public String db_address;
	public String db_username;
	public String db_password;
	
	public static AppProperties instance = new AppProperties();
	
	public void load() {
		Properties prop = new Properties();
		InputStream input = null;
	 
		try {	 
			input = new FileInputStream("config.properties");
	 
			// load a properties file
			prop.load(input);
	 
			// get the property value and print it out
		    db_name = prop.getProperty("db_name");
		    db_address = prop.getProperty("db_address");
		    db_username = prop.getProperty("db_username");
		    db_password = prop.getProperty("db_password");
		    
		} catch (Exception ex) {
			LogWriter.instance.appendException(ex);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					LogWriter.instance.appendException(e);
				}
			}
		}//end try catch		
	}
}
